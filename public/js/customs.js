jQuery(function($) {

    "use strict";

    /**
     * introLoader - Preloader
     */
    jQuery("#introLoader").introLoader({
        animation: {
            name: 'gifLoader',
            options: {
                ease: "easeInOutCirc",
                style: 'dark bubble',
                delayBefore: 500,
                delayAfter: 0,
                exitTime: 300
            }
        }
    });

    /**
     * Sticky Header
     */
    jQuery(".container-wrapper").waypoint(function() {
        jQuery(".navbar").toggleClass("navbar-sticky-function");
        jQuery(".navbar").toggleClass("navbar-sticky");
        return false;
    }, { offset: "-20px" });

    /**
     * Main Menu Slide Down Effect
     */
    // Mouse-enter dropdown
    jQuery('#navbar li').on("mouseenter", function() {
        jQuery(this).find('ul').first().stop(true, true).delay(350).slideDown(500, 'easeInOutQuad');
    });
    // Mouse-leave dropdown
    jQuery('#navbar li').on("mouseleave", function() {
        jQuery(this).find('ul').first().stop(true, true).delay(100).slideUp(150, 'easeInOutQuad');
    });

    /**
     * Background changes on focusing div by .addclass method
     */
    jQuery(".bg-change-focus-addclass").on("focusin", function() {
        jQuery(this).addClass("focus");
    }).on("focusout", function() {
        jQuery(this).removeClass("focus");
    });

    /**
     * Bootstarp Dropdown as Select with active state
     */
    jQuery('.dropdown-select').on( 'click', '.dropdown-menu li a', function() {
        var target = jQuery(this).html();

        //Adds active class to selected item
        jQuery(this).parents('.dropdown-menu').find('li').removeClass('active');
        jQuery(this).parent('li').addClass('active');

        //Displays selected text on dropdown-toggle button
        jQuery(this).parents('.dropdown-select').find('.dropdown-toggle').html(target + ' <span class="caret"></span>');
    });

    /**
     * Slicknav - a Mobile Menu
     */
    jQuery('#responsive-menu').slicknav({
        duration: 500,
        easingOpen: 'easeInExpo',
        easingClose: 'easeOutExpo',
        closedSymbol: '<i class="fa fa-plus"></i>',
        openedSymbol: '<i class="fa fa-minus"></i>',
        prependTo: '#slicknav-mobile',
        allowParentLinks: true,
        label:""
    });

    /**
     * Smooth scroll to anchor
     */
    jQuery('a.anchor[href*=#]:not([href=#])').on("click",function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = jQuery(this.hash);
            target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                jQuery('html,body').animate({
                    scrollTop: (target.offset().top - 75) // 70px offset for navbar menu
                }, 1000);
                return false;
            }
        }
    });

    /**
     * Counter - Number animation
     */
    jQuery(".counter").countimator();

    /**
     * Another Bootstrap Toggle
     */
    jQuery('.another-toggle').each(function(){
        if( jQuery('.another-toggle-header',this).hasClass('active') ){
            jQuery(this).find('.another-toggle-content').show();
        }
    });
    jQuery('.another-toggle .another-toggle-header').on( 'click' , function(){
        if( jQuery(this).hasClass('active') ){
            jQuery(this).removeClass('active');
            jQuery(this).next('.another-toggle-content').slideUp();
        } else {
            jQuery(this).addClass('active');
            jQuery(this).next('.another-toggle-content').slideDown();
        }
    });

    /**
     *  Arrow for Menu has sub-menu
     */
    if (jQuery(window).width() > 992) {
        jQuery(".navbar-arrow ul ul > li").has("ul").children("a").append("<i class='arrow-indicator fa fa-angle-right'></i>");
    }

    /**
     * Bootstrap Tooltip
     */
    jQuery(function () {
        jQuery('[data-toggle="tooltip"]').tooltip()
    });

    /**
     * Icon Change on Collapse
     */
    jQuery('.collapse.in').prev('.panel-heading').addClass('active');
    jQuery('.bootstarp-accordion, .bootstarp-toggle').on('show.bs.collapse', function(a) {
        jQuery(a.target).prev('.panel-heading').addClass('active');
    }).on('hide.bs.collapse', function(a) {
        jQuery(a.target).prev('.panel-heading').removeClass('active');
    });

    /**
     * Back To Top
     */
    jQuery(window).scroll(function(){
        if(jQuery(window).scrollTop() > 500) {
            jQuery("#back-to-top").fadeIn(200);
        } else{
            jQuery("#back-to-top").fadeOut(200);
        }
    });

    jQuery('#back-to-top').on("click",function() {
        jQuery('html, body').animate({ scrollTop:0 }, '800');

        return false;
    });

    /**
     *  Placeholder
     */
    jQuery("input, textarea").placeholder();

    /**
     * Sign-in Modal
     */
    var $formLogin = jQuery('#login-form');
    var $formLost = jQuery('#lost-form');
    var $formRegister = jQuery('#register-form');
    var $divForms = jQuery('#modal-login-form-wrapper');
    var $modalAnimateTime = 300;

    jQuery('#login_register_btn').on("click", function () { modalAnimate($formLogin, $formRegister) });
    jQuery('#register_login_btn').on("click", function () { modalAnimate($formRegister, $formLogin); });
    jQuery('#login_lost_btn').on("click", function () { modalAnimate($formLogin, $formLost); });
    jQuery('#lost_login_btn').on("click", function () { modalAnimate($formLost, $formLogin); });
    jQuery('#lost_register_btn').on("click", function () { modalAnimate($formLost, $formRegister); });

    function modalAnimate ($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height",$oldH);
        $oldForm.fadeToggle($modalAnimateTime, function(){
            $divForms.animate({height: $newH}, $modalAnimateTime, function(){
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }

    /**
     * Flex Image - Image Grid Layout
     */
    jQuery('.flex-image').flexImages({rowHeight: 230});
    jQuery('.flex-image-sm').flexImages({rowHeight: 130});
    jQuery('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        jQuery('.flex-image-in-tab').flexImages({rowHeight: 230});
    });
    jQuery('.flex-image-detail').flexImages({rowHeight: 180});

    /**
     * Image Preview Tooltip
     */
    jQuery('.image-preview-tooltip a').SimpleTip({'class':'imagePreview'});

    /**
     * Custom File Upload
     */
    jQuery('#input03').filestyle({
        input : false,
        buttonName : 'btn-primary'
    });
});