window.onload = function() {
    var sliderInterval = 4000;

    setInterval(init, sliderInterval) ;// использовать функцию
};

function changeSlide() {
    /**
     * top margin for animation
     * @type {number}
     */
    var marginTop = 500;

    /**
     * block with all slides
     */
    var slider = jQuery('#gallery ul.slides');

    /**
     * slides html elements
     */
    var sliderImages = jQuery("#gallery ul.slides li");

    /**
     * index of current slide
     */
    var currentSlide = slider.data('slide');

    /**
     * index of new slide
     * @type {number}
     */
    var active = currentSlide + 1;

    /**
     * all slides quantity
     */
    var slidesQuantity = jQuery('#gallery ul.slides').children('li').length;

    /**
     * last element index
     * @type {number}
     */
    var max = slidesQuantity - 1;

    /**
     * first element index
     * @type {number}
     */
    var min = 0;

    sliderImages.removeClass('current');

    if(active > max) {
        active = min;

        jQuery('#gallery .slides li').animate({
            height : '500px'
        }, 1000);
    } else {
        var prevSlide = active - 1;

        jQuery('#gallery .slides li.slide-' + prevSlide).animate({
            height: 0
        }, 1000);
    }

    jQuery('#gallery .slides li.slide-' + active).addClass('current');
    slider.data('slide', active);
}

function init() {
    changeSlide();
}