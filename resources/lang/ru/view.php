<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'keyword'   => 'Теги',
    'refine_result'   => 'Уточнить результат',
    'image_type'   => 'Тип изображения',
    'orientation'   => 'Ориентация',
    'image_not_found' => 'Изображения не найдены',
    'search_results' => 'Результат поиска:'
];
