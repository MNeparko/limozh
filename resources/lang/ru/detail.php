<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'stock'   => 'Название',
    'photo_detail'   => 'Информация о фото',
    'image' => 'изображение',
    'uploaded_date'   => 'Дата добавления',
    'collection'   => 'Категория',
    'keywords'   => 'Теги',
    'sizes'   => 'Размер',
    'download_image_string'   => 'Скачайте одно изображение по размеру',
    'download_photo'   => 'Скачать фото',
    'same_photos'   => 'Похожие фотографии:',
    'download_preview'   => 'Скачать',
    'find_similar'   => 'Найти похожие',
    'standard_license' => 'стандартная',
    'full_license' => 'расширенная',
    'read_license' => 'Прочтите лицензию перед загрузкой',
    'share_facebook' => 'Поделиться через фейсбук',
    'share_twitter' => 'Поделиться на Twitter',
    'share_pinterest' => 'Поделиться на Pinterest',
    'close' => 'отмена',
    'download' => 'скачать фото',
    'similar_photos' => 'Просмотренные фотографии:',
    'purchase' => 'Добавить в корзину',
    'preview' => 'Предварительный просмотр',
];
