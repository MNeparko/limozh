<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'contact'   => 'Контакт с нами ',
    'text' => 'Если у вас появились вопросы, Вы можете их задать заполнив форму ниже',
    'form' => [
        'name' => 'Ваше имя',
        'email' => 'Ваш адрес электронной почты',
        'subject' => 'Тема',
        'message' => 'Сообщение',
        'required' => 'обязательный',
        'enter_name' => 'Пожалуйста введите свое полное имя *',
        'enter_email' => 'Пожалуйста, введите свой адрес электронной почты *',
        'enter_subject' => 'Пожалуйста, введите свой вопрос *',
        'enter_message' => 'Сообщение для меня *'
    ],
    'send_button' => 'Отправить сообщение',
    'required' => 'Эти поля необходимы.'
];
