<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'text1'   => 'Заказать столик в «Раскоша 1795»',
    'text2'   => 'Кондитерская-кофейня «Раскоша 1795» входит в состав ГГТУП «Лимож». Закажите столик или несколько столиков, выбрав меню,  и наш специалист свяжется с вами.',
    'text3'   => 'Типы столиков',
    'text4'   => 'Популярные блюда',
    'text5'   => 'Хочу заказать',
    'text6'   => 'Сделать заказ',
    'text7'   => '',
    'text8'   => '',
    'text9'   => 'На главную страницу',
    'cart'   => 'Корзина',
    'search_images'   => 'Поиск ...',
    'all_stock' => 'Все категории',
    'about_us'   => 'О нас',
    'pricing'   => 'Цены',
    'contact'   => 'Связаться с нами',
    'language_of_the_site'   => 'Язык',
    'legal_information'   => 'Правовая информация',
    'license_agreement'   => 'В состав ГГТУП «Лимож» входят',
    'our_production'   => 'Наша продукция',
    'privacy_policy'   => 'Политика приватности',
    'terms_of_use'   => 'Условия использования',
    'read_us'   => 'Читайте нас',
    'email_us'   => 'Отправить Email',
    'home' => 'Главная'

];
