<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart_items' => 'Элементы корзины',
    'payment' => 'Оплата',
    'receipt' => 'Квитанция',
    'product' => 'Продукт',
    'size' => 'Размер',
    'quantity' => 'Количество',
    'price' => 'Цена',
    'total' => 'Всего',
    'shop_more' => 'Купить еще',
    'proceed_to_payment' => 'Перейти к оплате',
    'discount' => 'Скидка',
    'no_items' => 'Корзина пуста!',
    'order_completed' => 'Ваш заказ завершен!',
    'name' => 'Имя',
    'email' => 'E-mail',
    'phone' => 'Телефон',
    'country' => 'Страна',
    'city' => 'Город',

    'order_details' => 'Информация для заказа',
    'order_no' => '№ заказа',
    'trans_id' => 'ID транзакции',
    'order_date' => 'Дата заказа',
    'payment_details' => 'Детали оплаты',
    'amount' => 'Количество',
    'cart_details' => 'Информация о корзине',
    'items_in_cart' => 'Товары в корзине',
    'print' => 'Печать'
];
