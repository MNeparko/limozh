<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart_items' => 'Einkaufswagen Artikel',
    'payment' => 'Zahlung',
    'receipt' => 'Quittung',
    'product' => 'Produkt',
    'size' => 'Größe',
    'quantity' => 'Menge',
    'price' => 'Preis',
    'total' => 'Gesamt',
    'shop_more' => 'Kaufe mehr',
    'proceed_to_payment' => 'Weiter zur Zahlung',
    'discount' => 'Rabatt',
    'no_items' => 'Keine Artikel im Warenkorb!',
    'order_completed' => 'Ihre Bestellung ist abgeschlossen!',
    'name' => 'Name',
    'email' => 'E-mail',
    'phone' => 'Telefon',
    'country' => 'Land',
    'city' => 'Stadt',


    'order_details' => 'Bestelldetails',
    'order_no' => 'Best.-Nr',
    'trans_id' => 'Transaktions-ID',
    'order_date' => 'Auftragsdatum',
    'payment_details' => 'Zahlungsdetails',
    'amount' => 'Menge',
    'cart_details' => 'Warenkorb Details',
    'items_in_cart' => 'Artikel im Warenkorb',
    'print' => 'Drucken'
];
