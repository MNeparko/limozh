<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'stock'   => 'Titel',
    'photo_detail'   => 'Foto-Detail',
    'image' => 'bild',
    'uploaded_date'   => 'Hochgeladenes Datum',
    'collection'   => 'Kategorie',
    'keywords'   => 'Tags',
    'sizes'   => 'Größe',
    'download_image_string'   => 'Laden Sie ein einzelnes Bild basierend auf der Größe herunter',
    'download_photo'   => 'Foto herunterladen',
    'same_photos'   => 'Ähnliche Bilder:',
    'download_preview'   => 'Herunterladen',
    'find_similar'   => 'Finde es ähnlich',
    'standard_license' => 'standard',
    'full_license' => 'erweiterte',
    'read_license' => 'Lesen Sie die Lizenz vor dem Download',
    'share_facebook' => 'Auf Facebook teilen',
    'share_twitter' => 'Auf Twitter teilen',
    'share_pinterest' => 'Auf Pinterest teilen',
    'close' => 'schließen',
    'download' => 'herunterladen foto',
    'similar_photos' => 'Angesehene Bilder:',
    'purchase' => 'In den Korb',
    'preview' => 'Vorschau',
];
