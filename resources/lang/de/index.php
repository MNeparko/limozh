<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'text1'   => 'Bestellen Sie einen Tisch in Raskosha 1795',
    'text2'   => 'Konditorei-Kaffeehaus "Raskosha 1795" ist ein Teil von GGTUP "Limoges". Bestellen Sie einen Tisch oder mehrere Tische, wählen Sie ein Menü und unser Spezialist wird sich mit Ihnen in Verbindung setzen.',
    'text3'   => 'Arten von Tabellen',
    'text4'   => 'Beliebte Gerichte',
    'text5'   => 'Ich möchte bestellen',
    'text6'   => 'Bestellen',
    'text7'   => '',
    'text8'   => '',
    'text9'   => 'Zur Hauptseite',
    'cart'   => 'Korb',
    'search_images'   => 'Suche products...',
    'all_stock' => 'Alle Kategorien',
    'about_us'   => 'Über uns',
    'pricing'   => 'Preisliste',
    'contact'   => 'Kontaktieren Sie uns',
    'language_of_the_site'   => 'Sprache',
    'legal_information'   => 'Rechtliche Informationen',
    'license_agreement'   => 'Das GGTUP "Limoges" beinhaltet',
    'our_production'   => 'Unsere Produkte',
    'privacy_policy'   => 'Datenschutzrichtlinie',
    'terms_of_use'   => 'Nutzungsbedingungen',
    'read_us'   => 'Lesen sie uns',
    'email_us'   => 'E-Mail uns bei',
    'home' => 'Start'
];
