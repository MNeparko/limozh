<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'keyword'   => 'Tags',
    'refine_result'   => 'Ergebnis verfeinern',
    'image_type'   => 'Bildtyp',
    'orientation'   => 'Orientierung',
    'image_not_found' => 'Bilder nicht gefunden',
    'search_results' => 'Suchergebnis:'
];
