<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart_items'   => 'Einkaufswagen Artikel',
    'payment' => 'Zahlung',
    'receipt' => 'Quittung',
    'checkout_step_2'   => 'Checkout - Schritt 2',
    'order_details'   => 'Bestelldetails',

    'form' => [
        'first_name' => 'Vorname: *',
        'last_name' => 'Familienname, Nachname: *',
        'company' => 'Firmenname (Site): ',
        'zip' => 'Postleitzahl: ',
        'city' => 'Stadt: ',
        'email' => 'Email: *',
        'phone' => 'Telefon: '
    ],
    'i_have_read_1' => 'Ich habe die Einkaufsbedingungen sowie die Zahlungsbedingungen gelesen und',
    'i_have_read_2' => ' akzeptiert',
    'i_have_read_3' => ', die ',
    'i_have_read_4' => 'Geschäftsbedingungen sind mir auch klar',
    'subscribe' => 'Abonnieren Sie spannende Newsletter und tolle Tipps',
    'choose_payment' => 'Wählen Sie die Zahlung',
    'credit_cart' => 'Mit Kreditkarte bezahlen',
    'purchase' => "Verwenden Sie diese Art der Zahlung jedes Mal. Nutzen Sie auch Ihre mobilen Apps für die Bezahlung. Diese Zahlungsarten ermöglichen es Ihnen, unsere Produkte im Internetshop viel schneller zu kaufen.",
    'bank_transfer' => 'Banküberweisung Zahlung',
    'bank_transfer2' => 'Sie können Geld direkt auf unser Bankkonto überweisen. Diese Zahlungsart wird empfohlen, wenn der Einkauf mehr als 500,00 € beträgt.',
    'please_allow' => 'Wir benötigen drei Werktage, um Ihre ',
    'online_account' => 'Online-Zahlung',
    'once_payment' => 'zu bestätigen. Sobald Ihre Zahlung bestätigt ist, erstellen wir Ihre E-Rechnung, die Sie in Ihrem Konto oder Ihrer E-Mail einsehen / ausdrucken können.',
    'back_to_cart' => 'Zurück zum Warenkorb',
    'checkout' => 'Auschecken'
];
