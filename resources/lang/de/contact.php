<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'contact'   => 'Kontaktieren Sie uns',
    'text' => 'Wenn Sie irgendwelche Fragen haben, können Sie sie fragen, indem Sie das folgende Formular ausfüllen',
    'form' => [
        'name' => 'Dein Name',
        'email' => 'Deine E-Mail',
        'subject' => 'Gegenstand',
        'message' => 'Botschaft',
        'required' => 'erforderlich',
        'enter_name' => 'Bitte tragen Sie Ihren vollen Namen ein *',
        'enter_email' => 'Bitte geben Sie ihre E-Mail-Adresse ein *',
        'enter_subject' => 'Bitte geben Sie Ihren Betreff ein *',
        'enter_message' => 'Nachricht für mich *'
    ],
    'send_button' => 'Nachricht senden',
    'required' => 'Diese Felder sind erforderlich.'
];
