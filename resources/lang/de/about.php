<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title'   => 'Über uns',
    'head'   => 'Das Grundprinzip unserer Website ist Einfachheit. Schau einfach rein, und lade herunter, was Dir gefällt! Unsere Seite ist überaus praktisch und unterhaltsam. Und das Schöne ist, dass alles nur mit wenigen Klicks gemacht werden kann. Ohne Probleme und große Herausforderungen. Und, stimmt‘s? Es klingt doch großartig! Diese Seite bietet neue und außergewöhnliche Perspektiven auf unsere spannende Welt! Wir bieten individuelle und einzigartige Blickwinkel, die die unbegrenzte Geschichtenvielfalt unseres Lebens widerspiegeln. Hier, wie auf einem Basar, gibt es vor allem eine großartige Vielfalt und das wird deine Neugierde stillen, neue und spannende Momente zu erleben. Aber nun genug über uns und bilde dir selbst eine Meinung. Bevor du zu einem Ergebnis kommst, werfe einen Blick auf die Seiten unserer Fotobase. Werde unser Komplize und schaue dir das Leben mit unseren Augen an.',

];
