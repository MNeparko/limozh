<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart_items' => 'Cart items',
    'payment' => 'Payment',
    'receipt' => 'Receipt',
    'product' => 'Product',
    'size' => 'Size',
    'quantity' => 'Quantity',
    'price' => 'Price',
    'total' => 'Total',
    'shop_more' => 'Shop more',
    'proceed_to_payment' => 'Proceed to payment',
    'discount' => 'Discount',
    'no_items' => 'No items in Cart!',
    'order_completed' => 'Your order is completed!',
    'name' => 'Name',
    'email' => 'E-mail',
    'phone' => 'Phone',
    'country' => 'Country',
    'city' => 'City',

    'order_details' => 'Order details',
    'order_no' => 'Order no',
    'trans_id' => 'Transaction ID',
    'order_date' => 'Order date',
    'payment_details' => 'Payment details',
    'amount' => 'Amount',
    'cart_details' => 'Cart details',
    'items_in_cart' => 'Items in cart',
    'print' => 'Print'
];
