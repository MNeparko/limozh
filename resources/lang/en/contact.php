<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'contact'   => 'Contact us',
    'text' => 'If you have any questions, you can ask them by filling in the form below',
    'form' => [
        'name' => 'Your Name',
        'email' => 'Your Email',
        'subject' => 'Subject',
        'message' => 'Message',
        'required' => 'required',
        'enter_name' => 'Please enter your full name *',
        'enter_email' => 'Please enter your email *',
        'enter_subject' => 'Please enter your Subject *',
        'enter_message' => 'Message for me *'
    ],
    'send_button' => 'send message',
    'required' => 'These fields are required.'

];
