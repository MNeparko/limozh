<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'stock'   => 'Name',
    'photo_detail'   => 'Photo detail',
    'image' => 'image',
    'uploaded_date'   => 'Uploaded date',
    'collection'   => 'Collection',
    'keywords'   => 'Tags',
    'sizes'   => 'Sizes',
    'download_image_string'   => 'Download a single image based on size',
    'download_photo'   => 'Download photo',
    'same_photos'   => 'Similar Images:',
    'download_preview'   => 'Download preview',
    'find_similar'   => 'Find similar',
    'standard_license' => 'standard',
    'full_license' => 'extended',
    'read_license' => 'Read the license before downloading',
    'share_facebook' => 'Share on facebook',
    'share_twitter' => 'Share on twitter',
    'share_pinterest' => 'Share on pinterest',
    'close' => 'close',
    'download' => 'download photo',
    'similar_photos' => 'Viewed Images:',
    'purchase' => 'Add to Cart',
    'preview' => 'Preview',
];
