<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'cart_items'   => 'Cart items',
    'payment' => 'Payment',
    'receipt' => 'Receipt',
    'checkout_step_2'   => 'Checkout - Step 2',
    'order_details'   => 'Order details',

    'form' => [
        'first_name' => 'First name: *',
        'last_name' => 'Last name: *',
        'company' => 'Company name (site):',
        'zip' => 'Zip code: ',
        'city' => 'City: ',
        'email' => 'Email: *',
        'phone' => 'Phone: '
    ],
    'i_have_read_1' => ' I have read and accepted the   ',
    'i_have_read_2' => 'terms',
    'i_have_read_3' => ', of purchasing as well as the the terms of payment. ',
    'i_have_read_4' => 'Business conditions are also clear to me',
    'subscribe' => 'Subscribe to exciting newsletters and great tips',
    'choose_payment' => 'Choose payment',
    'credit_cart' => 'Pay by credit card',
    'purchase' => "Use this way of payment every  time. Also use your mobile apps for payment. These ways of payment will allow you to purchase our products in internet shop  much quicker.",
    'bank_transfer' => 'Bank transfer payment',
    'bank_transfer2' => 'You can transfer money directly to our bank account. This way of payment is recommended when the purchase is over 500,00 €.',
    'please_allow' => 'We need three working days to confirm your ',
    'online_account' => 'online payment',
    'once_payment' => 'As soon as your payment is confirmed we will generate your e-invoice which you can view/print from your account or e-mail.',
    'back_to_cart' => 'Back to cart',
    'checkout' => 'Checkout'
];
