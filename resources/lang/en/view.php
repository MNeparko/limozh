<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'keyword'   => 'Tags',
    'refine_result'   => 'Refine result',
    'image_type'   => 'Image type',
    'orientation'   => 'Orientation',
    'image_not_found' => 'Images not found',
    'search_results' => 'Search results:'
];
