<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title'   => 'About us',
    'head'   => 'The basic principle of our site is  its simplicity. Just have a  look at its pages and  download what appeals to you. Convenient and really simple. And all in a few clicks. Without problems : looking through and making your choice.  True, great! We exibit the world around from our point of view . Every image is individual and unique, and the plot diversity of our life is unlimited. When you look through the stock you really feel yourself as at the supermarket – a bazaar – you can always come across something that appeals to you. Good or bad – it is up to you to decide. But before you come to any conclusion, look through our stock once again. You are welcome to be our partner and appreciate the life events and the world around you with our help.',

];
