<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'text1'   => 'Order a table in Raskosha 1795',
    'text2'   => 'Confectionery-coffee house "Raskosha 1795" is a part of GGTUP "Limoges". Order a table or several tables, choosing a menu, and our specialist will contact you.',
    'text3'   => 'Types of tables',
    'text4'   => 'Popular dishes',
    'text5'   => 'I want to order',
    'text6'   => 'Make an order',
    'text7'   => '',
    'text8'   => '',
    'text9'   => 'to Home Page',
    'cart'   => 'Cart',
    'search_images'   => 'Search products...',
    'all_stock' => 'All stock',
    'about_us'   => 'About us',
    'pricing'   => 'Pricing',
    'contact'   => 'Contact',
    'language_of_the_site'   => 'Language',
    'legal_information'   => 'Legal information',
    'license_agreement'   => 'The GGTUP "Limoges" includes',
    'our_production'   => 'Our production',
    'privacy_policy'   => 'Privacy policy',
    'terms_of_use'   => 'Terms of use',
    'read_us'   => 'Read us',
    'email_us'   => 'Email us at',
    'home' => 'Home'
];
