<h1>New message from contact form!</h1>
<p><b>Email:</b> @if(!empty($email)) {{ $email }} @else Not found @endif</p>
@if(!empty($subject))<p><b>Subject:</b>  {!! $subject !!} </p>@endif
@if(!empty($name))<p><b>Name:</b>  {!! $name !!} </p>@endif

@if(!empty($text))<p><b>Message:</b>  {!! $text !!} </p>@endif