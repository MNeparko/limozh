@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        {{ Form::open(['method' => 'get', 'route' => 'site.search']) }}


        <div class="breadcrumb-wrapper breadcrumb-form">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-6 mb-20-sm">

                        <div class="input-group-search-form-wrapper">

                            <div class="input-group bg-change-focus-addclass">

                                <input name="search" type="text" class="form-control" placeholder="{!! trans('view.keyword') !!}" >
                                {{ Form::hidden('dropdownValue',null) }}
                                {{ csrf_field() }}
                                <div class="input-group-btn dropdown-select">
                                    <div class="dropdown dropdown-select">
                                        <button class="btn dropdown-toggle" type="button" id="mainSearchDropdown" data-category-id="0" data-toggle="dropdown" aria-expanded="true">
                                            {!! trans('index.all_stock') !!}
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" aria-labelledby="mainSearchDropdown">
                                            <li class="category-search" data-category-id="0" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">{!! trans('index.all_stock') !!}</a></li>
                                            @foreach($allCategories as $categoryItem)
                                                <li class="category-search" data-category-id="{{ $categoryItem->id }}" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">{{ $category->translationName(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationName(\App\Models\Translation::currentLang())->value }}</a></li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div><!-- /btn-group -->

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary buttonSearch"><i class="fa fa-search"></i></button>
                                </div>

                            </div><!-- /input-group -->

                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">

                        <div class="row gap-15">



                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-2">

                        <div class="text-right">
                            <span class="btn btn-toggle btn-refine collapsed" data-toggle="collapse" data-target="#refine-result">{!! trans('view.refine_result') !!}</span>
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <div class="filter-wrapper">

            <div id="refine-result" class="collapse">

                <div class="container">

                    <div class="collapse-inner clearfix">

                        <div class="row gap-15">



                            <div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">
                                <div class="form-group">
                                    <select name="image_type[]" id="filter_image_type" class="selectpicker show-tick form-control" title="{!! trans('view.image_type') !!}" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" multiple>
                                        <option value="0">JPG</option>
                                        <option value="1">PNG</option>
                                        <option value="2">TIFF</option>
                                        <option value="3">GIF</option>
                                    </select>
                                </div>
                            </div>

                            {{--<div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">--}}

                            {{--<div class="form-group">--}}
                            {{--<select id="filter_image_size" class="selectpicker show-tick form-control" title="Image Size" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" multiple>--}}
                            {{--<option value="0">S (0.5mpx)</option>--}}
                            {{--<option value="1">M (2mpx)</option>--}}
                            {{--<option value="2">L (6mpx)</option>--}}
                            {{--<option value="3">XL (12mpx)</option>--}}
                            {{--<option value="4">XXL (20mpx)</option>--}}
                            {{--</select>--}}
                            {{--</div>--}}

                            {{--</div>--}}

                            <div class="col-xss-12 col-xs-6 col-sm-4 col-md-2">

                                <div class="form-group">
                                    <select name="image_orientation[]" id="filter_image_orientation" class="selectpicker show-tick form-control" title="{!! trans('view.orientation') !!}" data-selected-text-format="count > 3" data-done-button="true" data-done-button-text="OK" multiple>
                                        <option value="0">Landscape</option>
                                        <option value="1">Portrait</option>
                                        <option value="2">Square</option>
                                    </select>
                                </div>

                            </div>


                        </div>

                    </div>
                </div>
            </div>

        </div>

        {{ Form::close() }}

        <div class="content-wrapper">

            <div class="container">

                <div class="section-sm">

                    <div class="section-title mb-30">


                        <p class="uppercase"><span class="font700 mr-10"> {{ trans('category.view.name_category') }}: </span> {{ $category->translationName(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationName(\App\Models\Translation::currentLang())->value }}</p>
                    </div>

                    {{--<div class="filter-sm-wrapper">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-xs-12 col-sm-3 col-md-4 mb-10">--}}

                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-5 col-md-4 mb-10">--}}

                            {{--</div>--}}
                            {{--<div class="col-xs-12 col-sm-4 col-md-4 mb-10">--}}
                                {{--<ul class="filter-paging pull-right mt">--}}
                                    {{--<li class="btn-wrapper">--}}
                                        {{--<div class="input-group input-group-sm">--}}
                                            {{--<span class="input-group-addon">Sort by:</span>--}}
                                            {{--<select class="selectpicker show-tick form-control" title="Sort by">--}}
                                                {{--<option value="upload">upload</option>--}}
                                                {{--<option value="size">size</option>--}}
                                                {{--<option value="download">download</option>--}}
                                            {{--</select>--}}
                                        {{--</div>--}}
                                    {{--</li>--}}

                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}


                        @if($photos->count())
                        <div class="flex-images flex-image category-item-wrapper posts endless-pagination" data-next-page="{{ $photos->nextPageUrl() }}">
                            @include('site.category.data')
                            {{--{!! $posts->render() !!}--}}
                        </div>

                        @else
                            <h2>{!! trans('view.image_not_found') !!}</h2>
                        @endif


                </div>

            </div>

        </div>




        <div class="row gap-0">
            <div class="col-md-10 col-md-offset-1">

                <div class="section-title-special mb-30">
                    {!! $category->translationDescription(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationDescription(\App\Models\Translation::currentLang())->value !!}
                </div>

            </div>
        </div>



    </div>



@endsection
