@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        <div class="image-bg-wrapper about-us-page-title" style="background-image: url('images/Garland_pattern960.jpg');background-color: rgba(97, 97, 97, .5);background-size: 30%;">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12 col-md-12">

                        <div class="section-title custom-text">
                        @if(!empty($aboutFirst))
                            {!! $aboutFirst->value !!}
                        @endif
                        </div>

                    </div>

                </div>

            </div>

        </div>

        <section class="section pb-60">

            <div class="container">

                <div class="row">

                    <div class="col-sm-12 col-md-6 mb-30">

                        <img src="images/about-us-page.jpg" alt="Images" />

                    </div>

                    <div class="col-sm-12 col-md-6 big-text">

                        @if(!empty($aboutSecond))
                            {!! $aboutSecond->value !!}
                        @endif

                    </div>

                </div>

            </div>

        </section>

    </div>
@endsection