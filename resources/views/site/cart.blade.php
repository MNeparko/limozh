@extends('layouts.app')

@section('content')


    @if(Session::has('cart'))
    <div class="step-wrapper">
        <div class="container">

            <div class="stepper">
                <ul class="row">
                    <li class="col-md-4 active">
                        <span data-text="{!! trans('cart.cart_items') !!}"></span>
                    </li>
                    <li class="col-md-4">
                        <span data-text="{!! trans('cart.payment') !!}"></span>
                    </li>
                    <li class="col-md-4">
                        <span data-text="{!! trans('cart.receipt') !!}"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <section class="checkout">

            @if ($message = Session::get('error'))
                <div class="alert alert-error">
                    <p>{{ $message }}</p>
                </div>
            @endif
        <div class="container">

            <header class="hidden">
                <h3 class="h3 title">Checkout - Step 1</h3>
            </header>

            <!-- ========================  Cart wrapper ======================== -->

            <div class="cart-wrapper">
                <!--cart header -->

                <div class="cart-block cart-block-header clearfix">
                    <div>
                        <span>{!! trans('cart.product') !!}</span>
                    </div>
                    <div>
                        <span>&nbsp;</span>
                    </div>
                    <div>
                        <span>{!! trans('cart.size') !!}</span>
                    </div>
                    <div>
                        <span>{!! trans('cart.quantity') !!}</span>
                    </div>
                    <div class="text-right">
                        <span>{!! trans('cart.price') !!}</span>
                    </div>
                </div>

                <!--cart items-->

                <div class="clearfix">

                    @php

                        $priceSum = 0;
                        $priceTrySum = 0;
                    @endphp

                    @foreach($items as $keySize => $size)

                        @foreach($size as $keyPhoto => $photo)
                            @if(!is_numeric($keyPhoto))
                                @continue
                            @endif

                                <div class="cart-block cart-block-item clearfix">
                                    <div class="image" style="width: 8%">
                                        <a href="#"><img src="{{ $photo['item']->getUrlThumbImage() }}" alt="" /></a>
                                    </div>

                                    <div class="title" style="width: 40%">
                                        <div class="h4"><a href="{{ route('site.photo.detail', ['id' => $keyPhoto]) }}">{{ $photo['item']->translationTitle(\App\Models\Translation::currentLang()) == null ? $photo['item']['title'] : $photo['item']->translationTitle(\App\Models\Translation::currentLang())->value}}</a></div>

                                    </div>
                                    <div class="title" style="width: 10%">
                                        <div class="h4">{{ $sizes[$keySize] }}</div>
                                    </div>
                                    <div class="title" style="width: 5%">
                                        <div class="h4">{{ $photo['qty'] }}</div>
                                    </div>
                                    <div class="price">
                                        <span class="final h3">{{ $photo['price'] }} €</span>

                                            {{--<span class="discount"> €</span>--}}

                                    </div>
                                    <!--delete-this-item-->
                                    <a href="{{ route('site.cart.deleteItem', ['sizeId' => $keySize, 'photoId' => $keyPhoto]) }}">
                                        <span class="icon icon-cross icon-delete"></span>
                                    </a>
                                </div>
                        @endforeach
                    @endforeach

                </div>

                <!--cart prices -->
                @if($totalDiscountPrice > 0)
                <div class="clearfix">
                    <div class="cart-block cart-block-footer clearfix">
                        <div>
                            @php

                                $percent = ($totalDiscountPrice/$totalPrice)*100;
                                //$discount = $priceSum-$totalPrice;
                                $discount = $totalPrice;
                            @endphp
                            <strong>{!! trans('cart.discount') !!} {{ $percent }}%</strong>
                        </div>
                        <div>
                            <span><del>{{ $discount }} €</del> </span>
                        </div>
                    </div>

                </div>
                @endif
                <!--cart final price -->

                <div class="clearfix">
                    <div class="cart-block cart-block-footer cart-block-footer-price clearfix">
                        <div>
                            <div class="h2 title">{!! trans('cart.total') !!}:</div>
                        </div>
                        <div>
                            <div class="h2 title">{{ $totalResultPrice }} €</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========================  Cart navigation ======================== -->

            <div class="clearfix">
                <div class="row">
                    <div class="col-xs-6">
                        <a href="{{ route('site.index') }}" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> {!! trans('cart.shop_more') !!}</a>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="{{ route('addmoney.checkout')}}" class="btn btn-main"><span class="icon icon-cart"></span> {!! trans('cart.proceed_to_payment') !!}</a>
                    </div>
                </div>
            </div>

        </div> <!--/container-->

    </section>
    @else
        <div style="margin-top: 5%; margin-bottom: 5%;" class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <h2>{!! trans('cart.no_items') !!}</h2>
            </div>
        </div>
    @endif


@endsection