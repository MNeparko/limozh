@extends('layouts.app')

@section('content')
    <div class="container">
        <br><br><br>
    @if(Session::has('cart'))
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <ul class="list-group">

                    @foreach($items as $keyPrice => $price)
                        <h4>{{ $prices[$keyPrice-1]->title.'/'.$prices[$keyPrice-1]->short_resolution }}</h4>
                        <hr>
                        @foreach($price as $keyPhoto => $photo)
                            @if(!is_numeric($keyPhoto))
                                @continue
                            @endif
                        <li class="list-group-item">
                            <span class="badge">{{ $photo['qty'] }}</span>
                            <strong><a href="{{ route('product', ['id' => $keyPhoto]) }}">{{ $photo['item']['title'] }}</a></strong>
                            <span class="label label-success">${{ $photo['price'] }}</span>
                            <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-xs dropdown-toggle"
                                data-toggle="dropdown">Action <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="{{ route('site.cart.minusItem', ['priceId' => $keyPrice, 'photoId' => $keyPhoto]) }}">Minus item</a></li>
                                    <li><a href="{{ route('site.cart.deleteItem', ['priceId' => $keyPrice, 'photoId' => $keyPhoto]) }}">Delete item</a></li>
                                    <li><a href="{{ route('site.cart.clear') }}">Clear cart</a></li>
                                </ul>
                            </div>
                        </li>
                        @endforeach

                        <strong>Total:
                            @if($price['try_sum'] < $price['sum'])
                                <s>${{ $price['sum'] }}</s>
                                <b class="discount_price">${{ $price['try_sum'] }}</b>
                            @else
                                ${{ $price['sum'] }}
                            @endif
                        </strong>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                <strong class="total_price">Total: ${{ $totalPrice }}</strong>
            </div>
        </div>
            <hr>
            <div class="row">
                <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                    <a href="{{ route('site.checkout') }}" type="button" class="btn btn-success">Checkout</a>
                </div>
            </div>
    @else

            <div class="row" style="margin-top: 200px;">
                <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                    <h2>No items in Cart!</h2>
                </div>
            </div>
    @endif
    </div>
    <br>
    <br>
@endsection