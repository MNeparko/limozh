<div class="modal fade" id="createOrder">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['url' => 'order/create', 'files' => true, 'class' => 'form-horizontal']) !!}

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h2 class="modal-title">{!! trans('index.text6') !!}</h2>
            </div>

            <div class="modal-body">
                <div class="form-group">
                    {{ Form::label('name', 'Имя и фамилия', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::text('name', '', ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('phone', 'Телефон', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::text('phone', '', ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('email', 'Email', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::text('email', '', ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="date-params">
                    <div class="form-group">
                        {{ Form::label('reservation_date','Дата и время брони', ['class' => 'control-label col-xs-3']) }}

                        <div class="col-xs-3">
                            <div class='input-group date' id='createOrderDatePicker'>
                                {{ Form::text('reservation_date', '', ['id' => 'datepicker', 'class' => 'form-control']) }}
                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-calendar"></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-xs-3">
                            <div class='input-group date' id='createOrderTimePicker'>
                                {{ Form::text('reservation_time', '', ['class' => 'form-control']) }}

                                <span class="input-group-addon">
                                    <span class="glyphicon glyphicon-time"></span>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('count','Кол-во человек (max - ' . $maxPlaces . ')', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::text('count', '', ['class' => 'form-control']) }}
                    </div>
                </div>

                <div class="form-group">
                    {{ Form::label('products[]', 'Блюда/Напитки:', ['class' => 'control-label col-xs-3']) }}
                    <div class="col-xs-9">
                        {{ Form::select('products[]', $products, null, [
                            'multiple' => true,
                            'class' => 'selectpicker form-control',
                            'title' => 'Нажмите для выбора',
                            'data-width' => 'fit'
                        ]) }}
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="submit" class="btn btn-success">Заказать</button>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">