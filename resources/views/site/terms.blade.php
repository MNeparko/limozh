@extends('layouts.app')

@section('content')
    <div class="main-wrapper scrollspy-container">

        <div class="breadcrumb-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">{!! trans('index.terms_of_use') !!}</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a></li>
                            <li class="active">{!! trans('index.terms_of_use') !!}</li>
                        </ol>
                    </div>

                </div>

            </div>

        </div>

        <div class="content-wrapper pt-50 pb-50">

            <div class="container">

                <div class="row">



                    <div class="col-xs-12 col-md-12">

                        <div class="static-wrapper">

                            @if(!empty($terms))
                                {!! $terms->value !!}
                            @endif
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection