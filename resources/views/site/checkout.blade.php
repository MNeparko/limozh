@extends('layouts.app')

@section('content')
    <div class="wrapper">


        <!-- ========================  Step wrapper ======================== -->

        <div class="step-wrapper">
            <div class="container">

                <div class="stepper">
                    <ul class="row">
                        <li class="col-md-4 active">
                            <span data-text="{!! trans('checkout.cart_items') !!}"></span>
                        </li>
                        <li class="col-md-4 active">
                            <span data-text="{!! trans('checkout.payment') !!}"></span>
                        </li>
                        <li class="col-md-4">
                            <span data-text="{!! trans('checkout.receipt') !!}"></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <!-- ========================  Checkout ======================== -->

        <section class="checkout">
            <div class="container">

                <header class="hidden">
                    <h3 class="h3 title">{!! trans('checkout.checkout_step_2') !!}</h3>
                </header>



                <!-- ========================  Payment ======================== -->

                <div class="cart-wrapper">

                    <div class="note-block">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {{ Form::open(['method' => 'post', 'route' => 'addmoney.paypal', 'id' => 'checkoutForm']) }}
                        {{ csrf_field() }}
                        <div class="row">
                            <!-- === left content === -->

                            <div class="col-md-6">

                                <div class="white-block">

                                    <div class="h4">{!! trans('checkout.order_details') !!}</div>

                                    <hr>

                                    <div class="row">

                                        <div class="login-block login-block-signup">





                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{--<input type="text" name="first_name" value="" class="form-control" placeholder="First name: *">--}}
                                                    {{ Form::text('first_name', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.first_name'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::text('last_name', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.last_name'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-12">

                                                <div class="form-group">
                                                    {{ Form::text('company', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.company'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    {{ Form::text('zip', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.zip'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-8">
                                                <div class="form-group">
                                                    {{ Form::text('city', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.city'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::text('email', null, ['class' => 'form-control',
                                                    'placeholder' => 'Email: *',
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    {{ Form::text('phone', null, ['class' => 'form-control',
                                                    'placeholder' => trans('checkout.form.phone'),
                                                    ]) }}
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <hr>
                                                <span class="checkbox">
                                                        <input name="terms" type="checkbox" id="checkBoxId1" style="display: none">
                                                        <label for="checkBoxId1">{!! trans('checkout.i_have_read_1') !!}<a href="http://Limoges.tw1.su/checkout-2.html#">{!! trans('checkout.i_have_read_2') !!}</a>{!! trans('checkout.i_have_read_3') !!}<a href="http://Limoges.tw1.su/checkout-2.html#">{!! trans('checkout.i_have_read_4') !!}</a></label>
                                                    </span>
                                                <span class="checkbox">
                                                        <input name="subscribe" type="checkbox" id="checkBoxId2">
                                                        <label for="checkBoxId2">{!! trans('checkout.subscribe') !!}</label>
                                                    </span>
                                                <hr>
                                            </div>


                                        </div> <!--/signup-->


                                    </div>


                                </div> <!--/col-md-6-->

                            </div>

                            <!-- === right content === -->

                            <div class="col-md-6">
                                <div class="white-block">

                                    <div class="h4">{!! trans('checkout.choose_payment') !!}</div>

                                    <hr>

                                    <span class="checkbox">
                                        <input value="1" type="radio" id="paymentID1" name="paymentOption" checked="checked">
                                        <label for="paymentID1">
                                            <strong>{!! trans('checkout.credit_cart') !!}</strong> <br>
                                            <small>(MasterCard, Maestro, Visa, Visa Electron, JCB and American Express)</small>
                                        </label>
                                    </span>

                                    <span class="checkbox">
                                        <input value="2" type="radio" id="paymentID2" name="paymentOption">
                                        <label for="paymentID2">
                                            <strong>PayPal</strong> <br>
                                            <small>{!! trans('checkout.purchase') !!}</small>
                                        </label>
                                    </span>

                                    <span class="checkbox">
                                        <input value="3" type="radio" id="paymentID3" name="paymentOption">
                                        <label for="paymentID3">
                                            <strong>{!! trans('checkout.bank_transfer') !!}</strong> <br>
                                            <small>{!! trans('checkout.bank_transfer2') !!}</small>
                                        </label>
                                    </span>

                                    <hr>

                                    <p>{!! trans('checkout.please_allow') !!} <a href="http://Limoges.tw1.su/checkout-3.html#">{!! trans('checkout.online_account') !!}</a>. {!! trans('checkout.once_payment') !!}</p>
                                </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>



                <!-- ========================  Cart navigation ======================== -->

                <div class="clearfix">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{ route('site.photo.cart') }}" class="btn btn-clean-dark"><span class="icon icon-chevron-left"></span> {!! trans('checkout.back_to_cart') !!}</a>
                        </div>
                        <div class="col-xs-6 text-right">
                            <button class="btn btn-main" id="checkoutFormSubmit">
                                <span class="icon icon-cart"></span>
                                <span class="submit-text">{!! trans('checkout.checkout') !!}</span>
                            </button>
                            {{--</span><input type="submit" class="btn btn-main icon icon-cart" value="Checkout">--}}
                            {{--<a href="{{ route('addmoney.paypal') }}" class="btn btn-main"><span class="icon icon-cart"></span> Checkout</a>--}}
                        </div>
                    </div>
                </div>


            </div> <!--/container-->

        </section>


    </div>

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        $("#checkoutFormSubmit").bind('click', function(event) {
            $("#checkoutForm").submit();
        });
    </script>
@endsection