@extends('layouts.app')

@section('content')
    <div class="step-wrapper">
        <div class="container">

            <div class="stepper">
                <ul class="row">
                    <li class="col-md-4 active">
                        <span data-text="{!! trans('cart.cart_items') !!}"></span>
                    </li>
                    <li class="col-md-4 active">
                        <span data-text="{!! trans('cart.payment') !!}"></span>
                    </li>
                    <li class="col-md-4 active">
                        <span data-text="{!! trans('cart.receipt') !!}"></span>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- ========================  Checkout ======================== -->

    <section class="checkout">
        <div class="container">

            <header class="hidden">
                <h3 class="h3 title">Checkout - Step 4</h3>
            </header>

            <!-- ========================  Cart navigation ======================== -->

            <div class="clearfix">
                <div class="row">
                    <div class="col-xs-12">
                        <span class="h2 title">{!! trans('cart.order_completed') !!}</span>
                    </div>
                </div>
            </div>

            <!-- ========================  Payment ======================== -->

            <div class="cart-wrapper">

                <div class="note-block">

                    <div class="row">
                        <!-- === left content === -->

                        <div class="col-md-6">

                            <div class="white-block">

                                <div class="h4">Info</div>

                                <hr />

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.name') !!}</strong> <br />
                                            <span>{{ $payment_inf['name'] }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.email') !!}</strong><br />
                                            <span>{{ $payment_inf['email'] }}</span>
                                        </div>
                                    </div>
                                    @if(!empty($payment_inf['phone']))
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.phone') !!}</strong><br />
                                            <span>{{ $payment_inf['phone'] }}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($payment_inf['zip']))
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.country') !!}</strong><br />
                                            <span>{{ $payment_inf['zip'] }}</span>
                                        </div>
                                    </div>
                                    @endif
                                    @if(!empty($payment_inf['city']))
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.city') !!}</strong><br />
                                            <span>{{ $payment_inf['city'] }}</span>
                                        </div>
                                    </div>
                                    @endif



                                </div>

                            </div> <!--/col-md-6-->

                        </div>

                        <!-- === right content === -->

                        <div class="col-md-6">
                            <div class="white-block">

                                <div class="h4">{!! trans('cart.order_details') !!}</div>

                                <hr />

                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.order_no') !!}.</strong> <br />
                                            <span>{{ $payment_inf['order_id'] }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.trans_id') !!}</strong> <br />
                                            <span>{{ $payment_inf['transaction_id'] }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.order_date') !!}</strong> <br />
                                            <span>{{ $payment_inf['order_date'] }}</span>
                                        </div>
                                    </div>

                                </div>

                                <div class="h4">{!! trans('cart.payment_details') !!}</div>

                                <hr />

                                <div class="row">


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.amount') !!}</strong><br />
                                            <span>{{ $payment_inf['amount'] }} €</span>
                                        </div>
                                    </div>



                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.cart_details') !!}</strong><br />
                                            <span>{{ $payment_inf['cart_details'] }}</span>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <strong>{!! trans('cart.items_in_cart') !!}</strong><br />
                                            <span>{{ $payment_inf['qty'] }}</span>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========================  Cart wrapper ======================== -->

            <div class="cart-wrapper">
                <!--cart header -->

                <div class="cart-block cart-block-header clearfix">
                    <div>
                        <span>{!! trans('cart.product') !!}</span>
                    </div>
                    <div>
                        <span>&nbsp;</span>
                    </div>
                    <div>
                        <span>{!! trans('cart.size') !!}</span>
                    </div>
                    <div>
                        <span>{!! trans('cart.quantity') !!}</span>
                    </div>
                    <div class="text-right">
                        <span>{!! trans('cart.price') !!}</span>
                    </div>
                </div>

                <!--cart items-->

                <div class="clearfix">

                    @php
                        $priceSum = 0;
                        $priceTrySum = 0;
                    @endphp

                    @foreach($items as $keySize => $size)

                        @foreach($size as $keyPhoto => $photo)
                            @if(!is_numeric($keyPhoto))
                                @continue
                            @endif

                            <div class="cart-block cart-block-item clearfix">
                                <div class="image">
                                    <a href="#"><img src="{{ $photo['item']->getUrlImage() }}" alt="" /></a>
                                </div>
                                <div class="title" style="width: 40%">
                                    <div class="h4"><a href="{{ route('site.photo.detail', ['id' => $keyPhoto]) }}">{{ $photo['item']['title'] }}</a></div>
                                    <div>{{ $sizes[$keySize-1]->short_resolution }}</div>
                                </div>
                                <div class="title" style="width: 10%">
                                    <div class="h4">{{ $sizes[$keySize-1]->title }}</div>
                                </div>
                                <div class="title" style="width: 5%">
                                    <div class="h4">{{ $photo['qty'] }}</div>
                                </div>
                                <div class="price">
                                    <span class="final h3">{{ $photo['price'] }} €</span>
                                    {{--<span class="discount">6 €</span>--}}
                                </div>
                                <!--delete-this-item-->
                                <a href="{{ route('site.cart.deleteItem', ['sizeId' => $keySize, 'photoId' => $keyPhoto]) }}">
                                    <span class="icon icon-cross icon-delete"></span>
                                </a>
                            </div>
                        @endforeach
                        @if($size['try_sum'] < $size['sum'])
                            @php
                                $priceSum += $size['sum'];
                                $priceTrySum += $size['try_sum'];
                            @endphp

                        @else
                            @php
                                $priceSum += $size['sum'];
                            @endphp
                        @endif
                    @endforeach

                </div>

                <!--cart prices -->
                @if($priceTrySum > 0 && $priceTrySum < $priceSum)
                    <div class="clearfix">
                        <div class="cart-block cart-block-footer clearfix">
                            <div>
                                @php
                                    $percent = ceil((($priceSum-$priceTrySum)*100)/$priceSum);
                                    $discount = $priceSum-$priceTrySum;
                                @endphp
                                <strong>{!! trans('cart.discount') !!} {{ $percent }}%</strong>
                            </div>
                            <div>
                                <span>- {{ $discount }} €</span>
                            </div>
                        </div>

                    </div>
            @endif
            <!--cart final price -->

                <div class="clearfix">
                    <div class="cart-block cart-block-footer cart-block-footer-price clearfix">
                        <div>
                            <div class="h2 title">{!! trans('cart.total') !!}:</div>
                        </div>
                        <div>
                            <div class="h2 title">{{ $totalPrice }} €</div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ========================  Cart navigation ======================== -->

            <div class="clearfix">
                <div class="row">
                    <div class="col-xs-6">
                        <span class="h2 title" id="getLink" data-link="{{ route('site.test') }}">{!! trans('cart.order_completed') !!}</span>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a onclick="window.print()" class="btn btn-main"><span class="icon icon-printer"></span> {!! trans('cart.print') !!}</a>
                    </div>
                </div>
            </div>

        </div> <!--/container-->

    </section>

    </div> <!--/wrapper-->

    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function() {

            setTimeout(function(){
                var link = $('#getLink').data('link');
                window.location.replace(link);
            }, 1000);

        });

    </script>
@endsection