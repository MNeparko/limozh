@extends('layouts.app')

@section('content')
    <div class="main-wrapper">




        <div class="breadcrumb-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">{!! trans('contact.contact') !!}</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a></li>
                            <li class="active">{!! trans('contact.contact') !!}</li>
                        </ol>
                    </div>

                </div>

            </div>

        </div>

        <div class="content-wrapper pt-50 pb-60">

            <div class="container">

                <div class="row">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">

                        <div class="section-title">

                            <h2>{!! trans('contact.contact') !!}</h2>

                            <p>{!! trans('contact.text') !!}</p>

                        </div>

                    </div>

                </div>


                {{ Form::open(['method' => 'post', 'route' => 'site.message.send', 'id' => 'contact-form']) }}

                    <div class="row">

                        <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">

                            <div class="messages"></div>

                            <div class="controls">

                                <div class="row">

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="form_name">{!! trans('contact.form.name') !!} <span class="font10 text-danger">({!! trans('contact.form.required') !!})</span></label>
                                            <input id="form_name" type="text" name="name" class="form-control" placeholder="{!! trans('contact.form.enter_name') !!}" required="required" data-error="Firstname is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label for="form_email">{!! trans('contact.form.email') !!} <span class="font10 text-danger">({!! trans('contact.form.required') !!})</span></label>
                                            <input id="form_email" type="email" name="email" class="form-control" placeholder="{!! trans('contact.form.enter_email') !!}" required="required" data-error="Valid email is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label>{!! trans('contact.form.subject') !!}</label>
                                            <input id="form_lastname" type="text" name="title" class="form-control" placeholder="{!! trans('contact.form.enter_subject') !!}" required="required" data-error="Subject is required.">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12">
                                        <div class="form-group">
                                            <label for="form_message">{!! trans('contact.form.message') !!} <span class="font10 text-danger">({!! trans('contact.form.enter_subject') !!})</span></label>
                                            <textarea id="form_message" name="message" class="form-control" placeholder="{!! trans('contact.form.enter_message') !!}" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 text-center">
                                        <input type="submit" class="btn btn-primary btn-send mt-10" value="{!! trans('contact.send_button') !!}">
                                    </div>

                                    <div class="col-md-12 text-center">
                                        <p class="text-muted font12 mt-20" style="line-height: 1.2;"><span class="font12 text-danger">*</span> {!! trans('contact.required') !!}</p>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                {{ Form::close() }}

            </div>

        </div>

        {{--<div class="bg-dark pt-70 pb-30">--}}

            {{--<div class="container">--}}

                {{--<div class="row">--}}

                    {{--<div class="col-xs-12 col-sm-6 col-md-3 mb-30">--}}

                        {{--<div class="contact-text-featured-item clearfix">--}}

                            {{--<div class="content">--}}

                                {{--<h5 class="text-white">Lorem Ipsum</h5>--}}
                                {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<span class="font500 mt-15 text-primary block">+12 34 5678 1234</span></p>--}}

                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}

                    {{--<div class="col-xs-12 col-sm-6 col-md-3 mb-30">--}}

                        {{--<div class="contact-text-featured-item clearfix">--}}

                            {{--<div class="content">--}}

                                {{--<h5 class="text-white">Lorem Ipsum</h5>--}}
                                {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<span class="font500 mt-15 text-primary block">+12 34 5678 1234</span></p>--}}

                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}

                    {{--<div class="col-xs-12 col-sm-6 col-md-3 mb-30">--}}

                        {{--<div class="contact-text-featured-item clearfix">--}}

                            {{--<div class="content">--}}

                                {{--<h5 class="text-white">Lorem Ipsum</h5>--}}
                                {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<span class="font500 mt-15 text-primary block">info@Limoges.by</span></p>--}}

                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}

                    {{--<div class="col-xs-12 col-sm-6 col-md-3 mb-30">--}}

                        {{--<div class="contact-text-featured-item clearfix">--}}

                            {{--<div class="content">--}}

                                {{--<h5 class="text-white">Lorem Ipsum</h5>--}}
                                {{--<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry--}}

                                    {{--<span class="font500 mt-15 text-primary block contact-social">--}}

											{{--<a href="contact.html#" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a>--}}
											{{--<a href="contact.html#" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a>--}}
											{{--<a href="contact.html#" data-toggle="tooltip" data-placement="top" title="Google Plus"><i class="fa fa-google-plus"></i></a>--}}
											{{--<a href="contact.html#" data-toggle="tooltip" data-placement="top" title="Pinterest"><i class="fa fa-pinterest"></i></a>--}}

										{{--</span>--}}

                                {{--</p>--}}

                            {{--</div>--}}

                        {{--</div>--}}

                    {{--</div>--}}

                {{--</div>--}}

            {{--</div>--}}

        {{--</div>--}}

    </div>
@endsection