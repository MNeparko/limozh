@extends('layouts.app')

@section('content')
    <!-- start Main Wrapper -->
    <div class="main-wrapper">

        <div class="breadcrumb-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">Pricing</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a></li>
                            <li class="active">Pricing</li>
                        </ol>
                    </div>

                </div>

            </div>

        </div>

        <div class="content-wrapper pt-50 pb-50">

            <div class="container">

                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <div class="section-title">
                            <h2>Lorem Ipsum is simply dumm</h2>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.</p>
                        </div>

                    </div>

                </div>



                <div class="clear mb-40"></div>

                <div class="tab-style-01-wrapper">


                    <div class="clear"></div>

                    <div class="tab-content" >

                        <div class="tab-pane fade in active" id="tab-style-01-03">

                            <div class="tab-inner">

                                <div class="pricing-wrapper clearfix">

                                    <div class="GridLex-gap-30">

                                        <div class="GridLex-grid-noGutter-equalHeight">

                                            <div class="GridLex-col-4_sm-4_xs-12_xss-12">

                                                <div class="pricing-item-01">

                                                    <div class="pricing-item-inner">

                                                        <div class="pricing-item-header">

                                                            <h4 class="mb-0">SD</h4>

                                                        </div>

                                                        <div class="pricing-item-content">

                                                            <ul class="tab-nav">
                                                                <li class="active"><a href="pricing-01.html#tab-3-image-count-01" data-toggle="tab">240p</a></li>
                                                                <li><a href="pricing-01.html#tab-3-image-count-02" data-toggle="tab">480p</a></li>
                                                            </ul>

                                                            <div class="tab-content">

                                                                <div class="tab-pane fade in active" id="tab-3-image-count-01">

                                                                    <div class="tab-inner">

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_13-1" name="pricing_radio_13" type="radio" class="radio" checked />
                                                                            <label for="pricing_radio_13-1">1 photo <span class="radio-price">$8</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_13-2" name="pricing_radio_13" type="radio" class="radio" />
                                                                            <label for="pricing_radio_13-2">11 photo <span class="radio-price">$33</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_13-3" name="pricing_radio_13" type="radio" class="radio" />
                                                                            <label for="pricing_radio_13-3">23 photo <span class="radio-price">$66</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_13-4" name="pricing_radio_13" type="radio" class="radio" />
                                                                            <label for="pricing_radio_13-4">34 photo <span class="radio-price">$99</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_13-5" name="pricing_radio_13" type="radio" class="radio" />
                                                                            <label for="pricing_radio_13-5">60 photo <span class="radio-price">$122</span></label>
                                                                        </div>

                                                                        <div class="clear"></div>

                                                                        <ul class="pricing-item-list mt-30">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> <span class="font700 font18">$1.5</span> per photo <span class="label label-main ml-10">10% save</span></li>

                                                                        </ul>

                                                                        <div class="mt-20 text-center">

                                                                            <a href="pricing-01.html#" class="btn btn-primary">Buy Now</a>

                                                                        </div>

                                                                        <ul class="pricing-item-list mt-20">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> Standart License</li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane fade" id="tab-3-image-count-02">

                                                                    <div class="tab-inner">

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_14-1" name="pricing_radio_14" type="radio" class="radio" checked />
                                                                            <label for="pricing_radio_14-1">1 image <span class="radio-price">$28</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_14-2" name="pricing_radio_14" type="radio" class="radio" />
                                                                            <label for="pricing_radio_14-2">11 image <span class="radio-price">$133</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_14-3" name="pricing_radio_14" type="radio" class="radio" />
                                                                            <label for="pricing_radio_14-3">23 image <span class="radio-price">$266</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_14-4" name="pricing_radio_14" type="radio" class="radio" />
                                                                            <label for="pricing_radio_14-4">34 image <span class="radio-price">$399</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_14-5" name="pricing_radio_14" type="radio" class="radio" />
                                                                            <label for="pricing_radio_14-5">60 image <span class="radio-price">$522</span></label>
                                                                        </div>

                                                                        <div class="clear"></div>

                                                                        <ul class="pricing-item-list mt-30">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> <span class="font700 font18">$10.5</span> per images <span class="label label-main ml-10">12% save</span></li>

                                                                        </ul>

                                                                        <div class="mt-20 text-center">

                                                                            <a href="pricing-01.html#" class="btn btn-primary">Buy Now</a>

                                                                        </div>

                                                                        <ul class="pricing-item-list mt-20">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> Extended License</li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="GridLex-col-4_sm-4_xs-12_xss-12">

                                                <div class="pricing-item-01">

                                                    <div class="pricing-item-inner">

                                                        <div class="pricing-item-header">

                                                            <h4 class="mb-0">HD</h4>

                                                        </div>

                                                        <div class="pricing-item-content">

                                                            <ul class="tab-nav">
                                                                <li class="active"><a href="pricing-01.html#tab-3-daily-01" data-toggle="tab">720p</a></li>
                                                                <li><a href="pricing-01.html#tab-3-daily-02" data-toggle="tab">1080p</a></li>
                                                            </ul>

                                                            <div class="tab-content">

                                                                <div class="tab-pane fade in active" id="tab-3-daily-01">

                                                                    <div class="tab-inner">

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_15-1" name="pricing_radio_15" type="radio" class="radio" checked />
                                                                            <label for="pricing_radio_15-1">10 photos <span class="radio-price">$28</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_15-2" name="pricing_radio_15" type="radio" class="radio" />
                                                                            <label for="pricing_radio_15-2">25 photos <span class="radio-price">$133</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_15-3" name="pricing_radio_15" type="radio" class="radio" />
                                                                            <label for="pricing_radio_15-3">40 photos <span class="radio-price">$266</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_15-4" name="pricing_radio_15" type="radio" class="radio" />
                                                                            <label for="pricing_radio_15-4">100 photos <span class="radio-price">$399</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_15-5" name="pricing_radio_15" type="radio" class="radio" />
                                                                            <label for="pricing_radio_15-5">150 photos <span class="radio-price">$522</span></label>
                                                                        </div>

                                                                        <div class="clear"></div>

                                                                        <ul class="pricing-item-list mt-30">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> <span class="font700 font18">$0.99</span> per photo <span class="label label-main ml-10">12% save</span></li>

                                                                        </ul>

                                                                        <div class="mt-20 text-center">

                                                                            <a href="pricing-01.html#" class="btn btn-primary">Buy Now</a>

                                                                        </div>

                                                                        <ul class="pricing-item-list mt-20">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> Standart License</li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                                <div class="tab-pane fade" id="tab-3-daily-02">

                                                                    <div class="tab-inner">

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_16-1" name="pricing_radio_16" type="radio" class="radio" checked />
                                                                            <label for="pricing_radio_16-1">10 photos <span class="radio-price">$23</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_16-2" name="pricing_radio_16" type="radio" class="radio" />
                                                                            <label for="pricing_radio_16-2">25 photos <span class="radio-price">$125</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_16-3" name="pricing_radio_16" type="radio" class="radio" />
                                                                            <label for="pricing_radio_16-3">40 photos <span class="radio-price">$250</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_16-4" name="pricing_radio_16" type="radio" class="radio" />
                                                                            <label for="pricing_radio_16-4">100 photos <span class="radio-price">$370</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_16-5" name="pricing_radio_16" type="radio" class="radio" />
                                                                            <label for="pricing_radio_16-5">150 photos <span class="radio-price">$500</span></label>
                                                                        </div>

                                                                        <div class="clear"></div>

                                                                        <ul class="pricing-item-list mt-30">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> <span class="font700 font18">$0.88</span> per photo <span class="label label-main ml-10">12% save</span></li>

                                                                        </ul>

                                                                        <div class="mt-20 text-center">

                                                                            <a href="pricing-01.html#" class="btn btn-primary">Buy Now</a>

                                                                        </div>

                                                                        <ul class="pricing-item-list mt-20">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> Standart License</li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>

                                            <div class="GridLex-col-4_sm-4_xs-12_xss-12">

                                                <div class="pricing-item-01 best">

                                                    <div class="pricing-item-best-label"><span>Best Value</span></div>

                                                    <div class="pricing-item-inner">

                                                        <div class="pricing-item-header">

                                                            <h4 class="mb-0">Ultra HD</h4>

                                                        </div>

                                                        <div class="pricing-item-content">

                                                            <ul class="tab-nav single-nav">
                                                                <li class="active"><a href="pricing-01.html#tab-3-monthly-01" data-toggle="tab">4k</a></li>
                                                            </ul>

                                                            <div class="tab-content">

                                                                <div class="tab-pane fade in active" id="tab-3-monthly-01">

                                                                    <div class="tab-inner">

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_17-1" name="pricing_radio_17" type="radio" class="radio" checked />
                                                                            <label for="pricing_radio_17-1">10 photos <span class="radio-price">$28</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_17-2" name="pricing_radio_17" type="radio" class="radio" />
                                                                            <label for="pricing_radio_17-2">25 photos <span class="radio-price">$133</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_17-3" name="pricing_radio_17" type="radio" class="radio" />
                                                                            <label for="pricing_radio_17-3">40 photos <span class="radio-price">$266</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_17-4" name="pricing_radio_17" type="radio" class="radio" />
                                                                            <label for="pricing_radio_17-4">100 photos <span class="radio-price">$399</span></label>
                                                                        </div>

                                                                        <div class="radio-block">
                                                                            <input id="pricing_radio_17-5" name="pricing_radio_17" type="radio" class="radio" />
                                                                            <label for="pricing_radio_17-5">150 photos <span class="radio-price">$522</span></label>
                                                                        </div>

                                                                        <div class="clear"></div>

                                                                        <ul class="pricing-item-list mt-30">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> <span class="font700 font18">$0.99</span> per photos <span class="label label-main ml-10">12% save</span></li>

                                                                        </ul>

                                                                        <div class="mt-20 text-center">

                                                                            <a href="pricing-01.html#" class="btn btn-primary">Buy Now</a>

                                                                        </div>

                                                                        <ul class="pricing-item-list mt-20">

                                                                            <li><i class="fa fa-check-square-o text-main"></i> Standart License</li>

                                                                        </ul>

                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="clear mb-10"></div>

            </div>

        </div>



    </div>
    <!-- end Main Wrapper -->
@endsection