@foreach($photos as $photo)
    @php

       $sizeImage = \App\Models\ImageUpload::getImageSize($photo->id);

       $dataH = $sizeImage[1];
       $dataW = $sizeImage[0];
    @endphp
    <div class="item" data-w="{{ $dataW }}"
         data-h="{{ $dataH }}"
         style="display: block;">
        <a href="{{ route('site.photo.detail', ['id' => $photo->id]) }}">
            <img src="{{ $photo->getUrlThumbImage() }}" alt="image">
        </a>
        <div class="category-item-caption">
            <div class="row gap-0">
                <div class="col-xs-12 col-sm-12">
                    <a href="{{ route('site.photo.detail', ['id' => $photo->id]) }}" data-toggle="tooltip"  data-placement="top" title="{!! trans('detail.purchase') !!}"><i class="fa fa-shopping-cart"></i></a>
                </div>
                {{--<div class="col-xs-4 col-sm-4">--}}
                    {{--<a href="#myModal" data-target="#myModal" class="modalPhotoPreview" data-id="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="{!! trans('detail.preview') !!}"><i class="fa fa-download"></i></a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-6 col-sm-6">--}}
                 {{--   <a href="{{ route('site.product.similar', ['id' => $product->id]) }}" data-toggle="tooltip" data-placement="top" title="{!! trans('detail.find_similar') !!}"><i class="fa fa-search"></i></a>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>

@endforeach
@php($l = $photos->currentPage())

