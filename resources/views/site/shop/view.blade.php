@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        <!-- start hero-header -->
        <div id="gallery" class="sliderBlock">
            <ul class="shopSlider slides" data-slide="0">
                @if(!empty($shopImages))
                    @foreach($shopImages as $slideKey => $image)
                        <li class="hero slide-{{ $slideKey }}">
                            <img src="{{ \App\Models\ImageUpload::getUrl($image) }}" alt="">

                            <div class="container sliderText">
                                <div class="row gap-0">

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="section-title-special">
                                            <h1>
                                                {{ $shop->translationName(\App\Models\Translation::currentLang()) == null ?
                                                    $shop->name : $shop->translationName(\App\Models\Translation::currentLang())->value }}
                                            </h1>
                                            <div class="p-title" style="background: rgba(0, 0, 0, .4);">
                                                {!! $shop->translationDescription(\App\Models\Translation::currentLang()) == null ?
                                                    $shop->name : $shop->translationDescription(\App\Models\Translation::currentLang())->value !!}
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </li>
                    @endforeach
                @else
                    <div class="hero limogesIncludesList" style="background-image:url('../images/limoges_photo.jpg');">
                        <div class="container">
                            <div class="row gap-0">

                                <div class="col-md-10 col-md-offset-1">
                                    <div class="section-title-special">
                                        <h1 style="background: rgba(0, 0, 0, .4);">
                                            {{ $shop->translationName(\App\Models\Translation::currentLang()) == null ?
                                                $shop->name : $shop->translationName(\App\Models\Translation::currentLang())->value }}
                                        </h1>
                                        <div class="p-title" style="background: rgba(0, 0, 0, .4);">
                                            {!! $shop->translationDescription(\App\Models\Translation::currentLang()) == null ?
                                                $shop->name : $shop->translationDescription(\App\Models\Translation::currentLang())->value !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </ul>
        </div>
    </div>


    <div class="main-wrapper scrollspy-container">
        <div class="breadcrumb-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">{!! trans('index.license_agreement') !!}</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a>
                            </li>
                            <li class="">
                                <a href="{{ route('site.shops.index') }}">{!! trans('license.title') !!}</a>
                            </li>
                            <li class="active">
                                {{ $shop->translationName(\App\Models\Translation::currentLang()) == null ?
                                                    $shop->name : $shop->translationName(\App\Models\Translation::currentLang())->value }}
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
