@extends('layouts.app')

@section('content')

    <div class="main-wrapper">
        <div class="hero limogesIncludesList" style="background-image:url('images/limoges_photo.jpg');">
            <div class="container">
                <div class="row gap-0">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="section-title-special">
                            <h1>{!! trans('license.title') !!}</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-wrapper scrollspy-container">

        <div class="breadcrumb-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">{!! trans('index.license_agreement') !!}</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a></li>
                            <li class="active">{!! trans('index.license_agreement') !!}</li>
                        </ol>
                    </div>

                </div>

            </div>

        </div>

        <div class="content-wrapper pt-50 pb-50">

            <div class="container">

                <div class="row">



                    <div class="col-xs-12 col-md-12">

                        <div class="static-wrapper">

                            @if(!empty($license))
                                {!! $license->value !!}
                            @endif
                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection