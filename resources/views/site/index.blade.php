@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

        <!-- start hero-header -->
        <div id="gallery" class="sliderBlock">
            <ul class="shopSlider slides" data-slide="0">
                @if(!empty($shopImages))
                    @foreach($shopImages as $slideKey => $image)
                        <li class="hero slide-{{ $slideKey }}">
                            <img src="{{ \App\Models\ImageUpload::getUrl($image) }}" alt="">

                            <div class="container sliderText">
                                <div class="row gap-0">

                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="section-title-special">
                                            <h1>{!! trans('index.text1') !!}</h1>
                                            <p class="p-title">{!! trans('index.text2') !!}</p>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </li>
                    @endforeach
                @else
                    <div class="hero" style="background-image:url('images/hero-header/New schapka.jpg');">
                        <div class="container">
                            <div class="row gap-0">

                                <div class="col-md-10 col-md-offset-1">
                                    <div class="section-title-special">
                                        <h1>{!! trans('index.text1') !!}</h1>
                                        <p class="p-title">{!! trans('index.text2') !!}</p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                @endif
            </ul>
        </div>
        <!-- end hero-header -->

        <div class="content-wrapper">

            <div class="section pb-50">

                <div class="container">

                    @if($places)
                        <div class="coffeeTables">

                            <div class="col-md-10 col-md-offset-1">
                                <div class="section-title-special">
                                    <h2>{!! trans('index.text3') !!}</h2>
                                </div>
                            </div>

                            @foreach($places as $place)
                                <div class="coffeeTable-overlay">
                                    <div class="coffeeTable places-{{ $place->count }}">
                                        @for($placeNumber = 1; $placeNumber <= $place->count; $placeNumber++)
                                            <span class="place place-{{ $placeNumber }}"></span>
                                        @endfor
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>

                <div class="container createOrder">
                    <a href="#createOrder" class="btn btn-primary orange-flat-button createOrder" data-toggle="modal">{!! trans('index.text5') !!}</a>
                </div>

                <div class="container">

                    <ul class="home-products-list clearfix mt-10 row menuBlock">
                        @if($categories)
                            <div class="col-md-10 col-md-offset-1">
                                <div class="section-title-special">
                                    <h2>{!! trans('index.text4') !!}</h2>
                                </div>
                            </div>

                            @foreach($categories as $category)
                                <li class="col-md-12 categories">
                                    <h3>{{ $category->translationName(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationName(\App\Models\Translation::currentLang())->value }}</h3>
                                    <ul class="categoryProducts">
                                        @foreach($category->products as $product)
                                            <li class="col-md-12">
                                                <span class="productName">{{ $product->title }}</span>
                                                <span class="productDescription">{{ $product->description }}</span>
                                                <span class="productPrice">{{ $product->price }}</span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection

@includeif('site.modal.modal-order')