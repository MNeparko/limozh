@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        <!-- start hero-header -->
        <div class="hero" style="background-image:url('images/hero-header/New schapka.jpg');">
            <div class="container">
                <div class="row gap-0">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="section-title-special">
                            <a href="{{ URL::to('/') }}">
                                <h2 style="color:#fff;">{!! trans('index.text9') !!}</h2>
                            </a>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- end hero-header -->

        <div class="content-wrapper">

            <div class="section pb-50">

                <div class="container">

                    <ul class="home-products-list clearfix mt-10 row">
                        @if($products)
                            @foreach($products as $product)
                                <li class="col-md-12">
                                    <a href="#{{--{{ URL::to('products/edit/' . $product->id) }}--}}">
                                        <h2>{{ $product->title }}</h2>
                                        <span class="dates">Акция с <strong>{{ $product->stock_start }}</strong> по <strong>{{ $product->stock_end }}</strong></span>
                                        <div class="price-block">
                                            <span class="price">{{ $product->price }} / {{ $product->units }}</span>
                                            <span class="old_price">{{ $product->old_price }} / {{ $product->units }}</span>
                                        </div>
                                        <img src="images/product.png">
                                    </a>
                                </li>
                            @endforeach
                        @endif
                        {{ $products->links() }}

                    </ul>
                </div>
            </div>

        </div>
    </div>
    <style>
        .pagination {
            display: block !important;
        }
    </style>

@endsection