@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        <div class="breadcrumb-wrapper">

            <div class="container">

                <div class="row">

                    <div class="col-xs-12 col-sm-6">
                        <h2 class="page-title">{!! trans('detail.photo_detail') !!}</h2>
                    </div>

                    <div class="col-xs-12 col-sm-6">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('site.index') }}">{!! trans('index.home') !!}</a></li>
                            <li><a href="{{ route('site.category.view', ['id' => $photo->categories()->first()->id]) }}">
                                    {{ $photo->categories()->first()->translationName(\App\Models\Translation::currentLang()) == null ? $photo->categories()->first()->name : $photo->categories()->first()->translationName(\App\Models\Translation::currentLang())->value }}</a></li>
                            <li class="active">{{ $photo->title }}</li>
                        </ol>
                    </div>

                </div>

            </div>

        </div>


        {{ Form::open(['method' => 'get', 'route' => 'site.search']) }}

            <div class="breadcrumb-wrapper breadcrumb-form">

                <div class="container">

                    <div class="row">

                        <div class="col-xs-12 col-sm-6 col-md-6 mb-20-sm">

                            <div class="input-group-search-form-wrapper">

                                <div class="input-group bg-change-focus-addclass">

                                    <input name="search" type="text" class="form-control" placeholder="{!! trans('view.keyword') !!}" >
                                    {{ Form::hidden('dropdownValue',null) }}
                                    {{ csrf_field() }}

                                    <div class="input-group-btn dropdown-select">
                                        <div class="dropdown dropdown-select">
                                            <button class="btn dropdown-toggle" type="button" id="mainSearchDropdown" data-category-id="0" data-toggle="dropdown" aria-expanded="true">
                                                {!! trans('index.all_stock') !!}
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu" aria-labelledby="mainSearchDropdown">
                                                <li class="category-search" data-category-id="0" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">{!! trans('index.all_stock') !!}</a></li>
                                                @foreach($allCategories as $category)
                                                    <li class="category-search" data-category-id="{{ $category->id }}" role="presentation"><a role="menuitem" tabindex="-1" href="javascript:void(0)">{{ $category->translationName(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationName(\App\Models\Translation::currentLang())->value }}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div><!-- /btn-group -->

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                    </div>

                                </div><!-- /input-group -->

                            </div>

                        </div>


                    </div>

                </div>

            </div>


        {{ Form::close() }}

        <div class="content-wrapper">

            <div class="detail-wrapper">

                <div class="container">

                    <div class="section-sm">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success">
                                <p>{{ $message }}</p>
                            </div>
                        @endif
                        <div class="row">

                            <div class="col-xs-12 col-sm-12 col-md-3">

                                <div class="row gap-20">

                                    <div class="col-xs-12 col-sm-6 col-md-12 mb-30-sm">

                                        <h3 class="detail-title">{!! trans('detail.stock') !!}: {{ $photo->translationTitle(\App\Models\Translation::currentLang()) == null ? $photo->title : $photo->translationTitle(\App\Models\Translation::currentLang())->value }}</h3>
                                        <p class="detail-meta">/ {!! trans('detail.stock') !!} ID: #{{ $photo->id }} </p>
                                        <p class="detail-meta">/ {!! trans('detail.uploaded_date') !!}: {{ $photo->uploaded_at->format('Y-m-d') }} </p>


                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-12 mb-30-sm detail-person ">

                                        <div class="detail-sm-section">
                                            <h4>{!! trans('detail.collection') !!}:</h4>
                                            @foreach($photo->categories as $category)
                                            <a href="{{ route('site.category.view', ['id'=> $category->id]) }}"
                                               class="detail-collection">{{ $category->translationName(\App\Models\Translation::currentLang()) == null ? $category->name : $category->translationName(\App\Models\Translation::currentLang())->value }}</a>
                                            @endforeach
                                        </div>
                                        @if($photo->keywords->count())
                                        <div class="detail-sm-section">
                                            <h4>{!! trans('detail.keywords') !!}:</h4>
                                            @php
                                            $keywords = $photo->keywords;
                                            $total = $keywords->count();
                                            $counter = 0;
                                            @endphp
                                            @foreach($keywords as $key => $keyword)
                                                @php($counter++)
                                                @if($counter == $total)
                                                    <a href="{{ route('site.photo.keyword', ['id'=>$keyword->id]) }}"
                                                       class="detail-keyword">{{ $keyword->translationValue(\App\Models\Translation::currentLang()) == null ? $keyword->value : $keyword->translationValue(\App\Models\Translation::currentLang())->value }}</a>
                                                @else
                                                    <a href="{{ route('site.photo.keyword', ['id'=>$keyword->id]) }}"
                                                       class="detail-keyword">{{ $keyword->translationValue(\App\Models\Translation::currentLang()) == null ? $keyword->value : $keyword->translationValue(\App\Models\Translation::currentLang())->value }}</a>,
                                                @endif
                                            @endforeach
                                        </div>
                                        @endif
                                    </div>

                                </div>

                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-9">

                                <div class="row">

                                    <div class="col-xs-12 col-sm-6 col-md-7 mb-30-sm">
                                        <div class="detail-image">
                                            <img src="{{ $photo->getUrlDetailImage() }}" alt="Images" />

                                            <div class="mt-15">

                                                <!-- Button trigger modal -->

                                                {{--<a href="#myModal" data-target="#myModal" class="mr-15 modalPhotoPreview" data-id="{{ $product->id }}" ><i class="fa fa-download"></i> {!! trans('detail.download_preview') !!}</a>--}}

                                            </div>

                                            <div class="social-share clearfix">
                                                <a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}" target="_blank" class="social-facebook">
														<span class="block">
															<i class="fa fa-facebook-official"></i>
														</span>
                                                    {!! trans('detail.share_facebook') !!}
                                                </a>
                                                <a href="https://twitter.com/home?status={{url()->current()}}" class="social-twitter">
														<span class="block">
															<i class="fa fa-twitter"></i>
														</span>
                                                    {!! trans('detail.share_twitter') !!}
                                                </a>
                                                <a href="https://pinterest.com/pin/create/button/?url={{ $photo->getUrlImage() }}&media={{ url()->current() }}&description=" class="social-pinterest">
														<span class="block">
															<i class="fa fa-pinterest-p"></i>
														</span>
                                                    {!! trans('detail.share_pinterest') !!}
                                                </a>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-6 col-md-5">

                                        <div class="detail-tab">

                                            <!-- Nav tabs -->
                                            <ul class="nav nav-tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="stockphoto-detail.html#detail-tab-3" data-toggle="tab">{!! trans('detail.sizes') !!}</a></li>
                                                {{--<li role="presentation"><a href="stockphoto-detail.html#detail-tab-2" data-toggle="tab">Credits</a></li>--}}
                                                {{--<li role="presentation"><a href="stockphoto-detail.html#detail-tab-1" data-toggle="tab">Subscription</a></li>--}}
                                            </ul>

                                            <!-- Tab panes -->
                                            <div class="tab-content">

                                                <div role="tabpanel" class="tab-pane fade in active" id="detail-tab-1">

                                                    <p class="mb-10">{!! trans('detail.download_image_string') !!}</p>
                                                    @php
                                                        $propertyCount = count($photo->properties);
                                                        for ($i = 1; $i < $propertyCount+1; $i++) {
                                                            if ($i == 1)
                                                                $arr[$i]['dpi'] = '72 DPI';
                                                            else
                                                                $arr[$i]['dpi'] = '300 DPI';
                                                        }

                                                    @endphp
                                                    {{ Form::open(['method' => 'get', 'route' => ['site.photo.addToCart', $photo->id]]) }}
                                                    <div class="detail-radio-wrapper mb-30">
                                                        @php($count = 0)

                                                        @foreach($photo->properties as $property)
                                                            @php($count++)

                                                        <div class="radio-block">
                                                            <input  id="radio_size-{{ $count }}" name="radio_size" type="radio" class="radio" value="{{ $property->size_id }}"
                                                                   {{ $count == 1 ? 'checked' : '' }} />
                                                            <label class="" for="radio_size-{{ $count }}">
																	<span class="detail-price-sm">
																		<span class="block uppercase">{{ $property->size->title }}</span>
                                                                        {{--{!! trans('detail.image') !!}--}}
																		{{ $property->actual_resolution }} px | <b style="text-transform: uppercase">{!! $photo->extension !!}</b> | @if($count==1) <b style="color: #1b16e0;">{{ $arr[$count]['dpi'] }}</b> @else <b style="color: #c72626;">{{ $arr[$count]['dpi'] }}</b> @endif
                                                                        @if(!$property->size->original)
                                                                            <b style="color: green;"> {!! trans('detail.standard_license') !!}</b>
                                                                        @else
                                                                            <b style="color: red;"> {!! trans('detail.full_license') !!}</b>
                                                                        @endif
																		<span class="price">{{ $property->size->cost }} €</span>
																	</span>
                                                            </label>
                                                        </div>
                                                        @endforeach

                                                    </div>
                                                    {{--<input type="submit" class="btn btn-primary btn-lg btn-block" >--}}
                                                    {{--<a href="stockphoto-detail.html#" class="btn btn-primary btn-lg btn-block"><i class="fa fa-download"></i> Download this product</a>--}}
                                                    <button type="submit" class="btn btn-primary btn-lg btn-block"><i class="fa fa-download"></i> {!! trans('detail.download') !!}</button>
                                                    <p class="mb-0 mt-15">{!! trans('detail.read_license') !!}(<a href="{{ route('site.license') }}">license</a>)</p>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

            <div class="container clearfix">

                <div class="section-sm">
                    @if($similarPhotos->count())
                    <div class="section-title mb-10">
                        <h4 class="text-left">{!! trans('detail.same_photos') !!}</h4>
                    </div>

                          <div class="flex-images flex-image-detail category-item-wrapper">
                             @foreach($similarPhotos as $photo)
                                 @php

                                     $sizeImage = \App\Models\ImageUpload::getImageSize($photo->id);

                                     $dataH = $sizeImage[1];
                                     $dataW = $sizeImage[0];
                                @endphp
                                <div class="item"  data-h="{{ $dataH }}" data-w="{{ $dataW }}">
                                <a href="{{ route('site.photo.detail', ['id' => $photo->id]) }}">
                                    <img src="{{ $photo->getUrlThumbImage() }}" alt="image">
                                </a>
                                <div class="category-item-caption">
                                    <div class="row gap-0">
                                        <div class="col-xs-12 col-sm-12">
                                            <a href="{{ route('site.photo.detail', ['id' => $photo->id]) }}" data-toggle="tooltip"  data-placement="top" title="Purchase"><i class="fa fa-shopping-cart"></i></a>
                                        </div>
                                    <div class="col-xs-4 col-sm-4">
                                        <a href="#myModal" data-target="#myModal" class="modalPhotoPreview" data-id="{{ $photo->id }}" data-toggle="tooltip" data-placement="top" title="Preview"><i class="fa fa-download"></i></a>
                             </div>
                                  <div class="col-xs-6 col-sm-6">
                                    <a href="{{ route('site.photo.similar', ['id' => $photo->id]) }}" data-toggle="tooltip" data-placement="top" title="Find similar"><i class="fa fa-search"></i></a>
                             </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                         </div>
                    @endif
                    <div class="clear mb-50"></div>
                    @if($samePhotos->count())
                    {{--<div class="section-title mb-10">--}}
                        {{--<h4 class="text-left">{!! trans('detail.same_photos') !!}</h4>--}}
                    {{--</div>--}}

                    {{--<div class="flex-images flex-image-detail category-item-wrapper">--}}
                        {{--@foreach($samePhotos as $product)--}}
                            {{--@php--}}

                                {{--$sizeImage = \App\Models\ImageUpload::getImageSize($product->id);--}}

                                {{--$dataH = $sizeImage[1];--}}
                                {{--$dataW = $sizeImage[0];--}}
                            {{--@endphp--}}
                            {{--<div class="item"  data-h="{{ $dataH }}" data-w="{{ $dataW }}">--}}
                            {{--<a href="{{ route('site.product.detail', ['id' => $product->id]) }}">--}}
                                {{--<img src="{{ $product->getUrlThumbImage() }}" alt="image">--}}
                            {{--</a>--}}
                            {{--<div class="category-item-caption">--}}
                                {{--<div class="row gap-0">--}}
                                    {{--<div class="col-xs-12 col-sm-12">--}}
                                        {{--<a href="{{ route('site.product.detail', ['id' => $product->id]) }}" data-toggle="tooltip"  data-placement="top" title="{!! trans('detail.purchase') !!}"><i class="fa fa-shopping-cart"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-4 col-sm-4">--}}

                                        {{--<a href="#myModal" data-target="#myModal" class="modalPhotoPreview" data-id="{{ $product->id }}" data-toggle="tooltip" data-placement="top" title="{!! trans('detail.preview') !!}"><i class="fa fa-download"></i></a>--}}
                                    {{--</div>--}}
                                    {{--<div class="col-xs-6 col-sm-6">--}}
                                        {{--<a href="{{ route('site.product.similar', ['id' => $product->id]) }}" data-toggle="tooltip" data-placement="top" title="{!! trans('detail.find_similar') !!}"><i class="fa fa-search"></i></a>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--@endforeach--}}

                    {{--</div>--}}
                        @endif
                    <div class="clear mb-10"></div>

                </div>

            </div>

        </div>


    </div>

@endsection