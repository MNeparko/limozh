@extends('layouts.app')

@section('content')
    <div class="main-wrapper">

        <!-- start hero-header -->
        <div class="hero" style="background-image:url('../images/hero-header/New schapka.jpg');">
            <div class="container">
                <div class="row gap-0">

                    <div class="col-md-10 col-md-offset-1">
                        <div class="section-title-special">
                            <a href="{{ URL::to('/') }}">
                                <h2 style="color:#fff;">{!! trans('index.text9') !!}</h2>
                            </a>
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <!-- end hero-header -->

        <div class="content-wrapper">

            <div class="section pb-50 row">

                <div class="container row" style="margin: 0 auto !important;">
                    <div class="col-md-12 product-view">

                        <img src="../images/product.png">

                        <h2>{{ $product->title }}</h2>
                        <div class="details">
                            <span class="dates">Акция проходит с <strong>{{ $product->stock_start }}</strong> по <strong>{{ $product->stock_end }}</strong></span>
                            <span class="price">Актуальная цена: {{ $product->price }} / {{ $product->units }}</span>
                            <span class="old_price">Старая цена: {{ $product->old_price }} / {{ $product->units }}</span>
                            <a class="buy" href="{{ URL::to('add-to-cart/' . $product->id) }}">Купить</a>
                        </div>
                    </div>
                    <br/><br/>
                    <span class="description">Описание: {{ $product->description }}</span>
                </div>
            </div>

        </div>
    </div>

@endsection