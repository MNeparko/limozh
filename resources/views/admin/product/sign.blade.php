<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.product.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['method' => 'PATCH','route' => ['admin.product.sign.update', $photo->id]]) }}

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>Ключевое слово</th>
    <th>Значимость</th>
    </thead>
    @foreach($photo->keywords as $keyword)

        <tr>
            <td>{{ 'En: '.$keyword->value  }}
                {{ $keyword->translationValue(\App\Models\Translation::LANG_RU) == null ? '' : ', Ru: '.$keyword->translationValue(\App\Models\Translation::LANG_RU)->value }}
                {{ $keyword->translationValue(\App\Models\Translation::LANG_DE) == null ? '' : ', De: '.$keyword->translationValue(\App\Models\Translation::LANG_DE)->value }}</td>
            <td>
                {{ Form::text($keyword->id, $keyword->pivot->sign, ['class' => 'form-control',
                'placeholder' => 'Введите число']) }}
            </td>

        </tr>
    @endforeach
</table>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}