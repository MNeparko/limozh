<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.product.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{!! Form::open(['url' => 'admin/products/update/' . @$product->id, 'files' => true]) !!}

<div class="form-group">
    {{ Form::label('titleEn', 'Заголовок фото (English):') }}
    {{ Form::text('titleEn', $product->translationTitle(\App\Models\Translation::LANG_EN) == null ? $product->title : $product->translationTitle(\App\Models\Translation::LANG_EN)->value, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
    {{ Form::label('titleRu', 'Заголовок фото (Russian):') }}
    {{ Form::text('titleRu', $product->translationTitle(\App\Models\Translation::LANG_RU) == null ? $product->title : $product->translationTitle(\App\Models\Translation::LANG_RU)->value, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
    {{ Form::label('titleDe', 'Заголовок фото (Deutschland):') }}
    {{ Form::text('titleDe', $product->translationTitle(\App\Models\Translation::LANG_DE) == null ? $product->title : $product->translationTitle(\App\Models\Translation::LANG_DE)->value, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
</div>
<div class="form-group">
    {{ Form::label('keywords[]', 'Ключевые слова:') }}
    {{ Form::select('keywords[]', $keywords, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<div class="form-group">
    {{ Form::label('shop_id[]', 'Заведения:') }}
    {{ Form::select('shop_id[]', $shops, null, [
        'title' => 'Нажмите для выбора',
        'data-width' => 'fit'
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('categories[]', 'Категории:') }}
    {{ Form::select('categories[]', $categories, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>
<div class="form-group">
    {{ Form::label('title', 'Наименование товара') }}
    {{ Form::text('title', @$product->title) }}
</div>

<div class="form-group">
    {{ Form::label('description', 'Описание') }}
    {{ Form::textarea('description', @$product->description) }}
</div>

<div class="form-group">
    {{ Form::label('link', 'Линк товара') }}
    {{ Form::text('link', @$product->link) }}
</div>

<div class="form-group">
    {{ Form::label('units', 'Единиццы измерения товара') }}
    {{ Form::text('units', @$product->units) }}
</div>

<div class="form-group">
    {{ Form::label('price', 'Цена') }}
    {{ Form::text('price', @$product->price) }}
</div>

<div class="form-group">
    {{ Form::label('old_price','Старая цена') }}
    {{ Form::text('old_price', @$product->old_price) }}
</div>

<div class="form-group">
    {{ Form::label('stock_start','Дата начала акции') }}
    {{ Form::text('stock_start', @$product->stock_start) }}
</div>

<div class="form-group">
    {{ Form::label('stock_end','Дата завершения акции') }}
    {{ Form::text('stock_end', @$product->stock_end) }}
</div>

<div class="form-group">
    {{ Form::label('priority','Приоритет вывода товара') }}
    {{ Form::text('priority', @$product->priority) }}
</div>

<div class="form-group">
    {{ Form::label('is_new','Новый товар') }}
    {{ Form::checkbox('is_new', @$product->is_new) }}
</div>

<div class="form-group">
    {{ Form::label('is_stock','Акционный товар') }}
    {{ Form::checkbox('is_stock', @$product->is_stock) }}
</div>

<div class="form-group">
    {{ Form::label('in_top','Товар в топе') }}
    {{ Form::checkbox('in_top', @$product->in_top) }}
</div>

<div class="form-group">
    {{ Form::label('keywords[]', 'Ключевые слова:') }}
    {{ Form::select('keywords[]', $keywords, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<div class="form-group">
    {{ Form::label('categories[]', 'Категории:') }}
    {{ Form::select('categories[]', $categories, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

@if(!empty($images))
    <h4>Загруженные:</h4>
    <div class="admin-images">
        @foreach($images as $image)
            <a target="_blank" href="{{ \App\Models\ImageUpload::getUrl($image) }}">
                {{ basename(\App\Models\ImageUpload::getUrl($image)) }}
            </a><br>
        @endforeach
    </div>
@endif

<div class="form-group">
    {{ Form::label('pictures[]', 'Изображения:') }}
    {{ Form::file('pictures[]', array('multiple'=>true)) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>

{{ Form::close() }}
@includeIf('admin._forms.multiselect')