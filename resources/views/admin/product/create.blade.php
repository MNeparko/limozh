<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.product.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{!! Form::open(['url' => 'admin/products/store', 'files' => true]) !!}

<div class="form-group">
    {{ Form::label('titleEn', 'Заголовок (English):') }}
    {{ Form::text('titleEn', null, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
    {{ Form::label('titleRu', 'Заголовок (Russian):') }}
    {{ Form::text('titleRu', null, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
    {{ Form::label('titleDe', 'Заголовок (Deutschland):') }}
    {{ Form::text('titleDe', null, ['class' => 'form-control',
    'placeholder' => 'Введите заголовок',
    ]) }}
</div>
<div class="form-group">
    {{ Form::label('title', 'Наименование товара') }}
    {{ Form::text('title', '') }}
</div>

<div class="form-group">
    {{ Form::label('shop_id[]', 'Заведения:') }}
    {{ Form::select('shop_id[]', $shops, null, [
        'title' => 'Нажмите для выбора',
        'data-width' => 'fit'
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('description', 'Описание') }}
    {{ Form::textarea('description', '') }}
</div>

<div class="form-group">
    {{ Form::label('link', 'Линк товара') }}
    {{ Form::text('link', '') }}
</div>

<div class="form-group">
    {{ Form::label('units', 'Единиццы измерения товара') }}
    {{ Form::text('units', '') }}
</div>

<div class="form-group">
    {{ Form::label('price', 'Цена') }}
    {{ Form::text('price', '') }}
</div>

<div class="form-group">
    {{ Form::label('old_price','Старая цена') }}
    {{ Form::text('old_price', '') }}
</div>

<div class="form-group">
    {{ Form::label('stock_start','Дата начала акции') }}
    {{ Form::text('stock_start', '') }}
</div>

<div class="form-group">
    {{ Form::label('stock_end','Дата завершения акции') }}
    {{ Form::text('stock_end', '') }}
</div>

<div class="form-group">
    {{ Form::label('priority','Приоритет вывода товара') }}
    {{ Form::text('priority', '') }}
</div>

<div class="form-group">
    {{ Form::label('is_new','Новый товар') }}
    {{ Form::checkbox('is_new') }}
</div>

<div class="form-group">
    {{ Form::label('is_stock','Акционный товар') }}
    {{ Form::checkbox('is_stock') }}
</div>

<div class="form-group">
    {{ Form::label('in_top','Товар в топе') }}
    {{ Form::checkbox('in_top') }}
</div>

<div class="form-group">
    {{ Form::label('keywords[]', 'Ключевые слова:') }}
    {{ Form::select('keywords[]', $keywords, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<div class="form-group">
    {{ Form::label('categories[]', 'Категории:') }}
    {{ Form::select('categories[]', $categories, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<div class="form-group">
    {{ Form::label('pictures[]', 'Изображения:') }}
    {{ Form::file('pictures[]', array('multiple'=>true)) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}

@includeIf('admin._forms.multiselect')