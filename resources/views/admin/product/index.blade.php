@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<a href="{{ route('admin.product.create') }}" class="btn btn-primary">Добавить</a>

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>ID</th>
    <th>Название</th>
    <th>Цена</th>
    <th>Дата создания</th>
    <th>Дата обновления</th>
    <th>Действия</th>
    </thead>

    @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->title }}</td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->created_at->format('d-m-Y') }}</td>
            <td>{{ $product->updated_at->format('d-m-Y') }}</td>
            <td>
                <a href="{{ URL::to('admin/products/edit/' . $product->id) }}" class="btn btn-primary" title="Редактировать">
                    <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>
                    &nbsp;
                </a>
            @if($product->keywords && $product->keywords->count())
                <a href="{{ URL::to('admin/products/edit/' . $product->id) }}" class="btn btn-primary" title="Настройка поиска">
                    <i class="fa fa-cog" aria-hidden="true"></i>
                    &nbsp;
                </a>
            @endif
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ URL::to('admin/products/delete/' . $product->id) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
    
</table>
{{ $products->links() }}

<?php $modalHeader = 'Удаление'; $modalBody = 'Вы уверены что хотите удалить товар?';?>
@includeif('admin.modal.modal-delete')