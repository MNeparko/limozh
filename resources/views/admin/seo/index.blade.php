
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
{{--@permission('section-create')--}}
<a href="{{ route('admin.seo.create') }}" class="btn btn-primary">Добавить seo страницы</a>
{{--@endpermission--}}
<table class="table  table-striped table-bordered table-responsive">
    <thead>
    <th>ID</th>
    <th>Url путь</th>
    <th>Дата изменения</th>
    <th>Действия</th>
    </thead>
    @foreach($seo_items as $seo_item)
        <tr>
            <td>{{ $seo_item->id }}</td>
            <td>{{ $seo_item->url_path }}</td>
            <td>{{ $seo_item->updated_at }}</td>
            <td>
                {{--@permission('section-edit')--}}
                <a href="{{ route('admin.seo.edit',['id' => $seo_item->id]) }}"
                   class="btn btn-primary" title="Редактировать"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>&nbsp;</a>
                {{--@endpermission--}}
                {{--@permission('service-delete')--}}
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ route('admin.seo.delete', ['id' => $seo_item->id]) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
                {{--@endpermission--}}
            </td>
        </tr>
    @endforeach
</table>
{{ $seo_items->links() }}

<?php $modalHeader = 'Удаление seo страницы'; $modalBody = 'Вы уверены что хотите удалить seo страницы?';?>
@include('admin.modal.modal-delete')