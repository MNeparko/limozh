<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.seo.index') }}"> Назад</a>
        </div>
    </div>
</div>
@foreach($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::model($seo_item, ['method' => 'PATCH','route' => ['admin.seo.update', $seo_item->id]]) }}

<div class="form-group">
    {{ Form::label('url_path', 'Url путь от домена:') }}
    {{ Form::text('url_path', null, ['class' => 'form-control',
    'placeholder' => 'Введите url',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('meta_description', 'meta description:') }}
    {{ Form::text('meta_description', null, ['class' => 'form-control',
    'placeholder' => 'Введите description',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('meta_keywords', 'meta keywords:') }}
    {{ Form::text('meta_keywords', null, ['class' => 'form-control',
    'placeholder' => 'Введите keywords',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('title', 'title:') }}
    {{ Form::text('title', null, ['class' => 'form-control',
    'placeholder' => 'Введите title',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('h1', 'h1:') }}
    {{ Form::text('h1', null, ['class' => 'form-control',
    'placeholder' => 'Введите h1',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('h2', 'h2:') }}
    {{ Form::text('h2', null, ['class' => 'form-control',
    'placeholder' => 'Введите h2',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('h3', 'h3:') }}
    {{ Form::text('h3', null, ['class' => 'form-control',
    'placeholder' => 'Введите h3',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('seo_text', 'Seo text:') }}
    {{ Form::textarea('seo_text', null, ['class' => 'form-control','id' => 'editor1', 'cols' => 30, 'rows' => 5]) }}
</div>


<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}
