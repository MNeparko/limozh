<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.keywords.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['route' => 'admin.keyword.store']) }}

<div class="form-group">
    {{ Form::label('valueEn', 'Ключевое слово (English):') }}
    {{ Form::text('valueEn', null, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}
    {{ Form::label('valueRu', 'Ключевое слово (Русский):') }}
    {{ Form::text('valueRu', null, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}
    {{ Form::label('valueDe', 'Ключевое слово (Deutsch):') }}
    {{ Form::text('valueDe', null, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}

</div>



<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}
