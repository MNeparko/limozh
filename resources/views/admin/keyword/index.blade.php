
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<a href="{{ route('admin.keyword.create') }}" class="btn btn-primary">Добавить ключевое слово</a>

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>ID</th>
    <th>Значение</th>
    <th>Дата создания</th>
    <th>Действия</th>
    </thead>
    @foreach($keywords as $keyword)

        <tr>
            <td>{{ $keyword->id }}</td>
            <td>{{ $keyword->value }}</td>
            <td>{{ $keyword->created_at->format('d-m-Y') }}</td>
            <td>
                <a href="{{ route('admin.keyword.edit',['id' => $keyword->id]) }}"
                   class="btn btn-primary" title="Редактировать"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>&nbsp;</a>
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ route('admin.keyword.delete', ['id' => $keyword->id]) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
</table>
{{ $keywords->links() }}

<?php $modalHeader = 'Удаление ключевого слова'; $modalBody = 'Вы уверены что хотите удалить ключевое слово?';?>
@includeif('admin.modal.modal-delete')