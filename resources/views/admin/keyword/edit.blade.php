<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.keywords.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::model($keyword, ['method' => 'PATCH','route' => ['admin.keyword.update', $keyword->id], 'files' => true]) }}

<div class="form-group">
    {{ Form::label('valueEn', 'Ключевое слово (English):') }}
    {{ Form::text('valueEn', $keyword->translationValue(\App\Models\Translation::LANG_EN) == null ? $keyword->value : $keyword->translationValue(\App\Models\Translation::LANG_EN)->value, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}
    {{ Form::label('valueRu', 'Ключевое слово (Русский):') }}
    {{ Form::text('valueRu', $keyword->translationValue(\App\Models\Translation::LANG_RU) == null ? $keyword->value : $keyword->translationValue(\App\Models\Translation::LANG_RU)->value, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}
    {{ Form::label('valueDe', 'Ключевое слово (Deutsch):') }}
    {{ Form::text('valueDe', $keyword->translationValue(\App\Models\Translation::LANG_DE) == null ? $keyword->value : $keyword->translationValue(\App\Models\Translation::LANG_DE)->value, ['class' => 'form-control',
    'placeholder' => 'Введите слово',
    ]) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}