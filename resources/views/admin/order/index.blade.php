
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>Order no.</th>
    <th>Имя</th>
    <th>E-mail покупателя</th>
    <th>Статус</th>
    <th>Дата заказа</th>
    <th>Действия</th>
    </thead>
    @foreach($orders as $order)

        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->name }}</td>
            <td>{{ $order->email }}</td>
            <td>{!! $order->getBadgeStatus() !!}</td>
            <td>{{ $order->created_at->format('d-m-Y') }}</td>
            <td>
                <a href="{{ route('admin.order.view',['id' => $order->id]) }}"
                   class="btn btn-primary" title="Просмотр"><i class="fa fa-eye" aria-hidden="true"></i>&nbsp;</a>
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ route('admin.order.delete', ['id' => $order->id]) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
</table>
{{ $orders->links() }}

<?php $modalHeader = 'Удаление заказа'; $modalBody = 'Вы уверены что хотите удалить заказ?';?>
@includeif('admin.modal.modal-delete')