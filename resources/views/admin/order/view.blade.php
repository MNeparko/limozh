<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.orders.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>

<h3>Order no. {{ $order->id }}</h3>
<p><b>Дата создания заказа:</b> {{ $order->created_at->format('d/m/Y') }}</p>
<p><b>Имя и фамилия клиента:</b> {{ $order->name }}</p>
<p><b>Email:</b> {{ $order->email }}</p>

@if(!empty($order->phone))<p><b>Phone:</b> {{ $order->phone }}</p>@endif
@if(!empty($order->zip))<p><b>Zip code:</b> {{ $order->email }}</p>@endif

<p><b>Статус заказа:</b> {!! $order->getBadgeStatus() !!}</p>
<p><b>Количество людей:</b> {{ $order->count }}</p>
<p><b>Количество забронированных столиков:</b> {{ $order->items->count() }}</p>
<p><b>Общая стоимость:</b> {{ $order->cost }} р.</p>
<hr style="background-color: black; height: 1px">
<h4>Дата, на которую произвели бронь.</h4>
<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>Дата</th>
    <th>Время</th>
    </thead>
    <tr>
        <td>
            {{ $order->reservation_date }}
        </td>
        <td>
            {{ $order->reservation_time }}
        </td>
    </tr>
</table>