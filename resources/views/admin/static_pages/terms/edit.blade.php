@foreach($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['method' => 'PATCH','route' => ['admin.static.terms.update']]) }}

<div class="form-group">
    {{ Form::label('textEn', 'Текст страницы включая html код (English):') }}
    {{ Form::textarea('textEn', $terms[0]->value, ['class' => 'form-control']) }}

    {{ Form::label('textRu', 'Текст страницы включая html код (Русский):') }}
    {{ Form::textarea('textRu', $terms[1]->value, ['class' => 'form-control']) }}

    {{ Form::label('textDe', 'Текст страницы включая html код (Deutsch):') }}
    {{ Form::textarea('textDe', $terms[2]->value, ['class' => 'form-control']) }}
</div>


<div class="form-group">
    {{ Form::submit('Сохранить изменения', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}


