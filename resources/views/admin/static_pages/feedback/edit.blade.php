@foreach($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['method' => 'PATCH','route' => ['admin.static.feedback.update']]) }}

<div class="form-group">
    {{ Form::label('textEn', 'Текст страницы Feedback включая html код (English):') }}
    {{ Form::textarea('textEn', $feedback[0]->value, ['class' => 'form-control']) }}

    {{ Form::label('textRu', 'Текст страницы Feedback включая html код (Русский):') }}
    {{ Form::textarea('textRu', $feedback[1]->value, ['class' => 'form-control']) }}

    {{ Form::label('textDe', 'Текст страницы Feedback включая html код (Deutsch):') }}
    {{ Form::textarea('textDe', $feedback[2]->value, ['class' => 'form-control']) }}
</div>


<div class="form-group">
    {{ Form::submit('Сохранить изменения', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}


