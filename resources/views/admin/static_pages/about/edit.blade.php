@foreach($errors->all() as $error)
    <p class="alert alert-danger">{{ $error }}</p>
@endforeach
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['method' => 'PATCH','route' => ['admin.static.about.update']]) }}

<h4>First text</h4>
<div class="form-group">
    {{ Form::label('textFirstEn', 'Текст страницы включая html код (English):') }}
    {{ Form::textarea('textFirstEn', $aboutFirst[0]->value, ['class' => 'form-control']) }}

    {{ Form::label('textFirstRu', 'Текст страницы включая html код (Русский):') }}
    {{ Form::textarea('textFirstRu', $aboutFirst[1]->value, ['class' => 'form-control']) }}

    {{ Form::label('textFirstDe', 'Текст страницы включая html код (Deutsch):') }}
    {{ Form::textarea('textFirstDe', $aboutFirst[2]->value, ['class' => 'form-control']) }}
</div>

<h4>Second text</h4>
<div class="form-group">
    {{ Form::label('textSecondEn', 'Текст страницы включая html код (English):') }}
    {{ Form::textarea('textSecondEn', $aboutSecond[0]->value, ['class' => 'form-control']) }}

    {{ Form::label('textSecondRu', 'Текст страницы включая html код (Русский):') }}
    {{ Form::textarea('textSecondRu', $aboutSecond[1]->value, ['class' => 'form-control']) }}

    {{ Form::label('textSecondDe', 'Текст страницы включая html код (Deutsch):') }}
    {{ Form::textarea('textSecondDe', $aboutSecond[2]->value, ['class' => 'form-control']) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить изменения', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}


