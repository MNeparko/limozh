<div class="modal fade" role="dialog" id="confirmDelete">
    <div class="modal-dialog">
        <div class="modal-content">
            {!! Form::open(['method' => 'DELETE', 'id'=>'delForm']) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>{{ $modalHeader }}</h3>
            </div>
            <div class="modal-body">
                <span id="pName"></span>
                <p>{{ $modalBody }}</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success">Удалить</button>
                <a href="#" class="btn" aria-hidden="true" data-dismiss="modal">Закрыть</a>
            </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>