<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.shop.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::model($shop, ['method' => 'PATCH','route' => ['admin.shop.update', $shop->id], 'files' => true]) }}

<div class="form-group">
    {{ Form::label('nameEn', 'Название категории (English):') }}
    {{ Form::text('nameEn', $shop->translationName(\App\Models\Translation::LANG_EN) == null ? $shop->name : $shop->translationName(\App\Models\Translation::LANG_EN)->value, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
    {{ Form::label('nameRu', 'Название категории (Русский):') }}
    {{ Form::text('nameRu', $shop->translationName(\App\Models\Translation::LANG_RU) == null ? $shop->name : $shop->translationName(\App\Models\Translation::LANG_RU)->value, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
    {{ Form::label('nameDe', 'Название категории (Deutsch):') }}
    {{ Form::text('nameDe', $shop->translationName(\App\Models\Translation::LANG_DE) == null ? $shop->name : $shop->translationName(\App\Models\Translation::LANG_DE)->value, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('keywords[]', 'Ключевые слова:') }}
    {{ Form::select('keywords[]', $keywords, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="form-group">
    {{ Form::label('descriptionEn', 'Описание (English):') }}
    {{ Form::textarea('descriptionEn', $shop->translationDescription(\App\Models\Translation::LANG_EN) == null ? $shop->description : $shop->translationDescription(\App\Models\Translation::LANG_EN)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionEn');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionRu', 'Описание (Русский):') }}
    {{ Form::textarea('descriptionRu', $shop->translationDescription(\App\Models\Translation::LANG_RU) == null ? $shop->description : $shop->translationDescription(\App\Models\Translation::LANG_RU)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionRu');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionDe', 'Описание (Deutsch):') }}
    {{ Form::textarea('descriptionDe', $shop->translationDescription(\App\Models\Translation::LANG_DE) == null ? $shop->description : $shop->translationDescription(\App\Models\Translation::LANG_DE)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionDe');
        }, 500);
    </script>

</div>

@if(!empty($images))
    <h4>Загруженные:</h4>
    <div class="admin-images">
        @foreach($images as $image)
            <a target="_blank" href="{{ \App\Models\ImageUpload::getUrl($image) }}">
                <img style="height: 10%; width: 10%;" src="{{ \App\Models\ImageUpload::getUrl($image) }}" alt="">
            </a><br>
        @endforeach
    </div>
@endif

<div class="form-group">
    {{ Form::label('pictures[]', 'Изображения:') }}
    {{ Form::file('pictures[]', array('multiple'=>true)) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>

{{ Form::close() }}

@includeIf('admin._forms.multiselect')