@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<a href="{{ route('admin.place.create') }}" class="btn btn-primary">Добавить</a>

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>ID</th>
    <th>Название</th>
    <th>Заведение</th>
    <th>Дата создания</th>
    <th>Действия</th>
    </thead>

    @foreach($places as $place)
        <tr>
            <td>{{ $place->id }}</td>
            <td>{{ $place->title }}</td>
            <td>{{ $place->shop->name }}</td>
            <td>{{ $place->created_at->format('d-m-Y') }}</td>
            <td>
                <a href="{{ route('admin.place.edit', ['id' => $place->id]) }}" class="btn btn-primary" title="Редактировать">
                    <i class="fa fa-pencil fa-fw" aria-hidden="true"></i>&nbsp
                </a>
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ route('admin.place.delete', ['id' => $place->id]) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
</table>
{{ $places->links() }}

<?php $modalHeader = 'Удаление'; $modalBody = 'Вы уверены что хотите удалить?';?>
@includeif('admin.modal.modal-delete')