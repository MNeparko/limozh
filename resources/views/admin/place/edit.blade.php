<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.place.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>

@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::model($place, ['method' => 'PATCH','route' => ['admin.place.update', $place->id], 'files' => true]) }}

<div class="form-group">
    {{ Form::label('title', 'Наименование') }}
    {{ Form::text('title', @$place->title) }}
</div>

<div class="form-group">
    {{ Form::label('shop_id[]', 'Заведения:') }}
    {{ Form::select('shop_id[]', $shops, @$place->shop_id, [
        'title' => 'Нажмите для выбора',
        'data-width' => 'fit'
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('count', 'Количество мест') }}
    {{ Form::text('count', @$place->count) }}
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="form-group">
    {{ Form::label('descriptionEn', 'Описание (English):') }}
    {{ Form::textarea('descriptionEn', $place->translationDescription(\App\Models\Translation::LANG_EN) == null ? $place->description : $place->translationDescription(\App\Models\Translation::LANG_EN)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionEn');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionRu', 'Описание (Русский):') }}
    {{ Form::textarea('descriptionRu', $place->translationDescription(\App\Models\Translation::LANG_RU) == null ? $place->description : $place->translationDescription(\App\Models\Translation::LANG_RU)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionRu');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionDe', 'Описание (Deutsch):') }}
    {{ Form::textarea('descriptionDe', $place->translationDescription(\App\Models\Translation::LANG_DE) == null ? $place->description : $place->translationDescription(\App\Models\Translation::LANG_DE)->value) }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionDe');
        }, 500);
    </script>

</div>

@if(!empty($images))
    <h4>Загруженные:</h4>
    <div class="admin-images">
        @foreach($images as $image)
            <a target="_blank" href="{{ \App\Models\ImageUpload::getUrl($image) }}">
                <img style="height: 10%; width: 10%;" src="{{ \App\Models\ImageUpload::getUrl($image) }}" alt="">
            </a><br>
        @endforeach
    </div>
@endif

<div class="form-group">
    {{ Form::label('pictures[]', 'Изображения:') }}
    {{ Form::file('pictures[]', array('multiple'=>true)) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>

{{ Form::close() }}