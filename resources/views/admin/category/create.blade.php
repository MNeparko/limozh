<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <a class="btn btn-primary" href="{{ route('admin.categories.index') }}"> Назад</a>
        </div>
    </div>
</div>
<br>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif

{{ Form::open(['route' => 'admin.category.store', 'files' => true]) }}

<div class="form-group">
    {{ Form::label('nameEn', 'Название категории (English):') }}
    {{ Form::text('nameEn', null, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
    {{ Form::label('nameRu', 'Название категории (Русский):') }}
    {{ Form::text('nameRu', null, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
    {{ Form::label('nameDe', 'Название категории (Deutsch):') }}
    {{ Form::text('nameDe', null, ['class' => 'form-control',
    'placeholder' => 'Введите название',
    ]) }}
</div>

<div class="form-group">
    {{ Form::label('keywords[]', 'Ключевые слова:') }}
    {{ Form::select('keywords[]', $keywords, null, ['multiple' => true, 'class' => 'selectpicker',
    'title' => 'Нажмите для выбора', 'data-width' => 'fit']) }}
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
<div class="form-group">
    {{ Form::label('descriptionEn', 'Описание (English):') }}
    {{ Form::textarea('descriptionEn') }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionEn');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionRu', 'Описание (Русский):') }}
    {{ Form::textarea('descriptionRu') }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionRu');
        }, 500);
    </script>

</div>
<div class="form-group">
    {{ Form::label('descriptionDe', 'Описание (Deutsch):') }}
    {{ Form::textarea('descriptionDe') }}

    <script>
        setTimeout(function () {
            CKEDITOR.replace('descriptionDe');
        }, 500);
    </script>

</div>


<div class="form-group">
    {{ Form::label('main', 'Показать на главной странице с изображением:') }}
    {{ Form::checkbox('main') }}
</div>

<div class="form-group">
    {{ Form::label('image', 'Фото:') }}
    {{ Form::file('image', null, [
    'class' => 'form-control',
    'placeholder' => 'Изображение',
    ], ['accept' => 'image/x-png,image/gif,image/jpeg']) }}
</div>

<div class="form-group">
    {{ Form::submit('Сохранить', ['class' => 'btn btn-primary form-control']) }}
</div>
{{ Form::close() }}
@includeIf('admin._forms.multiselect')