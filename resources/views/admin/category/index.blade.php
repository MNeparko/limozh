
@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<a href="{{ route('admin.category.create') }}" class="btn btn-primary">Добавить категорию</a>

<table class="table table-striped table-bordered table-responsive table-admin">
    <thead>
    <th>ID</th>
    <th>Название</th>
    <th>Количество фото</th>
    <th>Дата создания</th>
    <th>Действия</th>
    </thead>
    @foreach($categories as $category)

        <tr>
            <td>{{ $category->id }}</td>
            <td>{{ $category->name }}</td>
            <td>{{ $category->products->count() }}</td>
            <td>{{ $category->created_at->format('d-m-Y') }}</td>
            <td>
                <a href="{{ route('admin.category.edit',['id' => $category->id]) }}"
                   class="btn btn-primary" title="Редактировать"><i class="fa fa-pencil fa-fw" aria-hidden="true"></i>&nbsp;</a>
                <a class="btn btn-danger" href="#myModal" data-target="#confirmDelete"
                   data-action="{{ route('admin.category.delete', ['id' => $category->id]) }}"
                   title="Удалить" aria-label="Удалить"
                   onclick="pushId(this.getAttribute('data-action'))" data-toggle="modal">
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                </a>
            </td>
        </tr>
    @endforeach
</table>
{{ $categories->links() }}

@php $modalHeader = 'Удаление категории'; $modalBody = 'Вы уверены что хотите удалить категорию?'; @endphp
@includeif('admin.modal.modal-delete')