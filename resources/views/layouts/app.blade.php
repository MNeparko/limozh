<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">



    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="{{ asset('images/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="{{ asset('images/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="{{ asset('images/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" href="{{ asset('images/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="shortcut icon" href="{{ asset('images/ico/favicon.png') }}">

    <!-- CSS Plugins -->
    <link rel="stylesheet" type="text/css" href="{{ asset('bootstrap/css/bootstrap.min.css') }}" media="screen">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/component.css') }}" rel="stylesheet">

    <!-- CSS Font Icons -->
    <link rel="stylesheet" href="{{ asset('css/icons/open-iconic/font/css/open-iconic-bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/ionicons/css/ionicons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/icons/rivolicons/style.css') }}">

    {{--@if((Route::getCurrentRoute()->getName() == 'site.product.cart') ||
     (Route::getCurrentRoute()->getName() == 'payment.status') || (Route::getCurrentRoute()->getName() == 'addmoney.checkout'))
    <!--CSS bundle -->
    <link rel="stylesheet" media="all" href="{{ asset('css/bundle.min.css') }}" />
    <!-- Fav and Touch Icons -->
    @endif--}}
    <style type="text/css">
        .ajax-load{
            background: #e1e1e1;
            padding: 10px 0px;
            width: 100%;
        }
    </style>
    <!--Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&amp;subset=latin-ext" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- CSS Custom -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- Add your own style -->
    <link href="{{ asset('css/your-style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/my.css') }}" rel="stylesheet">


    <link href="{{ asset('css/new/main.css') }}" rel="stylesheet">
    <link href="{{ asset('css/new/style.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- start Container Wrapper -->
<div class="wrapper container-wrapper">

    <!-- start Header -->
    <header id="header">

        <!-- start Navbar (Menu) -->
        <nav class="navbar navbar-default navbar-fixed-top navbar-sticky-function">

            <div class="container">

                <div class="navbar-header">
                    <a class="navbar-brand" href="{{ route('site.index') }}">
                        <img src="{{ asset('images/logo.png') }}" alt="Лимож Лого">
                    </a>
                </div>

                <div id="navbar" class="collapse navbar-collapse navbar-arrow pull-left">


                </div><!--/.nav-collapse -->

                <div class="pull-right">
                    <div class="navbar-mini">
                        <ul class="clearfix">


                            <li class="dropdown hidden-xs">
                                <a id="currncy-dropdown" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-haspopup="true" aria-expanded="false">
                                    <i class="ion-android-globe hidden-xss"></i>

                                    @if(App::isLocale('en'))
                                        English
                                    @endif
                                    @if(App::isLocale('ru'))
                                        Русский
                                    @endif
                                    @if(App::isLocale('de'))
                                        Deutsch
                                    @endif

                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="language-dropdown">
                                    <li><a href="{{ route('site.lang', ['locale' => 'en']) }}">English</a></li>
                                    <li><a href="{{ route('site.lang', ['locale' => 'ru']) }}">Русский</a></li>
                                    <li><a href="{{ route('site.lang', ['locale' => 'de']) }}">Deutsch</a></li>
                                </ul>
                            </li>

                            <li class="dropdown visible-xs">
                                <a id="currncy-language-dropdown" class="dropdown-toggle" data-toggle="dropdown"
                                   role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-cog"></i>
                                </a>
                                <ul class="dropdown-menu" role="menu">

                                    <li><a href="{{ route('site.lang', ['locale' => 'en']) }}">English</a></li>
                                    <li><a href="{{ route('site.lang', ['locale' => 'ru']) }}">Русский</a></li>
                                    <li><a href="{{ route('site.lang', ['locale' => 'de']) }}">Deutsch</a></li>
                                </ul>
                            </li>
                            {{--<li>
                                <a href="{{ route('site.product.cart') }}">
                                    <i class="ion-bag hidden-xss" aria-hidden="true"></i>
                                    {!! trans('index.cart') !!}
                                    <span class="badge">
                                        {{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}
                                    </span>
                                </a>
                            </li>--}}
                            {{--<a data-toggle="modal" href="{{ route('home') }}" class="btn">Sign up/in</a>--}}
                            @if (!Auth::guest())

                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                  style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif

                        </ul>
                    </div>

                </div>

            </div>

            <div id="slicknav-mobile"></div>

        </nav>
        <!-- end Navbar (Menu) -->

    </header>
    <!-- end Header -->

    <!-- start Main Wrapper -->
@yield('content')
<!-- end Main Wrapper -->
    <footer class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-sm-4 col-md-3 mb-30-xs">
                    <h4 class="footer-title">Limoges</h4>
                    <ul class="menu-footer">
                        <li>
                            <a href="{{ route('site.about') }}">{!! trans('index.about_us') !!}</a>
                        </li>
                        <li><a href="{{ route('site.contact') }}">{!! trans('index.contact') !!}</a></li>
                        <li><a href="{{ route('site.shops.index') }}">{!! trans('index.license_agreement') !!}</a></li>
                        {{--<li><a href="{{ route('site.products.index') }}">{!! trans('index.our_production') !!}</a></li>--}}
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-sm-4 col-md-3 mb-30-xs">
                    <h4 class="footer-title">{!! trans('index.language_of_the_site') !!}</h4>
                    <ul class="menu-footer"> <li><a href="{{ route('site.lang', ['locale' => 'en']) }}">English</a></li>
                        <li><a href="{{ route('site.lang', ['locale' => 'de']) }}">Deutsch</a></li>
                        <li><a href="{{ route('site.lang', ['locale' => 'ru']) }}">Русский</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-sm-4 col-md-3 mb-30-xs">
                    <h4 class="footer-title">{!! trans('index.legal_information') !!}</h4>
                    <ul class="menu-footer">
                        <li><a href="{{ route('site.privacy') }}">{!! trans('index.privacy_policy') !!}</a></li>
                        <li><a href="{{ route('site.terms') }}">{!! trans('index.terms_of_use') !!}</a></li>
                    </ul>
                </div>
                <div class="col-xs-12 col-sm-12 col-sm-4 col-md-3 mb-30-xs">
                    <h5 class="footer-title">{!! trans('index.contact') !!}</h5>
                    <p class="footer-phone-number"><a href="mailto:info@Limoges.by">info@limoges.by</a></p>
                </div>
            </div>
        </div>
    </footer>

        <footer class="secondary-footer">

            <div class="container">

                <div class="row">

                    <div class="col-sm-6">
                        <p class="copy-right">&#169; Copyright 2018 Limoges</p>
                    </div>

                    <div class="col-sm-6">

                    </div>

                </div>

            </div>

        </footer>



    </div>

</div> <!-- / .wrapper -->
<!-- end Container Wrapper -->


<!-- start Back To Top -->
<div id="back-to-top">
    <a href="{{ route('site.index') }}"><i class="fa fa-arrow-up" aria-hidden="true"></i></a>
</div>
<!-- end Back To Top -->


<!-- JS -->

<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.jscroll.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-migrate-1.2.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bootstrap/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.waypoints.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.easing.1.3.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/SmoothScroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.slicknav.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.placeholder.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/spin.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.introLoader.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/select2.full.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.responsivegrid.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/readmore.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-filestyle.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.flex-images.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.countimator.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.countimator.wheel.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.simpletip-1.0.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/customs.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/gallery.js') }}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.13.0/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

<script>
    jQuery(function () {
        jQuery('#createOrderTimePicker').datetimepicker({
            format: 'LT'
        });
        jQuery('#createOrderDatePicker').datetimepicker({
            viewMode: 'days',
            format: 'DD/MM/YYYY'
        });
    });

    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });

    /*jQuery('#createOrder .checkOrder').on('click', function () {
        var photoId = jQuery(this).data('id');

        jQuery.ajax({
            type: 'POST',
            url: '/ajaxRequest',
            data: {photo_id: photoId},
            success: function (data) {

                jQuery('.modal-body').html(data.success);
                jQuery('.modal-title').html('Download preview');
                jQuery('input[name="modal_photo_id"]').val(photoId);
                $('#myModal').modal('show');
            }
        });
    });*/
</script>

</body>
</html>