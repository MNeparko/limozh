<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'site.index',
    'uses' => 'SiteController@index'
]);

Auth::routes();

Route::get('/home', [
    'as' => 'home',
    'uses' => 'HomeController@index'
]);

Route::get('contact', [
    'as' => 'site.contact',
    'uses' => 'SiteController@contact'
]);

Route::post('contact', [
    'as' => 'site.message.send',
    'uses' => 'SiteController@sendMessage'
]);

Route::post('order/create',[
    'as'=>'order.store',
    'uses'=>'OrderController@store',
]);

Route::get('category/{id}',[
    'as'=>'site.category.view',
    'uses'=>'CategoryController@view',
]);

Route::get('products', [
    'as' => 'site.products.index',
    'uses' => 'ProductController@index'
]);

Route::get('product/{id}', [
    'as' => 'site.product.view',
    'uses' => 'ProductController@view'
]);

Route::get('shops', [
    'as' => 'site.shops.index',
    'uses' => 'ShopController@index'
]);

Route::get('shop/{id}', [
    'as' => 'site.shop.view',
    'uses' => 'ShopController@view'
]);

Route::get('product/detail/{id}',[
    'as'=>'site.product.detail',
    'uses'=>'ProductController@detail',
]);

Route::get('similar/{id}',[
    'as'=>'site.product.similar',
    'uses'=>'SiteController@searchSimilar',
]);

Route::post('ajaxRequest', [
    'uses' => 'ProductController@ajaxPreview'
]);

Route::post('downloadPreview', [
    'as' => 'site.product.preview.download',
    'uses' => 'ProductController@downloadPreviewPhoto'
]);

Route::get('photos_keyword/{id}',[
    'as'=>'site.product.keyword',
    'uses'=>'SiteController@searchKeywordPhotos',
]);

Route::get('pricing',[
    'as'=>'site.pricing',
    'uses'=>'SiteController@pricing',
]);

Route::get('add-to-cart/{id}',[
    'as'=>'site.product.addToCart',
    'uses'=>'ProductController@addToCart',
]);

Route::get('cart',[
    'as'=>'site.product.cart',
    'uses'=>'ProductController@getCart',
]);

Route::get('checkout',[
    'as'=>'site.checkout',
    'uses'=>'ProductController@checkout',
]);

Route::get('clear-cart',[
    'as'=>'site.cart.clear',
    'uses'=>'ProductController@clearCart',
]);

Route::get('minus-item-cart/{priceId}/{photoId}',[
    'as'=>'site.cart.minusItem',
    'uses'=>'ProductController@minusItemCart',
]);

Route::get('delete-item-cart/{priceId}/{photoId}',[
    'as'=>'site.cart.deleteItem',
    'uses'=>'ProductController@deleteItemCart',
]);

Route::get('about',[
    'as'=>'site.about',
    'uses'=>'SiteController@about',
]);

Route::get('feedback',[
    'as'=>'site.feedback',
    'uses'=>'SiteController@feedback',
]);

Route::get('license',[
    'as'=>'site.license',
    'uses'=>'SiteController@license',
]);

Route::get('privacy',[
    'as'=>'site.privacy',
    'uses'=>'SiteController@privacy',
]);

Route::get('terms',[
    'as'=>'site.terms',
    'uses'=>'SiteController@terms',
]);

Route::post('payment', array('as' => 'addmoney.paypal','uses' => 'AddMoneyController@postPaymentWithpaypal'));

Route::get('paypal', array('as' => 'payment.status','uses' => 'AddMoneyController@getPaymentStatus'));

Route::get('checkout', array('as' => 'addmoney.checkout','uses' => 'AddMoneyController@checkout'));

Route::get('lang/{locale}', [
    'as' => 'site.lang',
    'Middleware' => 'LanguageSwitcher',
    'uses' => 'LanguageController@index'
]);

Route::get('payComplete', [
    'as' => 'site.test',
    'uses' => 'AddMoneyController@payComplete',
]);
