<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('translations')->insert([
            'id' => 1,
            'item_id' => 0,
            'type' => 10,
            'language' => 0,
            'value' => '<h1> The GGTUP "Limoges" includes: </ h1>
<h3> -cooked confectionery-coffee shop "Raskosha 1795" (since 2009.) </ h3>
<h3> - confectionery-coffee house "Spacus" (since August 2015) </ h3>
<h3> - Ponchykawa (from 2017) </ h3>
<h3> - Cafe seasonal "Cala Palaca" (since 2016) </ h3>'
        ]);

        DB::table('translations')->insert([
            'id' => 2,
            'item_id' => 0,
            'type' => 10,
            'language' => 1,
            'value' => '<h1>В состав ГГТУП «Лимож» входят:</h1>
<h3>-кондитерская-кофейня «Раскоша 1795» (с 2009г.)</h3>
<h3>- кондитерская-кофейня «Спакуса»( с августа 2015г.)</h3>
<h3>- Пончыкава(с 2017г)</h3>
<h3>- Кафе сезонное "Каля Палаца"(с 2016г)</h3>'
        ]);

        DB::table('translations')->insert([
            'id' => 3,
            'item_id' => 0,
            'type' => 10,
            'language' => 2,
            'value' => '<h1> Das GGTUP "Limoges" beinhaltet: </ h1>
<h3> gekochtes Konditorei-Café "Raskosha 1795" (seit 2009). </ h3>
<h3> - Süßwaren-Kaffeehaus "Spacus" (seit August 2015) </ h3>
<h3> - Ponchykawa (ab 2017) </ h3>
<h3> - Saisonales Café "Cala Palaca" (seit 2016) </ h3>'
        ]);

        DB::table('translations')->insert([
            'id' => 4,
            'item_id' => 0,
            'type' => 11,
            'language' => 0,
            'value' => 'About Limoges, GGTUP<br/>
Limoges, GGTUP<br/>
State enterprise<br/>
Year of foundation: 1986<br/>
Number of employees: 120<br/>
UNP: 500154505<br/>
<br/>
Offered products / services:<br/>
Production: brioche; pastry confectionery products; cupcakes; pizza; cakes<br/>
Works / services: catering services<br/>
Retail trade: haberdashery; wine and vodka products; Confectionery; hosiery; stationery; cosmetics; soap; perfumery; Food; household goods'
        ]);

        DB::table('translations')->insert([
            'id' => 5,
            'item_id' => 0,
            'type' => 11,
            'language' => 1,
            'value' => 'О компании Лимож, ГГТУП <br/>
Лимож, ГГТУП<br/>
Государственное предприятие<br/>
Год основания: 1986<br/>
Количество сотрудников: 120<br/>
УНП: 500154505<br/>
<br/>
Предлагаемая продукция/услуги:<br/>
Производство: булочки; изделия кондитерские мучные; кексы; пицца; торты<br/>
Работы/услуги: услуги общепита<br/>
Розничная торговля: галантерея; изделия вино-водочные; изделия кондитерские; изделия чулочно-носочные; канцтовары; косметика; мыло; парфюмерия; продукты питания; хозтовары'
        ]);

        DB::table('translations')->insert([
            'id' => 6,
            'item_id' => 0,
            'type' => 11,
            'language' => 2,
            'value' => 'Über Limoges, GGTUP<br/>
Limoges, GGTUP<br/>
Staatsunternehmen<br/>
Gründungsjahr: 1986<br/>
Anzahl der Angestellten: 120<br/>
UNP: 500154505<br/>
<br/>
Angebotene Produkte / Dienstleistungen:<br/>
Produktion: Brioche; Konditorwaren; Konditorwaren; Muffins; Pizza; Kuchen<br/>
Werke / Dienstleistungen: Catering-Dienstleistungen<br/>
Einzelhandel: Kurzwaren; Wein und Wodka-Produkte; Süßwaren; Strumpfwaren; Schreibwaren; Kosmetika; Seife; Parfümerie; Lebensmittel; Haushaltswaren'
        ]);

        DB::table('translations')->insert([
            'id' => 7,
            'item_id' => 0,
            'type' => 12,
            'language' => 0,
            'value' => '<h1> Contacts: </ h1>
<h3> City / Region: Гродненская область </ h3>
<h3> Address: 230005, Grodno, ul. Brikelya, 10 </ h3>
<h3> Telephone: (0152) 76-67-01-hands, 72-12-79-accountant., 76-83-55-зам.рук. </ h3>
<h3> Fax: (0152) 41-22-11-barg.ot </ h3>
<h3> E-mail: limoj@tut.by </ h3>'
        ]);

        DB::table('translations')->insert([
            'id' => 8,
            'item_id' => 0,
            'type' => 12,
            'language' => 1,
            'value' => '<h1>Контакты:</h1>
<h3>Город/регион: Гродненская область</h3>
<h3>Адрес: 230005, г. Гродно, ул. Брикеля, 10</h3>
<h3>Телефон: (0152)76-67-01-рук., 72-12-79-бухгал., 76-83-55-зам.рук.</h3>
<h3>Факс: (0152)41-22-11-торг.отд.</h3>
<h3>E-mail: limoj@tut.by</h3>'
        ]);

        DB::table('translations')->insert([
            'id' => 9,
            'item_id' => 0,
            'type' => 12,
            'language' => 2,
            'value' => '<h1> Kontakte: </ h1>
<h3> Stadt / Region: Grönland </ h3>
<h3> Adresse: 230005, Grodno, Ul. Brikelya, 10 </ h3>
<h3> Telefon: (0152) 76-67-01-hands, 72-12-79-accountant., 76-83-55-зам.рук. </ h3>
<h3> Fax: (0152) 41-22-11-barg.ot </ h3>
<h3> E-Mail: limoj@tut.by </ h3>'
        ]);
    }
}
