<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsToCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_to_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')
                ->index('products_to_categories_id');
            $table->unsignedInteger('category_id')
                ->index('products_to_categories_category_id');

            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')->on('products');
            $table->foreign('category_id')
                ->references('id')->on('categories');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_to_categories');
    }
}
