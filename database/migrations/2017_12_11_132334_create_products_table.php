<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('link')->unique();
            $table->mediumText('description')->nullable();
            $table->string('units')->comment('Единиццы измерения товара')->nullable();
            $table->double('price', 10, 2)->comment('Сумма за одну единицу товара');
            $table->double('old_price', 10, 2)->nullable();
            $table->string('stock_start', 16)->comment('Дата начала акции')->nullable();
            $table->string('stock_end', 16)->comment('Дата завершения акции')->nullable();
            $table->integer('priority')->comment('Приоритет вывода товара')->nullable();
            $table->integer('is_new')->comment('Флаг, является ли товар новым')->nullable();
            $table->integer('is_stock')->comment('Флаг, является ли товар акционным')->nullable();
            $table->integer('in_top')->comment('Флаг, является ли товар в топе')->nullable();

            $table->unsignedInteger('shop_id')
                ->index('products_to_shop_id');

            $table->foreign('shop_id')
                ->references('id')->on('shop');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
