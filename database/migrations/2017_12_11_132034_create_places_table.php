<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->mediumText('description')->nullable();
            $table->integer('count')->comment('Количество мест за столом')->nullable();
            $table->integer('in_vip')->comment('Флаг, является ли стол vip')->nullable();

            $table->unsignedInteger('shop_id')
                ->index('products_to_shop_id');

            $table->foreign('shop_id')
                ->references('id')->on('shop');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places');
    }
}
