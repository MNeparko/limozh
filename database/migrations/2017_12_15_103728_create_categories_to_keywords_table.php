<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_to_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('category_id')
                ->index('categories_to_keywords_category_id');
            $table->unsignedInteger('keyword_id')
                ->index('categories_to_keywords_keyword_id');

            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')->on('categories');
            $table->foreign('keyword_id')
                ->references('id')->on('keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_to_keywords');
    }
}
