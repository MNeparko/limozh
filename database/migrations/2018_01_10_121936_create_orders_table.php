<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('company')->nullable();
            $table->string('zip')->nullable();
            $table->string('city')->nullable();
            $table->string('phone')->nullable();
            $table->string('email');
            $table->boolean('terms');
            $table->boolean('subscribe')->default(false);
            $table->unsignedInteger('payment')->nullable();
            $table->float('cost')->comment('Цена брониварония');
            $table->integer('count')->comment('Количество человек');
            $table->smallInteger('status')
                ->default(0);
            $table->date('reservation_date')->comment('Дата бронирования');
            $table->string('reservation_time')->comment('Время бронирования');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
