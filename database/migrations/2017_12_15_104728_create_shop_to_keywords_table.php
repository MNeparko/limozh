<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_to_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('shop_id')
                ->index('shop_to_keywords_shop_id');
            $table->unsignedInteger('keyword_id')
                ->index('shop_to_keywords_keyword_id');

            $table->timestamps();

            $table->foreign('shop_id')
                ->references('id')->on('shop');
            $table->foreign('keyword_id')
                ->references('id')->on('keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_to_keywords');
    }
}
