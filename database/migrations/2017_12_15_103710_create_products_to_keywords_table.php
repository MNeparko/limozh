<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsToKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_to_keywords', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')
                ->index('products_to_keywords_photo_id');
            $table->unsignedInteger('keyword_id')
                ->index('products_to_keywords_keyword_id');

            $table->unsignedInteger('sign')
                ->default(0)
                ->comment('Значимость по сравнению с остальными.');

            $table->timestamps();

            $table->foreign('product_id')
                ->references('id')->on('products');
            $table->foreign('keyword_id')
                ->references('id')->on('keywords');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_to_keywords');
    }
}
