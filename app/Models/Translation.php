<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Translation extends Model
{
    const TYPE_CATEGORY_NAME = 1;
    const TYPE_CATEGORY_DESCRIPTION = 2;

    const TYPE_PRODUCT_TITLE = 3;
    const TYPE_KEYWORD_VALUE = 6;

    const TYPE_PAGE_FEEDBACK = 7;
    const TYPE_PAGE_TERMS = 8;
    const TYPE_PAGE_PRIVACY = 9;
    const TYPE_PAGE_LICENSE = 10;
    const TYPE_PAGE_ABOUT_FIRST = 11;
    const TYPE_PAGE_ABOUT_SECOND = 12;

    const TYPE_SHOP_NAME = 13;
    const TYPE_SHOP_DESCRIPTION = 14;

    const TYPE_PLACE_DESCRIPTION = 15;

    const LANG_EN = 0;
    const LANG_RU = 1;
    const LANG_DE = 2;

    private static $lang = [
        'de' => 2,
        'ru' => 1,
        'en' => 0
    ];

    protected $table = 'translations';

    protected $fillable = ['item_id', 'type', 'language', 'value'];

    public function getType($type_int = null)
    {
        return $this->where('type', $type_int);
    }

    public static function currentLang()
    {
        return self::$lang[App::getLocale()];
    }

    public function getEn()
    {
        return $this->where('language', self::LANG_EN);
    }

    public function getDe()
    {
        return $this->where('language', self::LANG_DE);
    }

    public function getRu()
    {
        return $this->where('language', self::LANG_RU);
    }
}
