<?php

namespace App\Models;

class Cart
{
    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;
    public $totalDiscountPrice = 0;
    public $totalResultPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
            $this->totalDiscountPrice = $oldCart->totalDiscountPrice;
            $this->totalResultPrice = $oldCart->totalResultPrice;
        }

    }

    public function add($photo, $photoId, $sizeId = NULL)
    {
        $storedItem = ['qty' => 0, 'price' => $photo->price, 'item' => $photo];
        if ($this->items) {
            if (array_key_exists(1, $this->items)) {
                if (array_key_exists($photoId, $this->items[1])) {
                    $storedItem = $this->items[1][$photoId];
                }
            }
        }

        $storedItem['qty']++;
        $storedItem['price'] = $photo->price * $storedItem['qty'];
        $this->items[1][$photoId] = $storedItem;
        $this->totalQty++;
        $this->totalPrice += $photo->price;
    }

    public function minusItem($photo, $photoId, $sizeId = NULL)
    {
        $storedItem = ['qty' => 0, 'price' => $photo->price, 'item' => $photo];
        if ($this->items) {
            if (array_key_exists(1, $this->items)) {
                if (array_key_exists($photoId, $this->items[1])) {
                    $storedItem = $this->items[1][$photoId];
                }
            }
        }

        if ($storedItem['qty']>1) {
            $storedItem['qty'] -= 1;
            $storedItem['price'] = $photo->price * $storedItem['qty'];
            $this->items[1][$photoId] = $storedItem;
            $this->totalQty--;
            $this->totalPrice -= $photo->price;
        }

    }

    public function deleteItem($photo, $photoId, $sizeId = NULL)
    {
        $size = Size::find($sizeId);
        if ($this->items) {
            if (array_key_exists($sizeId, $this->items)) {
                if (array_key_exists($photoId, $this->items[$sizeId])) {
                    $storedItem = $this->items[$sizeId][$photoId];
                    $this->totalQty -= $storedItem['qty'];
                    $this->totalPrice -= $size->cost * $storedItem['qty'];
                    if (count($this->items[$sizeId])>1) {
                        unset($this->items[$sizeId][$photoId]);
                    } else {
                        unset($this->items[$sizeId]);
                    }
                }
            }
        }

    }

    public function reCalcPrice()
    {
        if ($this->items) {
            $discounts = PriceItem::orderBy('images_quantity', 'ASC')->get();

            foreach ($discounts as $discount) {
                if ($this->totalQty >= $discount->images_quantity)
                    $this->totalDiscountPrice = $this->totalPrice*($discount->percent/100);
            }

            if ($this->totalDiscountPrice > 0)
                $this->totalResultPrice = $this->totalPrice - $this->totalDiscountPrice;
            else
                $this->totalResultPrice = $this->totalPrice;
        }
    }
}
