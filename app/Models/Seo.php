<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $table = 'seo';

    protected $fillable = ['url_path', 'meta_description', 'meta_keywords', 'title', 'h1', 'h2', 'h3', 'seo_text'];
}
