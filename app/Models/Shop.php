<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shop extends Model
{
    /**
     * ID заведения, которое выводить на главную
     *
     * @var int
     */
    public static $shopInMain = 1;

    use SoftDeletes;

    protected $table = 'shop';

    protected $fillable = ['name', 'description'];

    protected $dates = ['deleted_at'];

    public function products()
    {
        return $this->hasMany(Product::class, 'shop_id', 'id');
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class,'shop_to_keywords', 'shop_id', 'keyword_id');
    }

    /**
     * @param int $langInt
     * @return Model|null|static
     */
    public function translationName($langInt = Translation::LANG_EN)
    {
        return $this->hasOne(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_SHOP_NAME)->where('language', $langInt)
            ->first();
    }

    /**
     * @param int $langInt
     * @return Model|null|static
     */
    public function  translationDescription($langInt = Translation::LANG_EN)
    {
        return $this->hasOne(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_SHOP_DESCRIPTION)->where('language', $langInt)
            ->first();
    }

    public function translations()
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_SHOP_DESCRIPTION)
            ->orWhere('type', Translation::TYPE_SHOP_NAME);
    }

    /**
     * @param $id
     * @return int
     */
    public static function maxPlaces($id)
    {
        $allPlaces = 0;

        $places = Place::where(['shop_id' => $id])
            ->select('count')
            ->get();

        foreach ($places as $place) {
            $allPlaces += $place->count;
        }

        return $allPlaces;
    }
}
