<?php


namespace App\Models;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ImageUpload
{
    /**
     * @param $file
     * @param $id
     * @param $folderName
     * @return bool
     */
    public static function saveImage($file, $id, $folderName)
    {
        $filesInFolder = Storage::allFiles('public/images/' . $folderName . '/' . $id);

        if (!empty($filesInFolder)) {
            Storage::delete($filesInFolder);
        }

        if ($file->store('public/images/' . $folderName . '/' . $id))
            return true;
        else
            return false;
    }

    /**
     * Сохранение нескольких изображений
     * @param $files
     * @param $id
     * @param $folderName
     * @return bool
     */
    public static function saveSeveralImages($files, $id, $folderName)
    {
        $filesInFolder = Storage::allFiles('public/images/' . $folderName . '/' . $id);

        if (!empty($filesInFolder)) {
            Storage::delete($filesInFolder);
        }

        foreach ($files as $file)
            $file->store('public/images/' . $folderName . '/' . $id);

        return true;
    }

    /**
     * @param $id
     * @param $folderName
     */
    public static function deleteImage($id, $folderName)
    {
        $pathToFile = Storage::allFiles('public/images/' . $folderName . '/' . $id );

        if (!empty($pathToFile))
            Storage::deleteDirectory('public/images/' . $folderName . '/' . $id );
    }

    /**
     * @param $id
     * @param $folderName
     * @return null
     */
    public static function getImage($id, $folderName)
    {
        $pathToImage = Storage::allFiles('public/images/' . $folderName . '/' . $id);

        if (empty($pathToImage))
            return null;
        else
            return $pathToImage;
    }

    /**
     * @param $id
     * @param $folderName
     * @return null
     */
    public static function getThumbImage($id, $folderName)
    {
        $pathToImage = Storage::allFiles('public/images/' . $folderName . '/' . $id . '/_thumb');

        if (empty($pathToImage))
            return null;
        else
            return $pathToImage;
    }

    /**
     * @param $id
     * @param $folderName
     * @return null
     */
    public static function getDetailImage($id, $folderName)
    {
        $pathToImage = Storage::allFiles('public/images/' . $folderName . '/' . $id . '/_detail');

        if (empty($pathToImage))
            return null;
        else
            return $pathToImage;
    }

    public static function getUrl($image)
    {
        return Storage::disk('local')->url($image);
    }
}