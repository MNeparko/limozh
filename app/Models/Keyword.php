<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Keyword extends Model
{
    protected $table = 'keywords';

    protected $fillable = ['value'];

    public function products()
    {
        return $this->belongsToMany(Product::class,'products_to_keywords', 'keyword_id', 'product_id')->withPivot('sign');
    }

    public function translations()
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_KEYWORD_VALUE);
    }

    public function translationValue($lang_int = Translation::LANG_EN)
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_KEYWORD_VALUE)->where('language', $lang_int)
            ->first();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class,'categories_to_keywords', 'keyword_id', 'category_id');
    }

    public function shop()
    {
        return $this->belongsToMany(Shop::class,'shop_to_keywords', 'keyword_id', 'shop_id');
    }
}
