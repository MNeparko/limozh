<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';

    protected $fillable = ['order_id', 'place_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function place()
    {
        return $this->hasOne(Place::class, 'id', 'place_id');
    }


}
