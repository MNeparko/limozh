<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Place extends Model
{
    use SoftDeletes;

    protected $table = 'places';

    protected $fillable = ['title', 'description', 'count', 'in_vip', 'shop_id'];

    protected $dates = ['deleted_at'];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    /**
     * @param int $langInt
     * @return Model|null|static
     */
    public function  translationDescription($langInt = Translation::LANG_EN)
    {
        return $this->hasOne(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_PLACE_DESCRIPTION)->where('language', $langInt)
            ->first();
    }

    public function translations()
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_PLACE_DESCRIPTION);
    }
}
