<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_COMPLETE = 1;
    const STATUS_NOT_PAID = 0;


    private static $nameStatus = [
        0 => 'Not paid',
        1 => 'Complete'
    ];

    public static $choosePayment = [
        1 => 'Pay via credit cart',
        2 => 'PayPal',
        3 => 'Pay via bank transfer'
    ];

    protected $table = 'orders';

    protected $fillable = ['name','company','zip','city', 'phone','terms','subscribe','payment', 'email', 'cost', 'status', 'reservation_date', 'reservation_time', 'count'];

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function getBadgeStatus()
    {
        if ($this->status == self::STATUS_COMPLETE)
            return '<span class="label label-success">'.self::$nameStatus[$this->status].'</span>';

        if ($this->status == self::STATUS_NOT_PAID)
            return '<span class="label label-danger">'.self::$nameStatus[$this->status].'</span>';
    }
}
