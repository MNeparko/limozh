<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $table = 'products';

    protected $fillable = ['title', 'link', 'description', 'units', 'price', 'old_price', 'stock_start', 'stock_end', 'priority', 'is_new', 'is_stock', 'in_top', 'shop_id'];

    protected $dates = ['deleted_at'];

    public function shop()
    {
        return $this->belongsTo(Shop::class);
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category','products_to_categories', 'product_id', 'category_id');
    }

    public function translations()
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_PRODUCT_TITLE);
    }

    public function translationTitle($lang_int = Translation::LANG_EN)
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_PRODUCT_TITLE)->where('language', $lang_int)
            ->first();
    }

    public function getFirstImage()
    {
        $images = ImageUpload::getImage($this->id, 'watermark');

        if (empty($images))
            return null;

        $firstImage = null;

        foreach ($images as $image){
            $firstImage = $image;
            break;
        }

        return $firstImage;
    }

    public function getUrlImage($priceId = 4, $offWatermark = false)
    {
        if ($offWatermark)
            $images = ImageUpload::getImage($this->id .'/'.$priceId, 'off_watermark');
        else
            $images = ImageUpload::getImage($this->id .'/'.$priceId, 'watermark');
        if (empty($images))
            return null;
        foreach ($images as $image) {
            return ImageUpload::getUrl($image);
        }
    }

    public function getUrlThumbImage($offWatermark = true)
    {
        if ($offWatermark)
            $images = ImageUpload::getThumbImage($this->id, 'off_watermark');
        else
            $images = ImageUpload::getThumbImage($this->id, 'watermark');
        if (empty($images))
            return null;
        foreach ($images as $image) {
            return ImageUpload::getUrl($image);
        }
    }

    public function getUrlDetailImage($offWatermark = false)
    {
        if ($offWatermark)
            $images = ImageUpload::getDetailImage($this->id, 'off_watermark');
        else
            $images = ImageUpload::getDetailImage($this->id, 'watermark');
        if (empty($images))
            return null;
        foreach ($images as $image) {
            return ImageUpload::getUrl($image);
        }
    }
}
