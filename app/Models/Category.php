<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $table = 'categories';

    protected $fillable = ['name', 'description', 'main'];

    protected $dates = ['deleted_at'];

    public function products()
    {
        return $this->belongsToMany('App\Models\Product','products_to_categories', 'category_id', 'product_id');
    }

    public function keywords()
    {
        return $this->belongsToMany(Keyword::class,'categories_to_keywords', 'category_id', 'keyword_id');
    }

    /**
     * @param int $lang_int
     * @return Model|null|static
     */
    public function translationName($lang_int = Translation::LANG_EN)
    {
        return $this->hasOne(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_CATEGORY_NAME)->where('language', $lang_int)
            ->first();
    }

    /**
     * @param int $lang_int
     * @return Model|null|static
     */
    public function  translationDescription($lang_int = Translation::LANG_EN)
    {
        return $this->hasOne(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_CATEGORY_DESCRIPTION)->where('language', $lang_int)
            ->first();
    }

    public function translations()
    {
        return $this->hasMany(Translation::class, 'item_id', 'id')
            ->where('type', Translation::TYPE_CATEGORY_DESCRIPTION)
            ->orWhere('type', Translation::TYPE_CATEGORY_NAME);
    }

    public function getImageUrl()
    {
        $images = ImageUpload::getImage($this->id, 'category');
        if (empty($images))
            return '';
        foreach ($images as $image) {
            $imageUrl = ImageUpload::getUrl($image);
            break;
        }
        return $imageUrl;
    }
}
