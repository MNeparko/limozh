<?php

namespace App\Models;

use Config;
use Mail;

class SendMail
{
    const MAIL_FROM = 'fotobazar.smtp@gmail.com';
    const MAIL_TO = 'info@fotobazar.org';

    public static function html_email($text = null, $email = null, $name = null, $subject = null)
    {
        $data = array(
            'name' => $name,
            'subject' => $subject,
            'email' => $email,
            'text' => $text
        );

        try {
            Mail::send('site.mail', $data, function ($message) {
                $message->to(self::MAIL_TO, 'Manager')//кому
                ->subject('New message!');
                $message->from(self::MAIL_FROM, 'Fotobazar site');// От кого
            });
        } catch (\Exception $e) {
            dd($e);
            return false;
        }
        return true;
    }

}
