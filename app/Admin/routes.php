<?php

Route::get('', ['as' => 'admin.dashboard', function () {
	$content = 'Define your dashboard here.';
	return AdminSection::view($content, 'Dashboard');
}]);

Route::get('information', ['as' => 'admin.information', function () {
	$content = 'Define your information here.';
	return AdminSection::view($content, 'Information');
}]);

Route::group(['prefix' => 'shops'], function() {

    Route::get('/', [
        'as' => 'admin.shop.index',
        'uses' => '\App\Http\Controllers\Admin\ShopController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.shop.create',
        'uses'=>'\App\Http\Controllers\Admin\ShopController@create',
    ]);

    Route::post('/create',[
        'as'=>'admin.shop.store',
        'uses'=>'\App\Http\Controllers\Admin\ShopController@store',
    ]);

    Route::get('/{id}/edit',[
        'as'=>'admin.shop.edit',
        'uses'=>'\App\Http\Controllers\Admin\ShopController@edit',
    ]);

    Route::patch('/{id}',[
        'as'=>'admin.shop.update',
        'uses'=>'\App\Http\Controllers\Admin\ShopController@update',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.shop.delete',
        'uses'=>'\App\Http\Controllers\Admin\ShopController@delete',
    ]);

});

Route::group(['prefix' => 'places'], function() {

    Route::get('/', [
        'as' => 'admin.place.index',
        'uses' => '\App\Http\Controllers\Admin\PlaceController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.place.create',
        'uses'=>'\App\Http\Controllers\Admin\PlaceController@create',
    ]);

    Route::post('/create',[
        'as'=>'admin.place.store',
        'uses'=>'\App\Http\Controllers\Admin\PlaceController@store',
    ]);

    Route::get('/{id}/edit',[
        'as'=>'admin.place.edit',
        'uses'=>'\App\Http\Controllers\Admin\PlaceController@edit',
    ]);

    Route::patch('/{id}',[
        'as'=>'admin.place.update',
        'uses'=>'\App\Http\Controllers\Admin\PlaceController@update',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.place.delete',
        'uses'=>'\App\Http\Controllers\Admin\PlaceController@delete',
    ]);

});

Route::group(['prefix' => 'categories'], function() {

    Route::get('/', [
        'as' => 'admin.categories.index',
        'uses' => '\App\Http\Controllers\Admin\CategoryController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.category.create',
        'uses'=>'\App\Http\Controllers\Admin\CategoryController@create',
    ]);

    Route::post('/create',[
        'as'=>'admin.category.store',
        'uses'=>'\App\Http\Controllers\Admin\CategoryController@store',
    ]);

    Route::get('/{id}/edit',[
        'as'=>'admin.category.edit',
        'uses'=>'\App\Http\Controllers\Admin\CategoryController@edit',
    ]);

    Route::patch('/{id}',[
        'as'=>'admin.category.update',
        'uses'=>'\App\Http\Controllers\Admin\CategoryController@update',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.category.delete',
        'uses'=>'\App\Http\Controllers\Admin\CategoryController@delete',
    ]);

});

Route::group(['prefix' => 'products'], function() {

    Route::get('/', [
        'as' => 'admin.product.index',
        'uses' => '\App\Http\Controllers\Admin\ProductController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.product.create',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@create',
    ]);

    Route::post('/store',[
        'as'=>'admin.product.store',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@store',
    ]);

    Route::post('/store-images',[
        'as'=>'admin.product.store.images',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@storeImages',
    ]);

    Route::get('/close-images',[
        'as'=>'admin.product.close.images',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@closeImages',
    ]);

    Route::get('/edit/{id}',[
        'as'=>'admin.product.edit',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@edit',
    ]);

    Route::post('/update/{id}',[
        'as'=>'admin.product.update',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@update',
    ]);

    Route::get('/delete/{id}',[
        'as'=>'admin.product.delete',
        'uses'=>'\App\Http\Controllers\Admin\ProductController@delete',
    ]);

});

Route::group(['prefix' => 'keywords'], function() {

    Route::get('/', [
        'as' => 'admin.keywords.index',
        'uses' => '\App\Http\Controllers\Admin\KeywordController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.keyword.create',
        'uses'=>'\App\Http\Controllers\Admin\KeywordController@create',
    ]);

    Route::post('/create',[
        'as'=>'admin.keyword.store',
        'uses'=>'\App\Http\Controllers\Admin\KeywordController@store',
    ]);

    Route::get('/{id}/edit',[
        'as'=>'admin.keyword.edit',
        'uses'=>'\App\Http\Controllers\Admin\KeywordController@edit',
    ]);

    Route::patch('/{id}',[
        'as'=>'admin.keyword.update',
        'uses'=>'\App\Http\Controllers\Admin\KeywordController@update',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.keyword.delete',
        'uses'=>'\App\Http\Controllers\Admin\KeywordController@delete',
    ]);

});

Route::group(['prefix' => 'orders'], function() {

    Route::get('/', [
        'as' => 'admin.orders.index',
        'uses' => '\App\Http\Controllers\Admin\OrderController@index',
    ]);

    Route::get('/{id}/view',[
        'as'=>'admin.order.view',
        'uses'=>'\App\Http\Controllers\Admin\OrderController@view',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.order.delete',
        'uses'=>'\App\Http\Controllers\Admin\OrderController@delete',
    ]);

});

Route::get('logout', [
    'as' => 'admin.logout',
    'uses' => '\App\Http\Controllers\Auth\LoginController@logout'
]);

Route::group(['prefix' => 'seo'], function() {

    Route::get('/', [
        'as' => 'admin.seo.index',
        'uses' => '\App\Http\Controllers\Admin\SeoController@index',
    ]);

    Route::get('/create',[
        'as'=>'admin.seo.create',
        'uses'=>'\App\Http\Controllers\Admin\SeoController@create',
    ]);

    Route::post('/create',[
        'as'=>'admin.seo.store',
        'uses'=>'\App\Http\Controllers\Admin\SeoController@store',
    ]);

    Route::get('/{id}/edit',[
        'as'=>'admin.seo.edit',
        'uses'=>'\App\Http\Controllers\Admin\SeoController@edit',
    ]);

    Route::patch('/{id}',[
        'as'=>'admin.seo.update',
        'uses'=>'\App\Http\Controllers\Admin\SeoController@update',
    ]);

    Route::delete('/{id}',[
        'as'=>'admin.seo.delete',
        'uses'=>'\App\Http\Controllers\Admin\SeoController@delete',
    ]);

});

Route::group(['prefix' => 'static-page'], function() {

    Route::get('/feedback/edit',[
        'as'=>'admin.static.feedback.edit',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@editFeedback',
    ]);

    Route::patch('/feedback/update',[
        'as'=>'admin.static.feedback.update',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@updateFeedback',
    ]);

    Route::get('/terms/edit',[
        'as'=>'admin.static.terms.edit',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@editTerms',
    ]);

    Route::patch('/terms/update',[
        'as'=>'admin.static.terms.update',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@updateTerms',
    ]);

    Route::get('/privacy/edit',[
        'as'=>'admin.static.privacy.edit',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@editPrivacy',
    ]);

    Route::patch('/privacy/update',[
        'as'=>'admin.static.privacy.update',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@updatePrivacy',
    ]);

    Route::get('/license/edit',[
        'as'=>'admin.static.license.edit',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@editLicense',
    ]);

    Route::patch('/license/update',[
        'as'=>'admin.static.license.update',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@updateLicense',
    ]);

    Route::get('/about/edit',[
        'as'=>'admin.static.about.edit',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@editAbout',
    ]);

    Route::patch('/about/update',[
        'as'=>'admin.static.about.update',
        'uses'=>'\App\Http\Controllers\Admin\StaticPagesController@updateAbout',
    ]);

});