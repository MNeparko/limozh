<?php

use SleepingOwl\Admin\Navigation\Page;

return [

    [
        'title' => 'Сайт',
        'icon'  => 'fa fa-dashboard',
        'url'   => url('/'),
        'priority' => 1
    ],
    [
        'title' => 'Заведения',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.shop.index'),
        'priority' => 2,
    ],
    [
        'title' => 'Места/Столики зеведений',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.place.index'),
        'priority' => 3,
    ],
    [
        'title' => 'Категории',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.categories.index'),
        'priority' => 4,
    ],
    [
        'title' => 'Товары',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.product.index'),
        'priority' => 5,
    ],
    [
        'title' => 'Ключевые слова',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.keywords.index'),
        'priority' => 6,
    ],
    [
        'title' => 'Заказы',
        'icon'  => 'fa fa-list-alt',
        'url'   => route('admin.orders.index'),
        'priority' => 7,
    ],
    [
        'title' => 'Seo',
        'icon'  => 'fa fa-exclamation-circle',
        'priority' => 8,
        'url'   => route('admin.seo.index'),
    ],
    [
        'title' => 'Редактирование Страниц',
        'icon'  => 'fa fa-exclamation-circle',
        'priority' => 9,
        'pages' => [
            [
                'title' => 'Feedback',
                'priority' => '1',
                'url' => route('admin.static.feedback.edit'),
            ],
            [
                'title' => 'Terms & condition',
                'priority' => '2',
                'url' => route('admin.static.terms.edit'),
            ],
            [
                'title' => 'Privacy',
                'priority' => '3',
                'url' => route('admin.static.privacy.edit'),
            ],
            [
                'title' => 'The GGTUP includes',
                'priority' => '4',
                'url' => route('admin.static.license.edit'),
            ],
            [
                'title' => 'About',
                'priority' => '5',
                'url' => route('admin.static.about.edit'),
            ],
        ]
    ],
    [
        'title' => 'Выход',
        'icon'  => 'fa fa-sign-out',
        'url' => route('admin.logout'),
    ],
];