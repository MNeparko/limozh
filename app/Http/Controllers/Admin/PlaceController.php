<?php

namespace App\Http\Controllers\Admin;

use App\Models\Place;
use App\Models\Shop;
use App\Models\ImageUpload;
use App\Models\Translation;
use Illuminate\Http\Request;

class PlaceController extends OwlController
{
    public $rule = [
        'pictures.*' => 'image|mimes:jpeg,jpg,png,gif'
    ];

    public function index()
    {
        $places = Place::orderBy('id', 'DESC')->paginate(8);

        return $this->renderContent(view('admin.place.index', compact('places')), 'Места/Столики');
    }

    public function create()
    {
        $shops = Shop::pluck('name', 'id');

        return $this->renderContent(view('admin.place.create', compact('shops')), 'Создание');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        $input['description'] = !empty($request->get('descriptionEn')) ? $request->get('descriptionEn') : '';
        $input['shop_id'] = $input['shop_id'][0];

        $place = Place::create($input);

        if (!empty($request->get('descriptionEn')))
            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $request->get('descriptionEn')
            ]);

        if (!empty($request->get('descriptionRu')))
            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $request->get('descriptionRu')
            ]);

        if (!empty($request->get('descriptionDe')))
            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $request->get('descriptionDe')
            ]);

        if ($request->hasFile('pictures')) {
            ImageUpload::saveSeveralImages($request->file('pictures'), $place->id, 'place');
        }

        return redirect()->route('admin.place.create')
            ->with('success', 'Успешно создано');
    }

    public function edit($id)
    {
        $place = Place::find($id);
        $images = ImageUpload::getImage($id, 'shop');
        $shops = Shop::pluck('name', 'id');

        return $this->renderContent(view('admin.place.edit', compact('place', 'images', 'shops')), 'Редактирование');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        ini_set('max_execution_time', 2000);
        ini_set('max_input_time', 2000);

        $input['description'] = $request->get('descriptionEn');
        $input['shop_id'] = $input['shop_id'][0];

        if (!$request->has('in_vip'))
            $input['in_vip'] = 0;

        $place = Place::find($id);
        $place->update($input);

        if(Translation::where([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['descriptionEn']]);

            Translation::where([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['descriptionRu']]);

            Translation::where([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['descriptionDe']]);
        } else {
            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $input['descriptionEn']]);

            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $input['descriptionRu']]);

            Translation::create([
                'item_id' => $place->id,
                'type' => Translation::TYPE_PLACE_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $input['descriptionDe']]);
        }

        if ($request->hasFile('pictures')) {
            ImageUpload::saveSeveralImages($request->file('pictures'), $place->id, 'place');
        }

        return redirect()->route('admin.place.index')
            ->with('success', 'Успешно обновлено');
    }

    public function delete($id)
    {
        Place::find($id)->delete();

        ImageUpload::deleteImage($id, 'place');

        return redirect()->route('admin.place.index')->with('success', 'Успешно удалено');
    }
}