<?php

namespace App\Http\Controllers\Admin;


use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Keyword;
use App\Models\Translation;
use Illuminate\Http\Request;

class CategoryController extends OwlController
{

    public $rule = [
        'nameEn' => 'required|max:255',
        'nameRu' => 'required|max:255',
        'nameDe' => 'required|max:255',
        'image' => 'image|mimes:jpeg,jpg,png,gif',
    ];

    public function index()
    {
        $categories = Category::orderBy('id', 'DESC')->paginate(8);

        return $this->renderContent(view('admin.category.index',
            compact('categories')), 'Категории');
    }

    public function create()
    {
        $keywords = Keyword::pluck('value', 'id');

        return $this->renderContent(view('admin.category.create', compact('keywords')), 'Создание категории');
    }

    public function store(Request $request)
    {

        $this->validate($request, $this->rule);
        $input = $request->all();
        $input['name'] = $request->get('nameEn');

        if (!empty($request->get('descriptionEn')))
            $input['description'] = $request->get('descriptionEn');
        $category = Category::create($input);

        if (!empty($request->get('nameEn')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_EN,
                'value' => $request->get('nameEn')
            ]);

        if (!empty($request->get('nameRu')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_RU,
                'value' => $request->get('nameRu')
            ]);

        if (!empty($request->get('nameDe')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_DE,
                'value' => $request->get('nameDe')
            ]);

        if (!empty($request->get('descriptionEn')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $request->get('descriptionEn')
            ]);

        if (!empty($request->get('descriptionRu')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $request->get('descriptionRu')
            ]);

        if (!empty($request->get('descriptionDe')))
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $request->get('descriptionDe')
            ]);

        if ($request->has('keywords')) {
            foreach ($request->get('keywords') as $keywordId)
                Keyword::find($keywordId)->categories()->save($category);
        }

        if ($request->hasFile('image')) {
            ImageUpload::saveImage($request->file('image'), $category->id, 'category');
        }

        return redirect()->route('admin.category.create')
            ->with('success', 'Категория успешно создана');
    }

    public function edit($id)
    {
        $category = Category::find($id);
        $images = ImageUpload::getImage($id, 'category');
        $keywords = Keyword::pluck('value', 'id');

        return $this->renderContent(view('admin.category.edit',
            compact('category', 'images', 'keywords')), 'Редактирование категории');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        $input['name'] = $request->get('nameEn');
        $input['description'] = $request->get('descriptionEn');

        if (!$request->has('main'))
            $input['main'] = 0;

        $category = Category::find($id);
        $category->update($input);
        $category->keywords()->detach();

        if(Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['nameEn']]);

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['nameRu']]);

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['nameDe']]);
        } else {
            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_EN,
                'value' => $input['nameEn']]);

            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_RU,
                'value' => $input['nameRu']]);

            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_NAME,
                'language' => Translation::LANG_DE,
                'value' => $input['nameDe']]);

        }

        if(Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['descriptionEn']]);

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['descriptionRu']]);

            Translation::where([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['descriptionDe']]);

        } else {

            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $input['descriptionEn']]);

            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $input['descriptionRu']]);

            Translation::create([
                'item_id' => $category->id,
                'type' => Translation::TYPE_CATEGORY_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $input['descriptionDe']]);
        }

        if ($request->has('keywords')) {

            foreach ($request->get('keywords') as $keywordId)
                Keyword::find($keywordId)->categories()->save($category);
        }

        if ($request->hasFile('image')) {
            ImageUpload::saveImage($request->file('image'), $category->id, 'category');
        }

        return redirect()->route('admin.categories.index')
            ->with('success', 'Категория успешно обновлена');
    }

    public function delete($id)
    {
        Category::find($id)->keywords()->detach();
        Category::find($id)->products()->detach();
        Category::find($id)->translations()->delete();
        Category::find($id)->delete();

        ImageUpload::deleteImage($id, 'category');

        return redirect()->route('admin.categories.index')->with('success', 'Категория успешно удалена');
    }
}