<?php

namespace App\Http\Controllers\Admin;

use App\Models\Seo;
use Validator;
use Illuminate\Http\Request;

class SeoController extends OwlController
{

    public $rule = [
        'url_path' => 'required|unique:seo|max:255',
        'meta_description' => 'max:255',
        'meta_keywords' => 'max:255',
        'title' => 'max:255',
        'h1' => 'max:255',
        'h2' => 'max:255',
        'h3' => 'max:255',
        'seo_text' => ''
    ];


    public function index()
    {
        $seo_items = Seo::orderBy('id','DESC')->paginate(5);

        return $this->renderContent(view('admin.seo.index',
            compact('seo_items')), 'Seo');
    }

    public function create()
    {
        return $this->renderContent(view('admin.seo.create'), 'Создание seo страницы');
    }


    public function edit($id)
    {
        $seo_item = Seo::find($id);

        return $this->renderContent(view('admin.seo.edit',
            compact('seo_item')), 'Редактирование seo страницы');
    }

    public function update(Request $request, $id)
    {
        $rule_update = $this->rule;
        $rule_update['url_path'] = 'required|max:255';

        $v = Validator::make($request->all(),$rule_update);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors())
                ->withInput();
        }

        $seo_item = Seo::find($id);
        $seo_item->update($request->all());

        return redirect()->route('admin.seo.index')
            ->with('success','Seo страницы успешно обновлен');
    }

    public function store(Request $request)
    {

        $v = Validator::make($request->all(),$this->rule);

        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors())
                ->withInput();
        }

        Seo::create($request->all());

        return redirect()->route('admin.seo.create')
            ->with('success','Seo страницы успешно создана');
    }

    public function delete($id)
    {
        Seo::find($id)->delete();
        return redirect()->route('admin.seo.index')
            ->with('success','Seo страницы успешно удален');
    }
}