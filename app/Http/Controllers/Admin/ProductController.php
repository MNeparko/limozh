<?php

namespace App\Http\Controllers\Admin;


use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Keyword;
use App\Models\Product;
use App\Models\Shop;
use App\Models\Translation;
use Illuminate\Http\Request;
use Session;

class ProductController extends OwlController
{

    public $rule = [
        'titleEn' => 'required|max:255',
        'titleRu' => 'required|max:255',
        'titleDe' => 'required|max:255',
        'categories' => 'required'
    ];


    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->paginate(8);

        return $this->renderContent(view('admin.product.index', compact('products')), 'Товары');
    }

    public function create()
    {
        $categories = Category::all()->pluck('name','id');
        $keywords = Keyword::pluck('value', 'id');
        $shops = Shop::pluck('name', 'id');

        return $this->renderContent(view('admin.product.create', compact('categories', 'keywords', 'shops')), 'Создание');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        if ($request->hasFile('image')) {
            $input['extension'] = $request->file('image')->getClientOriginalExtension();
        }

        $input['shop_id'] = $input['shop_id'][0];

        $newProduct = Product::create($input);

        foreach ($input['categories'] as $categoryId)
            Category::find($categoryId)->products()->save($newProduct);

        if ($request->hasFile('pictures')) {
            ImageUpload::saveSeveralImages($request->file('pictures'), $newProduct->id, 'product');
        }

        return redirect()->route('admin.product.create')->with('success', 'Товар успешно загружен');
    }

    public function storeImages(Request $request)
    {
        $this->validate($request, ['pictures.*' => 'image|mimes:jpeg,jpg,png']);
        $input = $request->all();
        ini_set('max_execution_time', 2000);
        ini_set('max_input_time', 2000);

        if ($request->hasFile('pictures')) {
            $productsId = [];
            foreach ($request->file('pictures') as $file) {
                $input['extension'] = $file->getClientOriginalExtension();
                $newProduct = Product::create($input);

                ImageUpload::saveOriginalImages($file,$newProduct->id, $input['orientation'],
                    $request->get('price_id'));

                ImageUpload::saveWatermarkImages($file, $newProduct->id, $input['orientation']);
                $productsId[] = $newProduct->id;
            }
        }

        return redirect()->route('admin.product.create')
            ->with('success', 'Успешно загружено');
    }

    public function edit($id)
    {
        $product = Product::find($id);
        $categories = Category::all()->pluck('name','id');
        $images = ImageUpload::getThumbImage($id, 'off_watermark');
        $keywords = Keyword::pluck('value', 'id');
        $shops = Shop::pluck('name', 'id');

        return $this->renderContent(view('admin.product.edit', compact('product', 'categories', 'keywords', 'selectCategories', 'images', 'shops')), 'Редактирование');
    }

    public function update(Request $request, $id)
    {
        $currentRules = $this->rule;
        unset($currentRules['price_id']);

        $this->validate($request, $currentRules);
        $input = $request->all();
        $input['title'] = $input['titleEn'];
        $input['shop_id'] = $input['shop_id'][0];

        if ($request->hasFile('image')) {
            $input['extension'] = $request->file('image')->getClientOriginalExtension();
        }

        $product = Product::find($id);
        $product->update($input);
        $product->categories()->detach();

        if(Translation::where([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['titleEn']]);

            Translation::where([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['titleRu']]);

            Translation::where([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['titleDe']]);

        } else {
            Translation::create([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_EN,
                'value' => $input['titleEn']]);

            Translation::create([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_RU,
                'value' => $input['titleRu']]);

            Translation::create([
                'item_id' => $product->id,
                'type' => Translation::TYPE_PRODUCT_TITLE,
                'language' => Translation::LANG_DE,
                'value' => $input['titleDe']]);
        }

        foreach ($input['categories'] as $categoryId)
            Category::find($categoryId)->products()->save($product);

        if ($request->hasFile('pictures')) {
            ImageUpload::saveSeveralImages($request->file('pictures'), $product->id, 'product');
        }

        return redirect()->route('admin.product.index')
            ->with('success', 'Успешно обновлено');
    }

    public function delete($id)
    {
        Product::find($id)->categories()->detach();
        Product::find($id)->translations()->delete();
        Product::find($id)->delete();

        ImageUpload::deleteImage($id, 'product');

        return redirect()->route('admin.product.index')
            ->with('success', 'Успешно удалено');
    }
}