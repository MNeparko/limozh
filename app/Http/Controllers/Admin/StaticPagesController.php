<?php
/**
 * Created by PhpStorm.
 * User: eidos
 * Date: 09.11.17
 * Time: 15:50
 */

namespace App\Http\Controllers\Admin;

use App\Models\Translation;
use Illuminate\Http\Request;

class StaticPagesController extends OwlController
{

    public $rule = [
        'textEn' => 'required',
        'textRu' => 'required',
        'textDe' => 'required'
    ];

    public $ruleAbout = [
        'textFirstEn' => 'required',
        'textFirstRu' => 'required',
        'textFirstDe' => 'required',
        'textSecondEn' => 'required',
        'textSecondRu' => 'required',
        'textSecondDe' => 'required'

    ];

    public function editFeedback()
    {
        if (Translation::where('type', Translation::TYPE_PAGE_FEEDBACK)->count() == 0) {
            Translation::create(['type' => Translation::TYPE_PAGE_FEEDBACK,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_FEEDBACK,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_FEEDBACK,
                'language' => Translation::LANG_DE, 'value' => '']);
        }

        $feedback = Translation::where('type', Translation::TYPE_PAGE_FEEDBACK)
            ->OrderBy('language', 'ASC')->get();

        return $this->renderContent(view('admin.static_pages.feedback.edit',
            compact('feedback')), 'Feedback');
    }

    public function updateFeedback(Request $request)
    {
        $this->validate($request, $this->rule);

        Translation::where([
            'type' => Translation::TYPE_PAGE_FEEDBACK,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_FEEDBACK,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_FEEDBACK,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textDe')]);

        return redirect()->route('admin.static.feedback.edit')
            ->with('success','Feedback успешно обновлен');
    }

    public function editTerms()
    {
        if (Translation::where('type', Translation::TYPE_PAGE_TERMS)->count() == 0) {
            Translation::create(['type' => Translation::TYPE_PAGE_TERMS,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_TERMS,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_TERMS,
                'language' => Translation::LANG_DE, 'value' => '']);
        }

        $terms = Translation::where('type', Translation::TYPE_PAGE_TERMS)
            ->OrderBy('language', 'ASC')->get();

        return $this->renderContent(view('admin.static_pages.terms.edit',
            compact('terms')), 'Terms & conditions');
    }

    public function updateTerms(Request $request)
    {
        $this->validate($request, $this->rule);

        Translation::where([
            'type' => Translation::TYPE_PAGE_TERMS,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_TERMS,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_TERMS,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textDe')]);

        return redirect()->route('admin.static.terms.edit')
            ->with('success','Terms & conditions успешно обновлен');
    }

    public function editPrivacy()
    {
        if (Translation::where('type', Translation::TYPE_PAGE_PRIVACY)->count() == 0) {
            Translation::create(['type' => Translation::TYPE_PAGE_PRIVACY,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_PRIVACY,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_PRIVACY,
                'language' => Translation::LANG_DE, 'value' => '']);
        }

        $privacy = Translation::where('type', Translation::TYPE_PAGE_PRIVACY)
            ->OrderBy('language', 'ASC')->get();

        return $this->renderContent(view('admin.static_pages.privacy.edit',
            compact('privacy')), 'Privacy');
    }

    public function updatePrivacy(Request $request)
    {
        $this->validate($request, $this->rule);

        Translation::where([
            'type' => Translation::TYPE_PAGE_PRIVACY,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_PRIVACY,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_PRIVACY,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textDe')]);

        return redirect()->route('admin.static.privacy.edit')
            ->with('success','Privacy успешно обновлен');
    }

    public function editLicense()
    {
        if (Translation::where('type', Translation::TYPE_PAGE_LICENSE)->count() == 0) {
            Translation::create(['type' => Translation::TYPE_PAGE_LICENSE,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_LICENSE,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_LICENSE,
                'language' => Translation::LANG_DE, 'value' => '']);
        }

        $license = Translation::where('type', Translation::TYPE_PAGE_LICENSE)
            ->OrderBy('language', 'ASC')->get();

        return $this->renderContent(view('admin.static_pages.license.edit',
            compact('license')), 'License agreement');
    }

    public function updateLicense(Request $request)
    {
        $this->validate($request, $this->rule);

        Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textDe')]);

        return redirect()->route('admin.static.license.edit')
            ->with('success','License agreement успешно обновлен');
    }

    public function editAbout()
    {
        if (Translation::where('type', Translation::TYPE_PAGE_ABOUT_FIRST)->count() == 0) {
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_FIRST,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_FIRST,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_FIRST,
                'language' => Translation::LANG_DE, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_SECOND,
                'language' => Translation::LANG_EN, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_SECOND,
                'language' => Translation::LANG_RU, 'value' => '']);
            Translation::create(['type' => Translation::TYPE_PAGE_ABOUT_SECOND,
                'language' => Translation::LANG_DE, 'value' => '']);

        }

        $aboutFirst = Translation::where('type', Translation::TYPE_PAGE_ABOUT_FIRST)
            ->OrderBy('language', 'ASC')->get();
        $aboutSecond = Translation::where('type', Translation::TYPE_PAGE_ABOUT_SECOND)
                    ->OrderBy('language', 'ASC')->get();

        return $this->renderContent(view('admin.static_pages.about.edit',
            compact('aboutFirst', 'aboutSecond')), 'About');
    }

    public function updateAbout(Request $request)
    {
        $this->validate($request, $this->ruleAbout);

        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_FIRST,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textFirstEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_FIRST,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textFirstRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_FIRST,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textFirstDe')]);


        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_SECOND,
            'language' => Translation::LANG_EN
        ])->update(['value' => $request->get('textSecondEn')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_SECOND,
            'language' => Translation::LANG_RU
        ])->update(['value' => $request->get('textSecondRu')]);

        Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_SECOND,
            'language' => Translation::LANG_DE
        ])->update(['value' => $request->get('textSecondDe')]);

        return redirect()->route('admin.static.about.edit')
            ->with('success','About успешно обновлен');
    }
}