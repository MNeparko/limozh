<?php

namespace App\Http\Controllers\Admin;

use App\Models\Shop;
use App\Models\ImageUpload;
use App\Models\Keyword;
use App\Models\Translation;
use Illuminate\Http\Request;

class ShopController extends OwlController
{
    public $rule = [
        'nameEn' => 'required|max:255',
        'nameRu' => 'required|max:255',
        'nameDe' => 'required|max:255',
        'pictures.*' => 'image|mimes:jpeg,jpg,png,gif',
    ];

    public function index()
    {
        $shops = Shop::orderBy('id', 'DESC')->paginate(8);

        return $this->renderContent(view('admin.shop.index', compact('shops')), 'Категории');
    }

    public function create()
    {
        $keywords = Keyword::pluck('value', 'id');

        return $this->renderContent(view('admin.shop.create', compact('keywords')), 'Создание');
    }

    public function store(Request $request)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        $input['name'] = $request->get('nameEn');

        $input['description'] = !empty($request->get('descriptionEn')) ? $request->get('descriptionEn') : '';

        $shop = Shop::create($input);

        if (!empty($request->get('nameEn')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_EN,
                'value' => $request->get('nameEn')
            ]);

        if (!empty($request->get('nameRu')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_RU,
                'value' => $request->get('nameRu')
            ]);

        if (!empty($request->get('nameDe')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_DE,
                'value' => $request->get('nameDe')
            ]);

        if (!empty($request->get('descriptionEn')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $request->get('descriptionEn')
            ]);

        if (!empty($request->get('descriptionRu')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $request->get('descriptionRu')
            ]);

        if (!empty($request->get('descriptionDe')))
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $request->get('descriptionDe')
            ]);

        if ($request->has('keywords')) {
            foreach ($request->get('keywords') as $keywordId) {
                Keyword::find($keywordId)->shops()->save($shop);
            }
        }

        if ($request->hasFile('pictures')) {
            ImageUpload::saveSeveralImages($request->file('pictures'), $shop->id, 'shop');

        }

        return redirect()->route('admin.shop.create')
            ->with('success', 'Успешно создано');
    }

    public function edit($id)
    {
        $shop = Shop::find($id);
        $images = ImageUpload::getImage($id, 'shop');
        $keywords = Keyword::pluck('value', 'id');

        return $this->renderContent(view('admin.shop.edit', compact('shop', 'images', 'keywords')), 'Редактирование');
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, $this->rule);
        $input = $request->all();

        ini_set('max_execution_time', 2000);
        ini_set('max_input_time', 2000);

        $input['name'] = $request->get('nameEn');
        $input['description'] = $request->get('descriptionEn');

        if (!$request->has('main'))
            $input['main'] = 0;

        $shop = Shop::find($id);
        $shop->update($input);
        $shop->keywords()->detach();

        if(Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['nameEn']]);

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['nameRu']]);

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['nameDe']]);
        } else {
            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_EN,
                'value' => $input['nameEn']]);

            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_RU,
                'value' => $input['nameRu']]);

            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_NAME,
                'language' => Translation::LANG_DE,
                'value' => $input['nameDe']]);

        }

        if(Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['descriptionEn']]);

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['descriptionRu']]);

            Translation::where([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['descriptionDe']]);

        } else {

            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_EN,
                'value' => $input['descriptionEn']]);

            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_RU,
                'value' => $input['descriptionRu']]);

            Translation::create([
                'item_id' => $shop->id,
                'type' => Translation::TYPE_SHOP_DESCRIPTION,
                'language' => Translation::LANG_DE,
                'value' => $input['descriptionDe']]);
        }

        if ($request->has('keywords')) {

            foreach ($request->get('keywords') as $keywordId)
                Keyword::find($keywordId)->shop()->save($shop);
        }

        if ($request->hasFile('pictures')) {
            /*foreach ($request->file('pictures') as $file) {
                ImageUpload::saveImage($file, $shop->id, 'shop');
            }*/
            ImageUpload::saveSeveralImages($request->file('pictures'), $shop->id, 'shop');

        }

        return redirect()->route('admin.shop.index')
            ->with('success', 'Успешно обновлено');
    }

    public function delete($id)
    {
        Shop::find($id)->keywords()->detach();
        Shop::find($id)->products()->detach();
        Shop::find($id)->translations()->delete();
        Shop::find($id)->delete();

        ImageUpload::deleteImage($id, 'shop');

        return redirect()->route('admin.shop.index')->with('success', 'Успешно удалено');
    }
}