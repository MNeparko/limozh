<?php
/**
 * Created by PhpStorm.
 * User: eidos
 * Date: 10.11.17
 * Time: 16:59
 */

namespace App\Http\Controllers\Admin;

use SleepingOwl\Admin\Http\Controllers\AdminController;
use Illuminate\Foundation\Validation\ValidatesRequests;

class OwlController extends AdminController
{
    use ValidatesRequests;
}