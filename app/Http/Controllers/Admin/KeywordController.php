<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Keyword;
use App\Models\Translation;
use Illuminate\Http\Request;

class KeywordController extends OwlController
{

    public $rule = [
        'valueEn' => 'required|max:255|unique:keywords,value',
        'valueRu' => 'required|max:255',
        'valueDe' => 'required|max:255'
    ];

    public function index()
    {
        $keywords = Keyword::orderBy('id', 'DESC')->paginate(8);

        return $this->renderContent(view('admin.keyword.index',
            compact('keywords')), 'Ключевые слова для фото и категорий');
    }

    public function create()
    {
        return $this->renderContent(view('admin.keyword.create'), 'Создание ключевого слова');
    }


    public function store(Request $request)
    {

        $this->validate($request, $this->rule);
        $input = $request->all();
        $input['value'] = $input['valueEn'];
        $keyword = Keyword::create($input);
        Translation::create([
            'item_id' => $keyword->id,
            'type' => Translation::TYPE_KEYWORD_VALUE,
            'language' => Translation::LANG_EN,
            'value' => $input['valueEn']]);
        Translation::create([
            'item_id' => $keyword->id,
            'type' => Translation::TYPE_KEYWORD_VALUE,
            'language' => Translation::LANG_RU,
            'value' => $input['valueRu']]);
        Translation::create([
            'item_id' => $keyword->id,
            'type' => Translation::TYPE_KEYWORD_VALUE,
            'language' => Translation::LANG_DE,
            'value' => $input['valueDe']]);

        return redirect()->route('admin.keyword.create')
            ->with('success', 'Ключевое слово успешно создано');
    }

    public function edit($id)
    {
        $keyword = Keyword::find($id);

        return $this->renderContent(view('admin.keyword.edit',
            compact('keyword')), 'Редактирование ключевого слова');
    }

    public function update(Request $request, $id)
    {
        $this->rule['valueEn'] .= ','.$id;
        $this->validate($request, $this->rule);
        $input = $request->all();
        $input['value'] = $input['valueEn'];
        $keyword = Keyword::find($id);
        $keyword->update($input);

        if(Translation::where([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_EN
            ])->count() > 0) {

            Translation::where([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_EN
            ])->update(['value' => $input['valueEn']]);

            Translation::where([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_RU
            ])->update(['value' => $input['valueRu']]);

            Translation::where([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_DE
            ])->update(['value' => $input['valueDe']]);

        } else {
            Translation::create([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_EN,
                'value' => $input['valueEn']]);

            Translation::create([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_RU,
                'value' => $input['valueRu']]);

            Translation::create([
                'item_id' => $keyword->id,
                'type' => Translation::TYPE_KEYWORD_VALUE,
                'language' => Translation::LANG_DE,
                'value' => $input['valueDe']]);
        }

        return redirect()->route('admin.keywords.index')
            ->with('success', 'Ключевое слово успешно обновлено');
    }

    public function delete($id)
    {
        Keyword::find($id)->translations()->delete();
        Keyword::find($id)->delete();

        return redirect()->route('admin.keywords.index')
            ->with('success', 'Ключевое слово успешно удалено');
    }
}