<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;

class OrderController extends OwlController
{

    public function index()
    {
        $orders = Order::orderBy('id', 'DESC')->paginate(10);

        return $this->renderContent(view('admin.order.index', compact('orders')), 'Заказы');
    }

    public function view($id)
    {
        $order = Order::find($id);

        return $this->renderContent(view('admin.order.view', compact('order')), 'Просмотр заказа');
    }

    public function delete($id)
    {
        Order::find($id)->items()->delete();
        Order::find($id)->delete();

        return redirect()->route('admin.orders.index')->with('success', 'Заказ успешно удален');
    }
}