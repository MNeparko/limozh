<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Product;
use Illuminate\Http\Request;
use Session;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'DESC')->paginate(4);
        $allCategories = Category::all();

        return view('site.product.index', compact('products', 'allCategories'));
    }

    public function view($id)
    {
        $product = Product::find(trim($id));

        return view('site.product.view', compact('product'));
    }

    public function detail($id)
    {
        $products = Product::find(trim($id));

        $productsId = [];
        foreach ($products->keywords as $keyword) {
            if ($keyword->photos->count()) {
                $photos = $keyword->photos()->orderBy('sign', 'DESC')->get();
                if ($photos->count()) {
                    foreach ($photos as $item) {
                        $photosId[] = $item->id;
                    }
                }
            }
        }

        $photosId = array_unique($photosId);
        $similarPhotos = Product::whereIn('id', $photosId)->limit(5)->get();

        $photosId = [];
        foreach ($photo->categories as $category) {
            foreach ($category->photos as $item)
                $photosId[] = $item->id;
        }

        $photosId = array_unique($photosId);
        $samePhotos = Product::whereIn('id', $photosId)->limit(5)->get();
        $allCategories = Category::all();
        return view('site.photo.detail', compact('photo', 'similarPhotos', 'samePhotos', 'allCategories'));
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);

        $cart->add($product, $id);

        $request->session()->put('cart', $cart);

        return redirect()->route('site.product.detail', ['id' => $id])->with('success', 'Successfully added to the cart!');
    }

    public function getCart()
    {
        if (!Session::has('cart')) {
            return view('site.cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);

        $cart->reCalcPrice();

        $sizes = Size::orderBy('id', 'ASC')->pluck('title','id');

        if (empty($cart->items))
            Session::flush();

        return view('site.cart', ['items' => $cart->items,
            'totalPrice' => $cart->totalPrice, 'totalDiscountPrice' => $cart->totalDiscountPrice,
            'sizes' => $sizes, 'totalResultPrice' => $cart->totalResultPrice]);
    }

    public function clearCart()
    {
        Session::flush();
        return view('site.cart');
    }

    public function minusItemCart($priceId, $photoId)
    {
        if (!Session::has('cart')) {
            return view('site.cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $photo = Photo::find(trim($photoId));

        $cart->minusItem($photo, $photoId, $priceId);
        Session::put('cart', $cart);

        return redirect()->route('site.product.cart');
    }

    public function deleteItemCart($sizeId, $photoId)
    {
        if (!Session::has('cart')) {
            return view('site.cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $photo = Photo::find(trim($photoId));

        $cart->deleteItem($photo, $photoId, $sizeId);
        Session::put('cart', $cart);

        return redirect()->route('site.product.cart');
    }

    public function checkout()
    {

    }
}
