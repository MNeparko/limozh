<?php

namespace App\Http\Controllers;

use App\Models\ImageUpload;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Size;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPal\Api\PaymentExecution;

use Config;
use URL;
use PayPal;
use Session;
use Redirect;
use Illuminate\Support\Facades\Input;
use App\Models\Cart;
use App\Models\Price;

class AddMoneyController extends Controller
{
    private $_api_context;

    private $rule = [
        'first_name' => 'required|max:255',
        'last_name' => 'required|max:255',
        'email' => 'required|max:255|email',
        'terms' => 'required'
    ];

    public function __construct()
    {
        parent::__construct();
        $paypal_conf = Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'],
            $paypal_conf['secret']));

        $this->_api_context->setConfig($paypal_conf['settings']);

    }


    public function postPaymentWithpaypal(Request $request)
    {
        $this->validate($request, $this->rule);


        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);


        if ($cart->totalResultPrice == 0)
            Redirect::route('site.photo.cart');

        $newOrder = new Order;
        $newOrder->name = $request->get('first_name') . ' ' . $request->get('last_name');
        $newOrder->email = $request->get('email');
        $newOrder->cost = $cart->totalResultPrice;
        if ($request->has('company') && !empty($request->get('company'))) {
            $newOrder->company = $request->get('company');
        }
        if ($request->has('zip') && !empty($request->get('zip'))) {
            $newOrder->zip = $request->get('zip');
        }
        if ($request->has('city') && !empty($request->get('city'))) {
            $newOrder->city = $request->get('city');
        }
        if ($request->has('phone') && !empty($request->get('phone'))) {
            $newOrder->phone = $request->get('phone');
        }
        if ($request->has('terms')) {
            $newOrder->terms = true;
        }
        if ($request->has('subscribe')) {
            $newOrder->terms = true;
        }
        $newOrder->payment = $request->get('paymentOption');
        $newOrder->transaction_id = 0;
        $newOrder->status = Order::STATUS_NOT_PAID;
        $newOrder->save();
        $sizes = Size::orderBy('id', 'ASC')->get();
        Session::put('order_id', $newOrder->id);
        foreach($cart->items as $keySize => $size) {
            foreach($size as $keyPhoto => $photo) {
                if(!is_numeric($keyPhoto))
                    continue;
                OrderItem::create(['order_id' => $newOrder->id,
                    'photo_id' => $keyPhoto, 'size_id' => $sizes[$keySize-1]->id]);
            }
        }
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $item_1 = new Item();
        $item_1->setName('Limoges payment')
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($cart->totalResultPrice);

        $item_list = new ItemList();
        $item_list->setItems(array($item_1));
        $amount = new Amount();
        $amount->setCurrency('EUR')
            ->setTotal($cart->totalResultPrice);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Your transaction description');
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))
            ->setCancelUrl(URL::route('payment.status'));
        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($transaction));
//        dd($payment->create($this->_api_context));exit;
        try {
            $payment->create($this->_api_context);
        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            Order::find(Session::get('order_id'))->items()->delete();
            Order::find(Session::get('order_id'))->delete();
            return redirect()->route('site.photo.cart')->with('error', 'Some error occur, sorry for inconvenient');

        }

        foreach ($payment->getLinks() as $link) {
            if ($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if (isset($redirect_url)) {
            return Redirect::away($redirect_url);
        }
        Order::find(Session::get('order_id'))->items()->delete();
        Order::find(Session::get('order_id'))->delete();
        return redirect()->route('site.photo.cart')->with('error', 'Unknown error occurred');

    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');

        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {

            return redirect()->route('site.photo.cart')->with('error', 'Payment failed');

        }
        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/

        /** to execute a PayPal account payment. **/

        /** The payer_id is added to the request query parameters **/

        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {
            /** it's all right **/
            /** Here Write your database logic like that insert record or value in database if you want **/
            $payment_inf = [];
            $order = Order::find(Session::get('order_id'));
            //Name
            $payment_inf['name'] = $order->name;
            //Email
            $payment_inf['email'] = $order->email;
            //Phone
            $payment_inf['phone'] = $order->phone;
            //zip
            $payment_inf['zip'] = $order->zip;
            //city
            $payment_inf['city'] = $order->city;
            //address
            //$payment_inf['address'] = $result->getPayer()->getPayerInfo()->getShippingAddress()->getLine1();
            //order number
            //order date
            //transaction id
            $payment_inf['transaction_id'] = $result->getId();
            //transaction time
            $payment_inf['transaction_time'] = $result->getUpdateTime();
            //cart details
            $payment_inf['cart_details'] = substr_replace($result->getCart(), str_repeat('*',strlen($result->getCart())-3),
                0, strlen($result->getCart())-3);
            //amount
            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);
            $cart->reCalcPrice();
            $payment_inf['amount'] = $cart->totalResultPrice;
            //items in cart
            $payment_inf['qty'] = $cart->totalQty;

            $sizes = Size::orderBy('id', 'ASC')->get();
            $totalPrice = $cart->totalResultPrice;
            $items = $cart->items;

            $order->status = Order::STATUS_COMPLETE;
            $order->save();

            $payment_inf['order_id'] = $order->id;
            $payment_inf['order_date'] = $order->created_at->format('d/m/Y');
            //ImageUpload::getPhotosArchive($items);
            Session::flush();
            Session::put('cart_items', $items);

            return view('site.receipt', compact('payment_inf', 'sizes', 'totalPrice', 'items'));

        }
        Order::find(Session::get('order_id'))->items()->delete();
        Order::find(Session::get('order_id'))->delete();
        Session::put('error', 'Payment failed');
        return Redirect::route('addmoney.checkout');

    }

    public function payComplete()
    {
//        $encrypted = Crypt::encrypt('secret');
//        $encrypted = Crypt::decrypt('secret');

        if (Session::has('cart_items')) {
            $items = Session::get('cart_items');
            Session::flush();
            \request()->session()->forget('cart_items');
            ImageUpload::getPhotosArchive($items);
        } else {
            return Redirect::back();
        }
    }

    public function checkout()
    {
        return view('site.checkout');
    }
}
