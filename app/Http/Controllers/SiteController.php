<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\ImageUpload;
use App\Models\Place;
use App\Models\Product;
use App\Models\SendMail;
use App\Models\Shop;
use App\Models\Translation;
use GuzzleHttp\Subscriber\Redirect;
use Illuminate\Http\Request;
use function React\Promise\all;
use Illuminate\Support\Facades\DB;
use Session;

class SiteController extends Controller
{
    public function index()
    {
        $products = [];

        $shop = Shop::where(['id' => Shop::$shopInMain])
            ->first();

        $shopImages = ImageUpload::getImage(Shop::$shopInMain, 'shop');

        $places = Place::where(['shop_id' => Shop::$shopInMain])
            ->select('count')
            ->distinct('count')
            ->get('count');

        $categories = Category::whereHas('products', function ($query) {
            $query->where(['shop_id' => Shop::$shopInMain]);
        })->get();

        foreach ($categories as $category) {
            foreach ($category->products as $product) {
                $categoryName = $category->translationName(Translation::currentLang()) == null ? $category->name : $category->translationName(Translation::currentLang())->value;
                $products[$categoryName][$product->id] = $product->title . ' (Цена: ' . $product->price . 'р.)';
            }
        }

        $maxPlaces = Shop::maxPlaces(Shop::$shopInMain);

        return view('site.index', compact('shop', 'shopImages', 'products', 'places', 'categories', 'maxPlaces'));
    }

    public function contact()
    {
        return view('site.contact');
    }

    public function sendMessage(Request $request)
    {
        $message = $request->get('message');
        $email = $request->get('email');
        $name = $request->get('name');
        $subject = $request->get('title');

        $status = SendMail::html_email($message, $email,
            $name, $subject);

        return redirect()->route('site.contact')
            ->with('success', 'Message sent successfully!');
    }

    public function pricing()
    {
        return redirect()->back();
    }

    public function about()
    {
        $aboutFirst = Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_FIRST,
            'language' => Translation::currentLang()
        ])->first();

        $aboutSecond = Translation::where([
            'type' => Translation::TYPE_PAGE_ABOUT_SECOND,
            'language' => Translation::currentLang()
        ])->first();

        return view('site.about', compact('aboutFirst', 'aboutSecond'));
    }

    public function feedback()
    {
        $feedback = Translation::where([
            'type' => Translation::TYPE_PAGE_FEEDBACK,
            'language' => Translation::currentLang()
        ])->first();
        return view('site.feedback', compact('feedback'));
    }

    public function license()
    {
        $license = Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::currentLang()
        ])->first();
        return view('site.license', compact('license'));
    }

    public function privacy()
    {
        $privacy = Translation::where([
            'type' => Translation::TYPE_PAGE_PRIVACY,
            'language' => Translation::currentLang()
        ])->first();
        return view('site.privacy', compact('privacy'));
    }

    public function terms()
    {
        $terms = Translation::where([
            'type' => Translation::TYPE_PAGE_TERMS,
            'language' => Translation::currentLang()
        ])->first();
        return view('site.terms', compact('terms'));
    }
}
