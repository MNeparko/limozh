<?php

namespace App\Http\Controllers;

use App\Models\ImageUpload;
use App\Models\Shop;
use App\Models\Translation;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        $license = Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::currentLang()
        ])->first();

        $shops = Shop::all();

        return view('site.shop.index', compact('license', 'shops'));
    }

    public function view($id)
    {
        $shop = Shop::find(intval(trim($id)));

        $shopImages = ImageUpload::getImage(intval(trim($id)), 'shop');

        $license = Translation::where([
            'type' => Translation::TYPE_PAGE_LICENSE,
            'language' => Translation::currentLang()
        ])->first();

        return view('site.shop.view', compact('license', 'shop', 'shopImages'));
    }
}
