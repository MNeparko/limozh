<?php

namespace App\Http\Controllers;

use Redirect;
use Session;

class LanguageController extends Controller
{
    public function index($locale)
    {
        if (!\Session::has('locale')) {
            \Session::put('locale', $locale);
        } else {
            \Session::put('locale', $locale);
        }
        return Redirect::back();
    }
}
