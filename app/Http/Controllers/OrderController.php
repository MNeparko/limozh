<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Place;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public $rule = [
        'name' => 'required|max:191',
        'phone' => 'required|max:191',
        'email' => 'required|max:191',
        'reservation_date' => 'required|max:10',
        'reservation_time' => 'required|max:10',
        'count' => 'required|integer|max:10',
        'products' => 'required'
    ];

    private $terms = 1;

    public function store(Request $request)
    {
        $maxPlaces = Shop::maxPlaces(Shop::$shopInMain);
        $this->rule['count'] = 'required|integer|max:' . $maxPlaces;

        $this->validate($request, $this->rule);
        $input = $request->all();

        $products = !empty($request->get('products')) ? $request->get('products') : [];
        $count = !empty($request->get('count')) ? $request->get('count') : 0;

        $input['status'] = Order::STATUS_NOT_PAID;
        $input['cost'] = $this->getCost($products, intval($count));
        $input['terms'] = $this->terms;
        $reservedPlaces = $this->getPlaces($count);

        $order = Order::create($input);

        foreach ($reservedPlaces as $reservedPlace) {
            for ($place = 1; $place <= $reservedPlace['countTablesReserved']; $place++) {
                OrderItem::create([
                    'order_id' => $order->id,
                    'place_id' => $reservedPlace['value']
                ]);
            }
        }

        return redirect()->route('site.index')
            ->with('success', 'Успешно создано');
    }

    private function getCost(array $products, $countPlaces)
    {
        $totalCost = 0;

        if(intval($countPlaces) > 0 && count($products) > 0) {
            $selectProducts = Product::whereIn('id', $products)->get();

            foreach ($selectProducts as $selectProduct) {
                $totalCost += $selectProduct->price;
            }

            $totalCost *= $countPlaces;
        }

        return $totalCost;
    }

    /**
     *array:3 [▼
     *  8 => array:2 [▼
     *      "countTablesReserved" => 4
     *      "countTables" => 4
     *      "value" => 8
     *      "id" => 17
     *  ]
     *  6 => array:2 [▼
     *      "countTablesReserved" => 7
     *      "countTables" => 7
     *      "value" => 6
     *      "id" => 2
     *  ]
     *  4 => array:2 [▼
     *      "countTablesReserved" => 7
     *      "countTables" => 10
     *      "value" => 4
     *      "id" => 8
     *  ]
     *]
     *
     * @param $countPlaces
     * @return array
     */
    private function getPlaces($countPlaces)
    {
        $placesGlobalArray = [];
        $placesArray = [];

        $places = Place::where(['shop_id' => Shop::$shopInMain])
            ->select('count')
            ->distinct('count')
            ->get('count')
            ->sortByDesc('count');

        // TODO: id не корректен
        foreach ($places as $place) {
            $places = Place::where(['shop_id' => Shop::$shopInMain, 'count' => intval($place->count)])
                ->get();

            $placesArray[] = [
                'countTablesReserved' => 0,
                'countTables' => count($places),
                'value' => intval($place->count),
                'id' => $place->id
            ];
        }

        $minSizePlace = $placesArray[count($placesArray) - 1]['value'];

        while ($countPlaces >= $minSizePlace) {
            foreach ($placesArray as $tableValues) {
                $countTablesMaybe = intval($countPlaces / $tableValues['value']);

                if($countTablesMaybe > $tableValues['countTables']) {
                    $placesGlobalArray[intval($tableValues['value'])] = [
                        'countTablesReserved' => intval($tableValues['countTables']),
                        'countTables' => intval($tableValues['countTables']),
                        'value' => intval($tableValues['value']),
                        'id' => intval($tableValues['id'])
                    ];

                    $countPlaces -= $tableValues['countTables'] * $tableValues['value'];
                } elseif($countTablesMaybe > 0) {
                    $placesGlobalArray[intval($tableValues['value'])] = [
                        'countTablesReserved' => intval($countTablesMaybe),
                        'countTables' => intval($tableValues['countTables']),
                        'value' => intval($tableValues['value']),
                        'id' => intval($tableValues['id'])
                    ];

                    $countPlaces -= $countTablesMaybe * $tableValues['value'];
                }

                if($minSizePlace == $tableValues['value'] && $countPlaces < $minSizePlace) {
                    $reservedPlacesInThisCount = isset($placesGlobalArray[intval($tableValues['value'])]['countTablesReserved']) ?
                        $placesGlobalArray[intval($tableValues['value'])]['countTablesReserved'] : 0;

                    $placesGlobalArray[intval($tableValues['value'])] = [
                        'countTablesReserved' => $reservedPlacesInThisCount + 1,
                        'countTables' => intval($tableValues['countTables']),
                        'value' => intval($tableValues['value']),
                        'id' => intval($tableValues['id'])
                    ];

                    $countPlaces -= $countTablesMaybe * $tableValues['value'];
                }
            }
        }

        return $placesGlobalArray;
    }
}