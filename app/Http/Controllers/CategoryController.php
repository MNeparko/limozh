<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function view(Request $request,$id)
    {
        $category = Category::find(trim($id));
        //$categoryName = $category->name;
        $products = $category->products()->paginate(6);
        if($request->ajax()) {

            return [
                'posts' => view('site.category.data')->with(compact('products'))->render(),
                'next_page' => $products->nextPageUrl()
            ];
        }
        $allCategories = Category::all();
        return view('site.category.view', compact('products', 'category', 'allCategories'));
    }
}
