<?php

return array(

    /** set your paypal credential **/

    'client_id' =>'ASHJouFDqveB6sh0uZda6qvfzJXFaERdHHtJAKzcRWgPa5j4iwKQ28vtrxC_986lc0P9IVqYUbPzsB9D',

    'secret' => 'EKYuFy5-49EYOc0LjyJepnZ2QBjzy1KDb9TJEcOC1rx6vAsU_-jJQ70v1aayNI8OYE8lqAK1-Sq7nUQG',

    /**

     * SDK configuration

     */

    'settings' => array(

        /**

         * Available option 'sandbox' or 'live'

         */

        'mode' => 'live',

        /**

         * Specify the max request time in seconds

         */

        'http.ConnectionTimeOut' => 1000,

        /**

         * Whether want to log to a file

         */

        'log.LogEnabled' => true,

        /**

         * Specify the file that want to write on

         */

        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**

         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'

         *

         * Logging is most verbose in the 'FINE' level and decreases as you

         * proceed towards ERROR

         */

        'log.LogLevel' => 'FINE'

    ),

);